--[[
This script got from:
http://www.domoticz.com/forum/viewtopic.php?f=24&t=4190&sid=a951bf47b807b7ac87312762344a1a5a&start=40
Heating Coordinator Script (http://www.domoticz.com/forum/viewtopic.php?f=24&t=4190)
==========================

This script is used to manage the firing of a boiler to support multiple zone requirements using temperature sensor readings and thermostat setpoints in each.

This has been very heavily edited from the above source - but grateful thanks to those who created enough information to get me started.



This is a time script so recieves the following tables:
otherdevices, otherdevices_lastupdate, otherdevices_svalues, uservariables and uservariables_lastupdate

Three scripts make up this functionality together with csv files containing the temperature profiles
a) This one script_time_heatingcoordinator.lua
b) script_device_boostchange.lua
c) Utils scripts in zzutils.lua

For full documentation Refer to APC document "Heating Coordinator Manual.docx" REv 1.00 or later. Dated 28/3/2016


Coppo. 28/3/2016  

]]



local scriptRevision = "Time V1.01 2/4/2016 APC"
-- where the lua scripts are kept
package.path = package.path .. ';' .. '/home/pi/domoticz/scripts/lua/?.lua' 
luautils = require("luautils")

-- Include some util fuctions - declared as locals to improve speed
local logtext = luautils.logtext
local issueDeviceUpdate = luautils.issueDeviceUpdate
local getFirstDeviceSvalue = luautils.getFirstDeviceSvalue
local splitUpString = luautils.splitUpString
local getIdxList = luautils.getIdxList
local updateInfoString = luautils.updateInfoString
local getSetpoint = luautils.getSetpoint
local getLastUpdatedTime = luautils.getLastUpdatedTime
local issueURLCommand = luautils.issueURLCommand
local getSecondsSinceUpdate = luautils.getSecondsSinceUpdate

--global variables
heatingProfileSet = uservariables["HCN_HeatingProfile"]
hcPrefix = uservariables["HCN_NamePrefix"]
radSuffix = uservariables["HCN_RadiatorNameSuffix"]
statSuffix = uservariables["HCN_ThermostatNameSuffix"]
tempSuffix = uservariables["HCN_TempSensorNameSuffix"]
occupiedSuffix = uservariables["HCN_OccupiedSuffix"]
timerSuffix = uservariables["HCN_TimerSuffix"]
HC_FrostSetting = uservariables["HCN_FrostSetting"]
boilerReceiverSwitchName = uservariables["HCN_BoilerReceiverSwitchName"]
heatingZones = splitUpString(uservariables["HCN_Zones"],",")
MySensorsDelayVariableName = "HCN_MySensorsDelay"
hotWaterZoneName = "HotWater"
updateAllIDXDeviceName = "UpdateAllIds"

commandArrayIndex = 1

--local module scope variables
local countActiveRadiatorZones = 0 -- the number of zones neading heat
local countActiveHotWaterZones = 0 -- the number of zones neading heat
local logActivity = uservariables["HCN_LogActivity"]

local boilerStateChangeWaitTime = uservariables["HCN_BoilerStateChangeWaitTimeMins"]
local lastactiveZonesCountName = "HCN_LastActiveZoneCount"
local lastActiveZoneCount = uservariables[lastactiveZonesCountName]
local tempHysteresis = uservariables["HCN_TempHysterisis"]
local tempSensorMaxTimeSinceLastSeen = uservariables["HCN_TempSensorMaxTimeSinceLastSeenSecs"]
local oldBoilerState = otherdevices[boilerReceiverSwitchName]
local oldHouseState = otherdevices["HC_HouseHeatingOn"]
local houseTimerVariableName = "HCN_HouseTimer"
local houseHeatingEnableDevice = "HC_HouseHeatingOn"
local DomoticzIP="192.168.1.170"
local DomoticzPort="8080"

logtext(string.format("START: %s",scriptRevision)) -- for timing purposes

if lastActiveZoneCount == nil then
  lastActiveZoneCount = 0
end


commandArray = {}

-- Process heating requirements
-- first the global stuff - house on/off timed etc
if oldHouseState == nil then
   print("ERROR:Force house heating away")
   oldHouseState = "Off"    
end
local newHouseState = oldHouseState


local oldHouseTimer = uservariables[houseTimerVariableName]
if oldHouseTimer == nil then 
   print("ERROR:Force house timer")
   oldHouseTimer = 0
end
local newHouseTimer = oldHouseTimer
if newHouseTimer > 0 then
  newHouseTimer = newHouseTimer -1
  
  if newHouseTimer == 0 then
    -- timer has just gone to 0, turn everything back on again
    if newHouseState == 'Off' then
      -- turn house state on again
      newHouseState = 'On'
    end
  end
end
if oldHouseState ~= newHouseState then
  commandArray[houseHeatingEnableDevice] = newHouseState
end
if oldHouseTimer ~= newHouseTimer then
  commandArray["Variable:"..houseTimerVariableName] = tostring(newHouseTimer)
end


--dumpCommandArray()




function getZoneHeatNeeded(zoneName)

  -- this function is called once per minute per zone
  local currentZoneTemp = tonumber(getFirstDeviceSvalue(hcPrefix..'_'..zoneName..'_'..tempSuffix))
  local oldZoneTimer = uservariables[hcPrefix..'_'..zoneName..'_'..timerSuffix]
  local oldSetpoint = tonumber(getFirstDeviceSvalue(hcPrefix..'_'..zoneName..'_'..statSuffix))
  local radiatorValveName = hcPrefix..'_'..zoneName..'_'..radSuffix
  local oldRadiatorState = otherdevices[radiatorValveName]
  local newRadiatorState = oldRadiatorState
  local oldZoneState = otherdevices[hcPrefix..'_'..zoneName..'_'..occupiedSuffix]
  if oldRadiatorState == nil then
    -- if the valve hasn;t been operated at all sinec domoticz started up - assume it is ON now
    oldRadiatorState = 'On'
  end
  
  if oldZoneTimer == nil then
    oldZoneTimer = 0;
    logtext(string.format("ERROR: UserVariable %s does not exist",hcPrefix..'_'..zoneName..'_'..timerSuffix),true)
  end
  
  local newZoneTimer = oldZoneTimer
  -- Get the ids of the setpoint thermostat and the text version of it for this zone.
  -- This is because we cannot update these by name - we need the id
  zoneSetpointIDX, zoneInfoIDX = getIdxList(zoneName)
    
  if newZoneTimer == nil then
    newZoneTimer = 0
  elseif newZoneTimer > 0 then
    newZoneTimer = newZoneTimer -1
  end
  newZoneSetpoint = getSetpoint(zoneName, oldSetpoint, newZoneTimer, oldZoneState, newHouseState)
  
  if oldZoneState == 'Off' or newHouseState == 'Off' then
    -- override the zone timer. The zone is OFF
    newZoneTimer = 0
  end
  
  -- if the reading is recent enough - lets use it, otherwise ignore
  local minutesSinceUpdate = getSecondsSinceUpdate(hcPrefix..'_'..zoneName..'_'..tempSuffix, true)/60
  
  
  if currentZoneTemp ~= -1 and 
    tempSensorMaxTimeSinceLastSeen < minutesSinceUpdate then
    -- Its old, ignore it
    currentZoneTemp = 999  -- nonsensical value that will keep the zone switched off
    newRadiatorState = 'Off'
    if minutesSinceUpdate > 60*24 then
      logtext(string.format("ERROR: Temp sensor not seen for %d days", minutesSinceUpdate/(60*24)))
    else
      logtext(string.format("ERROR: Temp sensor not seen for %d:%d hh:mm", minutesSinceUpdate/60, minutesSinceUpdate%60))
    end
  else
    -- determine heating requirements including hysterisis
    if oldRadiatorState == 'On' then
      if (currentZoneTemp > (newZoneSetpoint + tempHysteresis)) then
        newRadiatorState = 'Off'
        if lastActiveZoneCount > 1 then
          -- we are no the last to go off so can switch off immediatley
          commandArray[radiatorValveName] = 'Off'
          logtext(string.format(" Valve/Radiator: '%s' turned OFF", radiatorValveName))
        else
          commandArray[radiatorValveName] = 'Off AFTER 200'
          logtext(string.format(" Valve/Radiator: '%s' turned OFF after 200", radiatorValveName))
        end
        
      end
    else
      if (currentZoneTemp < (newZoneSetpoint - tempHysteresis)) then
        commandArray[radiatorValveName] = 'On'
        newRadiatorState = 'On'
        logtext(string.format(" Valve/Radiator: '%s' turned ON", radiatorValveName))
      end
    end
    if newRadiatorState == 'On' then  
      if zoneName == hotWaterZoneName then
        countActiveHotWaterZones = countActiveHotWaterZones +1
      else
        countActiveRadiatorZones = countActiveRadiatorZones +1
      end
    end
      
  end
  local reportString
  if oldZoneState == 'Off' or newHouseState == 'Off' then
    reportString = string.format("Zone Not heated. House Heating is '%s', Room Heating is '%s'. Temp = '%s'",newHouseState, oldZoneState, currentZoneTemp)
  else
    if newZoneTimer == 0 then
      reportString =  string.format("Temp = '%s', Setpoint = '%s', Heat Needed = '%s'", currentZoneTemp,newZoneSetpoint, newRadiatorState)
    else
      reportString =  string.format("Temp = '%s', Setpoint = '%s', Heat Needed = '%s' for %d minutes", currentZoneTemp,newZoneSetpoint, newRadiatorState, newZoneTimer)
    end
  end
    
  logtext(reportString,true)

  if newZoneSetpoint ~= oldSetpoint or newZoneTimer ~= oldZoneTimer then
    if newZoneTimer ~= oldZoneTimer then
      logtext(string.format("Issue zone timer update was %s is now %s",oldZoneTimer,newZoneTimer))
    end
    -- always update the zone timer so it acts as a timestamp on script updated setpoint
    commandArray["Variable:"..hcPrefix..'_'..zoneName ..'_'..timerSuffix] = tostring(newZoneTimer)
  
    if oldSetpoint ~= newZoneSetpoint then
      --setpoint has changed, write the new value back
      logtext("Issue set point update was ".. oldSetpoint .. ' is ' ..newZoneSetpoint ..' id= '..zoneSetpointIDX)
      issueURLCommand('http://'.. DomoticzIP..':'..DomoticzPort..'/json.htm?type=command&param=udevice&idx='..zoneSetpointIDX..'&nvalue=0&svalue='..tostring(newZoneSetpoint))
      
      --issueURLCommand('http://192.168.1.170:8080/json.htm?type=command&param=udevice&idx='..zoneSetpointIDX..'&nvalue=0&svalue='..tostring(newZoneSetpoint))
      -- device update did not work for thermostat - don't know why. The value changed but the script ran for more than 10 seconds
      --issueDeviceUpdate( zoneSetpointIDX, newZoneSetpoint)
    end
  end
    
  updateInfoString( getFirstDeviceSvalue(hcPrefix..'_'..zoneName..'_Info'),
                    0, -- keep flags2 untouched
                    zoneName, --infZoneName, 
                    zoneInfoIDX, --infZoneIDX
                    newRadiatorState, --infRadiatorState, 
                    oldBoilerState,  --infBoilerState, 
                    newHouseState, --infHouseState,
                    oldZoneState, --infZoneState,
                    newZoneTimer,   --infZoneTimer,
                    newHouseTimer, --infHouseTimer,
                    newZoneSetpoint) --infSetPoint)

  return newRadiatorState

end



function processZones()

  local zoneName
  local hZIndex
  local zoneHeatNeeded
   -- loop our zones and see if any needs heat
  local newBoilerState = oldBoilerState
  local HCN_HeatingValveSwitchName = uservariables["HCN_HeatingValveSwitchName"]
  local oldHeatingValveState = otherdevices[HCN_HeatingValveSwitchName]
  local newHeatingValveState = oldHeatingValveState
  if oldHouseState == "Off" then
    logtext(string.format("House heating OFF , All zones set to Frost setting %s degC",HC_FrostSetting))
  end
  
  for hZIndex = 1, #heatingZones do
      -- determine if heat needed. If so turn on boiler
      zoneName = heatingZones[hZIndex]
      print(string.format('=== ZONE: %d %s ===',hZIndex,zoneName)) -- put nice header in log
      
      zoneHeatNeeded = getZoneHeatNeeded(zoneName)
           
   end
  --local countActiveRadiatorZones = 0 -- the number of zones neading heat
  --local countActiveHotWaterZones = 0 -- the number of zones neading heat

   print(string.format('**** BOILER *****')) -- Nice header in log
   
  if countActiveHotWaterZones > 0 or countActiveRadiatorZones > 0 then 
    local statusString ='Heat Needed by '
    if countActiveHotWaterZones > 0 then
      statusString = string.format ("%s %s_%s_%s ",statusString,hcPrefix, hotWaterZoneName, radSuffix)
    end
    if countActiveRadiatorZones > 0 and countActiveHotWaterZones > 0 then 
      statusString = string.format ("%s AND ",statusString)
    end
    if countActiveRadiatorZones > 0 then
      statusString = string.format ("%s %s of %s radiator zones",statusString,countActiveRadiatorZones, #heatingZones -1)
    end
    logtext(statusString,true)
    
    -- need to put boiler on and turn on heating zone valve if needed
    --local boilerLastSwitchTime = os.difftime(os.time(),getLastUpdatedTime(boilerReceiverSwitchName))
    local boilerLastSwitchTime = getSecondsSinceUpdate(boilerReceiverSwitchName, true)
  
    if oldBoilerState == 'Off' and boilerLastSwitchTime > 240 then
      newBoilerState = 'On'
    end
    if countActiveRadiatorZones > 0 then
      newHeatingValveState = 'On'
    else
      newHeatingValveState = 'Off'
    end
  else
    newBoilerState = 'Off'
    newHeatingValveState = 'Off'
  end
  
  
  if lastActiveZoneCount ~= (countActiveHotWaterZones + countActiveRadiatorZones) then
    commandArray["Variable:"..lastactiveZonesCountName] = tostring(countActiveHotWaterZones + countActiveRadiatorZones)
  end
  
  
  if newBoilerState ~= oldBoilerState then
    commandArray[boilerReceiverSwitchName] = newBoilerState
    logtext(string.format("Boiler State %s Change: was %s is now %s",boilerReceiverSwitchName, oldBoilerState, newBoilerState),true)
  else
    logtext(string.format("Boiler state %s remains %s ",boilerReceiverSwitchName ,oldBoilerState),true)
  end
  
  if newHeatingValveState ~= oldHeatingValveState then
    if newHeatingValveState == 'Off' then
      newHeatingValveState = 'Off AFTER 360'
    end
    logtext(string.format("Radiator Zone valve %s Change: was %s is now %s",HCN_HeatingValveSwitchName ,oldHeatingValveState,newHeatingValveState))
    commandArray[HCN_HeatingValveSwitchName] = newHeatingValveState
  else
    logtext(string.format("Radiator Zone valve %s remains %s ",HCN_HeatingValveSwitchName ,newHeatingValveState))
  end
   
end


function dumpCommandArray()
  for hZIndex = 1, #commandArray do
    print(string.format("Index %d:[%s]",hZIndex,commandArray[hZIndex]))
  end
end


processZones()
logtext(string.format("END")) -- for timing purposes


return commandArray



-- full zone list
--LivingRoom,Kitchen,DiningRoom,Hall,JanStudy,MusicRoom,BackBed,FrontBed,ShowerRoom,MainBed,BathRoom,HotWater,Loo

--[[
To do still 27/3/2016

Add back in remaining zones (temporarily removed)

Sort out boiler requirements for bioth water and heating

Allow boost to work on hotwater - but change the range and start point

Put radiator valve actuator pulse every so often if the valve hasn't been used for a while to stop valves sticking


Possible other issue. If you issue an OFF after command several times at minutes apart they are queue up and are sent at appropriate times
What happens if you then ussue an opposite command that is immediate whilst that queue is still winding down? In this case it probably doesn't matter
because the script overwrites the correct value within th enext 1 minute anyway

May hav eto implement th eoff after logic within the lua script rather than use the built in logic

]]

  --[[
  IMPORTANT FINDING1: If you create a temperature sensor but don't edit or send a value to it it will NOT appear in otehr
  devices. You can send a value such as shown below
  http://192.168.1.170:8080/json.htm?type=command&param=udevice&idx=274&nvalue=0&svalue=20.3  (temperature sensor)
  http://192.168.1.170:8080/json.htm?type=command&param=udevice&idx=274&nvalue=0&svalue=andywozere  (text sensor)
  
  IMPORTANT FINDING2: If you create a thermostat and try and update its setpoint commandarray will time out uninformatively
  and tell you the script has been running for more than 10 seconds
  CANNOT use a thermostat type for this. Must be type temperature
  http://192.168.1.170:8080/json.htm?type=command&param=udevice&idx=274&nvalue=0&svalue=20.3  (temperature sensor)
  http://192.168.1.170:8080/json.htm?type=command&param=udevice&idx=274&nvalue=0&svalue=andywozere  (text sensor)
  
  IMPORTANT FINDING3:
  Cannot pad strings with spaces - all but the first one are stripped out when printing and handling. Hence why zone names are          padded with underscores instead
  ]]