

commandArray = {}

function splitString(str, delim, maxNb)
    -- Eliminate bad cases...
    if string.find(str, delim) == nil then
        return { str }
    end
    if maxNb == nil or maxNb < 1 then
        maxNb = 0    -- No limit
    end
    local result = {}
    local pat = "(.-)" .. delim .. "()"
    local nb = 0
    local lastPos
    for part, pos in string.gmatch(str, pat) do
        nb = nb + 1
        result[nb] = part
        lastPos = pos
        if nb == maxNb then break end
    end
    -- Handle the last field
    if nb ~= maxNb then
        result[nb + 1] = string.sub(str, lastPos)
    end
    return result
end

function getLastUpdatedTime(deviceName)

   lastUpdatedString = otherdevices_lastupdate[deviceName]

   year = string.sub(lastUpdatedString, 1, 4)
   month = string.sub(lastUpdatedString, 6, 7)
   day = string.sub(lastUpdatedString, 9, 10)
   hour = string.sub(lastUpdatedString, 12, 13)
   minutes = string.sub(lastUpdatedString, 15, 16)
   seconds = string.sub(lastUpdatedString, 18, 19)

   return os.time{year=year, month=month, day=day, hour=hour, min=minutes, sec=seconds}

end

function logtext(text, override)
   if logActivity == 1 or override == true then
      print("(HeatingCoordinator) "..text)
   end
end


function processZones()

   logtext("APC testing profile file read")
   
   for heatingZonesIndex = 1, #heatingZones do
     fileName = "//home/pi/domoticz/scripts/zoneprofiles"..heatingCoordinatorNamePrefix..heatingZones[heatingZonesIndex].."_profile.csv"
     logtext(string.format(" Looking for '%s' ",fileName ))
     fh = io.open(fileName,"r")
     if fh then
        --[[
        while true do
          line = fh.read(fh)
          if not line then 
            break 
          end
          print (line)
        end
        ]]
     
     else
       logtext(string.format(" Not Found")  
     end
     

  

   end
   
   return
end

-- User variable population
heatingCoordinatorNamePrefix = uservariables["HeatingCoordinator_NamePrefix"]
radiatorNameSuffix = uservariables["HeatingCoordinator_RadiatorNameSuffix"]
thermostatNameSuffix = uservariables["HeatingCoordinator_ThermostatNameSuffix"]
tempSensorNameSuffix = uservariables["HeatingCoordinator_TempSensorNameSuffix"]
windowSwitchNameSuffix = uservariables["HeatingCoordinator_WindowSwitchNameSuffix"]
boilerReceiverSwitchName = uservariables["HeatingCoordinator_BoilerReceiverSwitchName"]
heatingZones = splitString(uservariables["HeatingCoordinator_Zones"],",")
boilerStateChangeWaitTime = uservariables["HeatingCoordinator_BoilerStateChangeWaitTime"]
boilerAliveTime = uservariables["HeatingCoordinator_BoilerAliveTime"]
tempHysteresis = uservariables["HeatingCoordinator_TempHysterisis"]
logActivity = uservariables["HeatingCoordinator_LogActivity"]
tempSensorMaxTimeSinceLastSeen = uservariables["HeatingCoordinator_TempSensorMaxTimeSinceLastSeen"]

-- Process heating requirements
processZones()

return commandArray

