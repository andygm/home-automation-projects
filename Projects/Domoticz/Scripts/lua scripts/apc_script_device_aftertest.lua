commandArray = {}


function issueDeviceUpdate(deviceID, deviceValue)
  commandArray[commandArrayIndex] = {['UpdateDevice'] = deviceID .. "|0|" .. tostring(deviceValue)}
  commandArrayIndex = commandArrayIndex + 1
end

function logtext(text, override)
   if logActivity == 1 or override == true then
      print("(HeatingCoordinator) "..text)
   end
end



function splitUpString(str, delim, maxNb)
    
    -- Eliminate bad cases...
    if string.find(str, delim) == nil then
        return { str }
    end
    if maxNb == nil or maxNb < 1 then
        maxNb = 0    -- No limit
    end
    local result = {}
    local pat = "(.-)" .. delim .. "()"
    local nb = 0
    local lastPos
    for part, pos in string.gmatch(str, pat) do
        nb = nb + 1
        result[nb] = part
        lastPos = pos
        if nb == maxNb then break end
    end
    -- Handle the last field
    if nb ~= maxNb then
        result[nb + 1] = string.sub(str, lastPos)
    end
    return result
end

-- returns setpoint idx followed by textsetpoint idx
function getIdxList(zoneName)
	setpointDeviceIdx = splitUpString(uservariables["HeatingCoordinator_Setpoint_idx"],",")
	setpointTextDeviceIdx = splitUpString(uservariables["HeatingCoordinator_TextSetpoint_idx"],",")
	for heatingZonesIndex = 1, #heatingZones do
	   if zoneName == heatingZones[heatingZonesIndex] then
	       return setpointDeviceIdx[heatingZonesIndex], setpointTextDeviceIdx[heatingZonesIndex]
	   end
	end
end


function updateAllIdx()
  local DeviceID
  local DomoticzIP="192.168.1.170"
  local DomoticzPort="8080"
  local DeviceFileName = "/var/tmp/JsonAllDevDat.tmp"
  local DeviceList =''
  local InfoDeviceList =''
  local handle
  
  -- check this...
  --https://www.domoticz.com/forum/viewtopic.php?f=28&t=262 
  
  --logtext(string.format("idxlist2")  )
  
  -- Update the list of device names and ids to be checked later
    os.execute("curl 'http://" .. DomoticzIP .. ":" .. DomoticzPort .. "/json.htm?type=devices&order=name' 2>/dev/null | /usr/local/bin/jq -r '.result[]|{(.Name): .idx}' >" .. DeviceFileName)
    
  --logtext(string.format("idxlist3")  )
    
  for hZIndex = 1, #heatingZones do
  
      DeviceName = hcPrefix..'_'..heatingZones[hZIndex]..'_'..statSuffix
      --logtext(string.format("index "..hZIndex.." Name is "..DeviceName)  )
  
       handle = io.popen("/usr/local/bin/jq -r '.\"" .. DeviceName .. "\" | values' " .. DeviceFileName)
       DeviceID = handle:read("*n")
       handle:close()
      if DeviceID == nil then
        DeviceID = 0
      end
      DeviceList = DeviceList..tonumber(DeviceID)..','
      
      DeviceName = hcPrefix..'_'..heatingZones[hZIndex]..'_Info'
      handle = io.popen("/usr/local/bin/jq -r '.\"" .. DeviceName .. "\" | values' " .. DeviceFileName)
      DeviceID = handle:read("*n")
      handle:close()
       
      if DeviceID == nil then
        DeviceID = 0
      end
      --logtext(string.format(DeviceID)  )
      --logtext(string.format("zzzz"))
      InfoDeviceList = InfoDeviceList..tonumber(DeviceID)..','
 
  end
  
  commandArray["Variable:HeatingCoordinator_Setpoint_idx"] = DeviceList
  commandArray["Variable:HeatingCoordinator_TextSetpoint_idx"] = InfoDeviceList

  logtext(string.format("Success - DeviceList= "..DeviceList)  )
  logtext(string.format("Success - InfoDeviceList= "..InfoDeviceList)  )
  
end


function getFirstDeviceSvalue(deviceName, errorReport)
 deviceValue = otherdevices_svalues[deviceName]
  errorReport = true
  if deviceValue == nil then 
    if errorReport then
      logtext(string.format("ERROR: Device name %s Not Found", deviceName), true)  
    end
    return nil
  end
  -- if device has more than just one value get the first one
  deviceValueDelimited = string.find(deviceValue, ";")
  if deviceValueDelimited ~= nil then
    deviceValue = string.sub(deviceValue, 0, deviceValueDelimited - 1)
  end
  
  return deviceValue
end


function updateZoneTextInfo(thisID,  thisZoneTimer, thisSetPoint, thisName)
  --The text string may have changed - lets issue an update if needed
  -- we may have updated the setPoint and the timer and maybe the name. All the rest of the info remains untouched
	oldZoneInfo = getFirstDeviceSvalue(hcPrefix..'_'..thisName..'_Info')
  --[[
  1  flag byte 1 - subtract 0x20 then 6 bits of flag data
  2  flag byte 2 - subtract 0x20 then 6 bits of flag data
  3-7 Zone timer 0-99 mins or next set time 13:23. (If : present it is a time)
  8-11 Set point eg 19.2
  12-25 Zone name

  ]]--
  if oldZoneInfo ~= nil then
    if thisZoneTimer == 0 then
      -- Don't touch the settime/ timer field - it will be updated by the next scheduled lua script
      newZoneInfo = string.sub (oldZoneInfo, 1,7)..string.format("%4.1f", thisSetPoint )..thisName
    else
      newZoneInfo = string.sub (oldZoneInfo, 1,2)..string.format("%2d   %4.1f", thisZoneTimer, thisSetPoint )..thisName
    end

    if newZoneInfo ~= oldZoneInfo then
      --print("apc #######################BOOST#################################zone info update "..newZoneInfo)
      issueDeviceUpdate(thisID, newZoneInfo)
    end
  end
end




--##############################################################################

hcPrefix = uservariables["HeatingCoordinator_NamePrefix"]
logActivity = uservariables["HeatingCoordinator_LogActivity"]
timerSuffix = uservariables["HeatingCoordinator_TimerSuffix"]
occupiedSuffix = uservariables["HeatingCoordinator_OccupiedSuffix"]
tempSuffix = uservariables["HeatingCoordinator_TempSensorNameSuffix"]
statSuffix = uservariables["HeatingCoordinator_ThermostatNameSuffix"]
heatingZones = splitUpString(uservariables["HeatingCoordinator_Zones"],",")

manualOverrideTime = uservariables["HC_OverrideTimeMins"]
commandArrayIndex = 1

apctimer = uservariables["apctimer"]

--[[
Check if a button of name type HC_Kitchen+ or HC_Kitchen- has been pressed,
If so, check the Kitchen timer
If Timer = 0 
	read current kitchen temp
else
	read current kitchen stat temp
either inc or dec that temp and put it into setpoint
set timer to manualOverrideTime
]]--


 -- A bit of DEBUG code to print out which device caused this change
 --[[logging = false
 device = ""
 for i, v in pairs(devicechanged) do
   if (#device == 0 or #i < #device) then device = i end
 end
 if (logging) then print("Triggered by " .. device .. " now " .. otherdevices[device]) end
-- end of printout code
]]

local zoneTimer = -1
infostringchanged = false
zonetimerchanged = false

mydevice = tostring(next(devicechanged))

-- Break the device name that caused this into sections split with underscore character
deviceNameSplit = splitUpString(mydevice,"_")
if deviceNameSplit[1] == "HC" then --{
  -- its a heating control device so we are interested in it
  -- find the ids of the relevent devices that are part of this heating zone
    if deviceNameSplit[2] =="APC1" then
    	logtext(string.format("Off after "..apctimer))
    	commandArray['HC_FrontBed_Radiator'] = 'Off AFTER '..apctimer
    elseif  deviceNameSplit[2] =="APC2" then
    	logtext(string.format("Off after 10"))
    	commandArray['HC_FrontBed_Radiator'] = 'Off AFTER 10'
    elseif  deviceNameSplit[2] =="APC3" then
        logtext(string.format("On"))
    	commandArray['HC_FrontBed_Radiator'] = 'On'
    elseif  deviceNameSplit[2] =="APC4" then
        logtext(string.format("Off"))
    	commandArray['HC_FrontBed_Radiator'] = 'Off'
    end
end  
 

return commandArray
