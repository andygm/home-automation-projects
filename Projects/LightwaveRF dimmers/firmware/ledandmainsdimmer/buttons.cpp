/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

	Last change: AC 15/02/2016 19:15:12
*/


#include "globals.h"
#include "LwRxapc.h"

#include "lights.h"
#include "buttons.h"
#include "indicatorLED.h"
#include <avr/pgmspace.h>




// ===============================================================================
// ===================== BUTTON  DEFINATIONS and variables ======================
#define MAX_BUTTONS LIGHTING_CHANNELS
static byte debouncectr[MAX_BUTTONS];
#define BUTTON_DEBOUNCEmS 50  // any button press must be at least this long
#define START_REPEATmS 800 // the button becomes a long press at this long
#define BUTTON_REPEATmS 70 // and is repeated every n mS (eg for DIM/BRIGHTEN)

// each short button press is counted. The counters are cleared by the inactivity
// timer after INACTIVITY_TIMEOUTmS.
// If the button presses reach PAIRING_SHORT_PRESSES the pairing activity is started
// for that channel and the indicator led starts flashing
static byte buttonrepeatctr[MAX_BUTTONS];
#define INACTIVITY_TIMEOUTmS 1500
#define PAIRING_SHORT_PRESSES 4
#define PAIRING_TIMEOUTmS 5000 // stay in pairing mode until paired or timeout. Max 30000

// Count inactivity (no button) time in LAMP_UPDATEmS units.
// if LAMP_UPDATEmS = 15mS than the max is 255 x 15 = 3800mS approx
static byte inactivityCtr;

// control the direction of fade on every alternate button press. If the
// channel bit is set, the next button press fades UP, otherwise DOWN.
byte fadeUpBitlist;

// a store used to delay button OFF commands. We need to distinguish between a
// long button press meaning dim/brighten or a short press which is ON or off
static byte delayedoffbitlist = 0;

// some arrays to make setting up the pins easier
static const byte channelButtons[MAX_BUTTONS] = { BUTTON1_PIN, BUTTON2_PIN, BUTTON3_PIN, BUTTON4_PIN};


void initButtons(void)
{
    for (byte i = 0; i < MAX_BUTTONS; i++)
    {
        pinMode(channelButtons[i], INPUT_PULLUP);
    };
}

// called every light update - nominally every 15mS
void handleButtons(void)
{
    static byte oldstatus;
    byte ctr = 0;
    byte bitmask = 0x01;
    boolean clearbuttonctr = false;
    byte currentstatus =
    ((digitalRead(BUTTON1_PIN)==LOW)? 0x01:0) +
    ((digitalRead(BUTTON2_PIN)==LOW)? 0x02:0) +
    ((digitalRead(BUTTON3_PIN)==LOW)? 0x04:0) +
    ((digitalRead(BUTTON4_PIN)==LOW)? 0x08:0);
//apcdebug();
    if ( ++inactivityCtr >= INACTIVITY_TIMEOUTmS/LAMP_UPDATEmS )
    {
        inactivityCtr = 0;
        clearbuttonctr = true;
    }
    do
    {
        if (clearbuttonctr)
        {
            buttonrepeatctr[ctr] = 0;
        }
        if ( (currentstatus & bitmask) != (oldstatus & bitmask))
        {
            // this button has changed state
            debouncectr[ctr] = 0;
            oldstatus ^= bitmask;
        }
        else
        {
            // this button is unchanged
            if ( ++debouncectr[ctr] == (BUTTON_DEBOUNCEmS/LAMP_UPDATEmS))
            {
                // its just settled into this state
                if ((oldstatus & bitmask) != 0)
                {
                    // its just gone down
                    inactivityCtr = 0;
                    if ( pairingActive() )
                    {
                        // any button press when pairing is active stops pairing mode
                        stopMakePair();
                    }

                    if ( ++buttonrepeatctr[ctr] == PAIRING_SHORT_PRESSES )
                    {
                        // start pairing
                        lwrxapc_makepair((PAIRING_TIMEOUTmS/100), ctr);
                        Serial.printf("Start Pairing\n\r");
                    }
                    fadeUpBitlist ^= bitmask; // alternate up and down fade direction

                    // check whether this lamp is on or off
                    if ( lampStateOn(ctr) )
                    {
                        // its currently on, set a flag so that if we have a short press
                        // we will turn it off
                        delayedoffbitlist |= bitmask;
                    }
                    else
                    {
                        //Its off, turn it on at its current level
                        startFade(ctr, FM_ON, 0);
                    }
                    Serial.printf("Button %d DOWN\n\r",ctr);
                }
                else
                {
                    // its just been released
                    Serial.printf("Button %d RELEASED\n\r",ctr);
                    if ( (delayedoffbitlist & bitmask) != 0 )
                    {
                        delayedoffbitlist &= ~bitmask;
                        startFade(ctr, FM_OFF, 0);
                    }
                }

            }
            else if (debouncectr[ctr] > (START_REPEATmS/LAMP_UPDATEmS))
            {
                // its a long press
                delayedoffbitlist &= ~bitmask; // clear the stored possible command
                debouncectr[ctr] = (START_REPEATmS - BUTTON_REPEATmS)/LAMP_UPDATEmS;
                if ((oldstatus & bitmask) != 0)
                {

                    if (buttonrepeatctr[ctr] > 5)
                    {
                        // its 5 short presses followed by one long one
                        // clear all stored data
                        setIndicatorLED(LEDS_START_ALLCLEAR_SUCCESS);
                       lwrxapc_clearpairing();
                       initLightLevels();
                    }
                    else
                    {

    //                    Serial.printf("Button %d REPEAT\n\r",ctr);
                        startFade(ctr,
                            ((fadeUpBitlist & bitmask) != 0)?FM_BUTTON_BRIGHTEN:FM_BUTTON_DIM,
                             0);
                            }
                }
            }
        }
        bitmask <<= 1;
    }
    while ( ++ctr < MAX_BUTTONS );
}


