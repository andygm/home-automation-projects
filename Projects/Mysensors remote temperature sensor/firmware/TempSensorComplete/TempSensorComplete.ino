
/* This sketch is a battery power temperature sensor fitted with OLED display and buttons to control setpoint.
 * It takes around 12uA from a 3v battery, wakes up every so often to check the temperature. If it has changed or it hasn't phoned home for a long while it sends
 * an update of temperature and requests the latest info back - via a text sensor message.
 * 
 * If any button is pressed it wakes up and turns on the OLED display for a few seconds. Any button press also requests an text update from the gateway. 
 * 
 * Domoticz responds to the text request with a multi line string, comma seperated. Nomrally this woudl be 
 *  17.2,60,Kitchen      which is setpoint, timer in minutes and zone name
 *  
 *  Domoticz responds to the plus and minus buttons by operating the BOOST+ or BOOST- functions on this zone.
 * Button presses are sent to Domoticz, the plus button is normally handled as a boost + function (increase setpoint by 1 deg for 60 mins) 
 * The minus button decreases setpoint by 1 degree for 60minutes
 * 
 * The gateway is a serial gateway on arduino connected to a RPI running Domoticz.
 * 
 * 
	Last change: AC 05/04/2016 18:29:04
 */
#include <MySensorapc.h>
#include "myglobals.h"
#include <SPI.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <Time.h>
#include <Wire.h>  // I2C library
#include <SSD1306_textapc.h> // the text version only library for the OLED display

#include "MyHw.h"
#include "MyHwATMega328.h"
#include "buttons.h"
#include "menus.h"
#include <avr/pgmspace.h>



// Define this if we want to see how good the radio is performing
//#define DEBUG_MYSENSORS_PERF

// I2c Data line for display A4/PC4
// I2c clock line for display A5/PC5

/*
 *  If

 */

float lastTemperature;
boolean radioLostContact;
int oldbatteryPercent;

typedef enum
{
    RTS_IDLE,
    RTS_RETRY,
    RTS_GETTING_INFO,
    RTS_WAITING_FOR_SEND
//    RTS_GETTING_TIME

}t_radioTickState;
static t_radioTickState radioTickState;

static unsigned int timestampTextInfo;
static unsigned int timestampTempInfo;

static t_bitlist8 radioStatusBitlist;

//============================================================

/*
    INFO String mapping is as follows. 25 bytes to match Mysensors payload
     (NOTE : LUA byte numbering shown - starts at 1 whereas C starts at 0
        1-12 Fixed 12 character (No end NULL) name padded with spaces - don't touch
        13-15 digit room override timer in minutes
        16-18 3 digit house override timer in minutes
        19-21 set point in form of three digits EG 192 means 19.2C
        22  flag byte 1 - subtract 0x20 then 7 bits of flag data
        23  flag byte 2 - subtract 0x20 then 7 bits of flag data
        24-25 not used

  Flags - subtract 32 then...
    Flag1==============
          bits 0-2:   0-7. (0 = 15sec, 1 = 30s etc. 15s/30s/60s/2m/4m/8m/16m/32m). This is the update rate for mysensor in minutes -          getfrom domoticz. Typical is 2 for every 1 minute
          bit 3 :     This zone valve is on (from domoticz)
          bit 4 :     zone enabled if 1 (zone unocupied)(from domoticz)
          bit 5 :     House enabled if 1  (from domoticz)
          bit 6 :     Boiler is ON if 1 (from domoticz)

    Flag2==============
    bit 0 - if set - disable house heating (from sensor)
    bit 1 - if set - enable house heating (from sensor)
    bit 2 -if set - disable room heating (from sensor)
    bit 3 -if set - enable room heating (from sensor)
*/
#define FLAG1_OFF (22 -1)
#define FLAG2_OFF (23 -1)
#define ROOMOV_OFF (13 -1)
#define HOUSEOV_OFF (16 -1)
#define SETPOINT_OFF (19 -1)

static unsigned char infoRxBuffer[INFO_MESSAGE_SIZE] =
{
    'a','b','c','d','e','f','g','h','i','j','l','p',
    '0','0','1',
    '0','0','3',
    '1','9','.','3',
    '0x0',  // flag 1
    '0x0',  // flag 2
};
static unsigned char infoWorkingCopy[INFO_MESSAGE_SIZE];
// stores for the info coming in on a text message from Domoticz
static char oldInfo[MAX_PAYLOAD+1]; //technically it is the mysensors payload size

static unsigned int domoticzUpdate = 1 * 600;
static DeviceAddress DSdeviceAddress;

MySensor gw;


unsigned int countTenths, oldTenths;
unsigned char countHalfMinutes;
byte stayAwake = 20;

unsigned long elapsedMillis;


void timestamp(void)
{
     Serial.printf(F("%02d:%02d:%03d "),(unsigned int)(millis()/60000L), (unsigned int)(millis()/1000)%60,millis()%1000);
}

// initialise a general message - can overwrite it later
MyMessage msgGeneral(CHILD_ID_LOCAL_TEMP,V_TEMP);


//initialise one wire bus system
OneWire oneWire(PIN_ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

boolean hasMainInfoChanged(void)
{
    return  !memcompcpy((char*)infoWorkingCopy,(char*)infoRxBuffer);
}


boolean checkUpToDateInfo(unsigned int numseconds)
{
    boolean status = true;
    int oldestdata = (unsigned int)(countTenths - timestampTextInfo);
    //timestamp();  Serial.printf("CRD:Checking for recent data %d seconds\n\r", numseconds);
    if ( (unsigned int)(countTenths - timestampTextInfo) < (numseconds*10) )
    {
        // we have reasonably up to date info - copy the latest recieved data
        //timestamp(); Serial.printf("apczzz1 Text OK\n\r");
        memcompcpy((char*)infoWorkingCopy,(char*)infoRxBuffer);
    }
    else
    {
        //timestamp();Serial.printf("apczzz2Text OLD %04x %04x\n\r", countTenths,timestampTextInfo);
        status = false;

        updateRadioInfo( RI_REQUEST_INFO );
    }
    oldestdata = (unsigned int)(countTenths - timestampTempInfo);
    if ( (unsigned int)(countTenths - timestampTempInfo) >= (numseconds *10) )
    {
        requestTemperatures();
        status = false;
    }
    return status;
}

void sendInfoToDomoticz(void)
{
    memcompcpy((char*)infoRxBuffer,(char*)infoWorkingCopy);
    // assume data has changed - this only happens after menu action so sne it every time
    updateRadioInfo( RI_SEND_INFO);
   // make the current data appear to be a minute old so we will request an update
    timestampTextInfo = (unsigned char)(timestampTextInfo - 600);
}


//=====================================================
void checkBatteryState() {
    // Read 1.1V reference against AVcc
    // set the reference to Vcc and the measurement to the internal 1.1V reference
    #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
    #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    
    ADMUX = _BV(MUX3) | _BV(MUX2);
    #else
     ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    #endif
 
    delay(2); // Wait for Vref to settle
    ADCSRA |= _BV(ADSC); // Start conversion
    while (bit_is_set(ADCSRA,ADSC)); // measuring
 
    uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
    uint8_t high = ADCH; // unlocks both
 
    long result = (high<<8) | low;
 
    result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000

    if ( result <= BATTERYEMPTYmV)
    {
      result = 0;
    }
    else if (result >= BATTERYFULLmV)
    {
      result = 100;
    }
    else
    {
       result = ((result - BATTERYEMPTYmV) *100)/ (BATTERYFULLmV - BATTERYEMPTYmV);
    }
    oldbatteryPercent = (int)result;
}

//============================================================


//============================================================

//============================================================

//============================================================



unsigned char getFlag1(void)
{
    return infoWorkingCopy[FLAG1_OFF] - 32;
}
void putFlag1(unsigned char value)
{
    infoWorkingCopy[FLAG1_OFF] = value + 32;
}
unsigned char getFlag2(void)
{
    return infoWorkingCopy[FLAG2_OFF] - 32;
}
void putFlag2(unsigned char value)
{
    infoWorkingCopy[FLAG2_OFF] = value + 32;
}

void setRoomOn(boolean roomOn)
{
    unsigned char flag1 = getFlag1();
    unsigned char flag2 = getFlag2();
    if ( roomOn )
    {
        flag1 |= 0x01 << 4;
        flag2 |= 0x01 << 3;
        flag2 &= ~(0x01 << 2);
    }
    else
    {
        flag1 &= ~(0x01 << 4);
        flag2 |= 0x01 << 2;
        flag2 &= ~(0x01 << 3);
    }
    putFlag1(flag1);
    putFlag2(flag2);
}

void setHouseOn(boolean houseOn)
{
    unsigned char flag1 = getFlag1();
    unsigned char flag2 = getFlag2();
    if ( houseOn )
    {
        flag1 |= 0x01 << 5;
        flag2 |= 0x01 << 1;
        flag2 &= ~(0x01 << 0);
    }
    else
    {
        flag1 &= ~(0x01 << 5);
        flag2 |= 0x01 << 0;
        flag2 &= ~(0x01 << 1);
    }
    putFlag1(flag1);
    putFlag2(flag2);
}
boolean getHouseOn(void)
{
    return (getFlag1() & (0x1 << 5)) != 0;
}
boolean getRoomOn(void)
{
    return (getFlag1() & (0x1 << 4)) != 0;
}
boolean getBoilerOn(void)
{
    return (getFlag1() & (0x1 << 6)) != 0;
}
boolean getRadiatorOn(void)
{
    return (getFlag1() & (0x1 << 3)) != 0;
}

void getRoomName(char *dest)
{
    char ctr = 0;
    // because of a bug in domoticz, the zone name is padded with underscores
    // rather than spaces. Replace the spaces
    do
    {
        if ( !isalpha(infoWorkingCopy[ctr]) || infoWorkingCopy[ctr] == '_' )
        {
            *dest++ = ' ';
        }
        else
        {
            *dest++ = infoWorkingCopy[ctr];
        }
    }while ( ++ctr < 11 );
    *dest = '\0';
}

int getHouseTime(void)
{
    int value = (infoWorkingCopy[HOUSEOV_OFF] -'0') *100 +
                (infoWorkingCopy[HOUSEOV_OFF+1] -'0') *10 +
                (infoWorkingCopy[HOUSEOV_OFF+2] -'0');
    if ( value > 999 )
    {
        value = 999;
    }
    return value;
}
void putHouseTime(int value)
{
    if ( value > 999 )
    {
        value = 999;
    }

    infoWorkingCopy[HOUSEOV_OFF] = (value /100) + '0';
    infoWorkingCopy[HOUSEOV_OFF+1] = ((value % 100)/10) + '0';
    infoWorkingCopy[HOUSEOV_OFF+2] = ((value % 10)/1) + '0';
}

int getRoomTime(void)
{
    int value = (infoWorkingCopy[ROOMOV_OFF] -'0') *100 +
                (infoWorkingCopy[ROOMOV_OFF+1] -'0') *10 +
                (infoWorkingCopy[ROOMOV_OFF+2] -'0');
    if ( value > 999 )
    {
        value = 999;
    }
    return value;
}

void putRoomTime(int value)
{
    if ( value > 999 )
    {
        value = 999;
    }
    infoWorkingCopy[ROOMOV_OFF] = (value /100) + '0';
    infoWorkingCopy[ROOMOV_OFF+1] = ((value % 100)/10) + '0';
    infoWorkingCopy[ROOMOV_OFF+2] = ((value % 10)/1) + '0';
}

int getSetPoint(void)
{
    int value = (infoWorkingCopy[SETPOINT_OFF] -'0') *100 +
                (infoWorkingCopy[SETPOINT_OFF+1] -'0') *10 +
                (infoWorkingCopy[SETPOINT_OFF+2] -'0');
    if ( value > 999 )
    {
        value = 999;
    }
    return value;
}

void putSetPoint(int value)
{
    if ( value > 999 )
    {
        value = 999;
    }
    infoWorkingCopy[SETPOINT_OFF] = (value /100) + '0';
    infoWorkingCopy[SETPOINT_OFF+1] = ((value % 100)/10) + '0';
    infoWorkingCopy[SETPOINT_OFF+2] = ((value % 10)/1) + '0';
}


boolean memcompcpy(char *dest, char *src)
{
    boolean same = true;
    byte ctr = 0;
    while ( ++ctr < MAX_PAYLOAD)
    {
//        Serial.printf("ctr %d, src %02x dest %02x\n\r",ctr,*src, *dest);
        if ( *src != *dest )
        {
//            Serial.printf("MISMATCH\n\r");
            *dest = *src;
            same = false;
        }
        src++;
        dest++;
    }
    return same;
}


void incomingMessage(const MyMessage &message)
{

    if (message.type == V_TEXT)
    {
        //timestamp(); Serial.printf(F("Got V_TEXT info\n\r"));
        radioLostContact = false;
        // lets assume it is for us for now
        if (radioTickState == RTS_GETTING_INFO )
        {
            //timestamp(); Serial.printf(F("We were waiting\n\r"));
            radioTickState = RTS_IDLE;
            radioStatusBitlist &= ~RI_REQUEST_INFO;
            timestampTextInfo = countTenths;

            if ( memcompcpy ((char *)infoRxBuffer, (char*)message.data) == false )
            {
                // THis message includes the zone name, room and house timers,
                //setpoint and some flags. Lets assume it has all changed
                // apczzz could improve on this
                // The only thiungs we react to outside of the menus is the update rate
                // calculate the base update rate - add a bit of spread from the id
                //timestamp(); Serial.printf("Its different - set update flag\n\r");
                #ifdef debugsss
                {
                    char ctr = 0;
                    do
                    {
                        if ( infoRxBuffer[ctr] < 0x20 || infoRxBuffer[ctr] > 127 )
                        {
                            Serial.printf("%02x ",infoRxBuffer[ctr]);
                        }
                        else
                        {
                            Serial.printf("%c  ",infoRxBuffer[ctr]);
                        }
                    }
                    while ( ++ctr < 24 );
                    Serial.printf("\n\r");
                }
                #endif
                unsigned int calc = (infoRxBuffer[FLAG1_OFF] -32) & 0x07;

                domoticzUpdate = ((1 << calc) * 300) + (gw.getNodeId() *16);
            }
        }
    }
}
  





static unsigned long oldmillis, oldTenthsMillis;

void updateRadioInfo(t_bitlist8 value)
{
    radioStatusBitlist |= value;
    //timestamp(); Serial.printf("UpdateRadioInfo %d %02x\n\r",radioTickState, radioStatusBitlist);
}


static char radioTryCtr;
static void radioTick(void)
{
    static unsigned char radioTimeoutTenths;

    if ( radioStatusBitlist != 0 || radioTickState == RTS_WAITING_FOR_SEND )
    {
        stayAwake = 3;
        switch ( radioTickState )
        {
            case RTS_RETRY:
            case RTS_IDLE:
                if ( (radioStatusBitlist & RI_REQUEST_INFO) != 0 )
                {
                    //timestamp();  Serial.printf(F("Request text update\n\r"));
                    gw.request( CHILD_ID_TEXT, V_TEXT );
                    if ( radioTickState == RTS_IDLE )
                    {
                        radioTryCtr = 6;
                    }
                    radioTickState = RTS_GETTING_INFO;

                    radioTimeoutTenths = (unsigned char)countTenths;
                }
                else if ( (radioStatusBitlist & RI_SEND_BATTERY) != 0 )
                {
                    gw.sendBatteryLevel(oldbatteryPercent);
                    //timestamp();  Serial.printf(F("SEnding battery update %d percent\n\r"),oldbatteryPercent);
                    radioStatusBitlist &= ~RI_SEND_BATTERY;
                    radioTimeoutTenths = (unsigned char)countTenths;
                    radioTickState = RTS_WAITING_FOR_SEND;
                }
                else if ( (radioStatusBitlist & RI_SEND_INFO) != 0 )
                {
                    radioStatusBitlist &= ~RI_SEND_INFO;
                    //timestamp();   Serial.printf(F("SEnding text update\n\r"));
                    #ifdef debugsss
                    {
                        char ctr = 0;
                        do
                        {
                            Serial.printf("%02x ",infoRxBuffer[ctr]);
                        }
                        while ( ++ctr < 25 );
                        Serial.printf("\n\r");
                    }
                    #endif
                    gw.send(msgGeneral.setSensor(CHILD_ID_TEXT).setType(V_TEXT).set((char*)infoRxBuffer));
                    // immediately request an update. If Domoticz changed the setpoint we will pick up the
                    // new value by doing this
                    radioStatusBitlist |= RI_REQUEST_INFO;
                    radioTimeoutTenths = (unsigned char)countTenths;
                    radioTickState = RTS_WAITING_FOR_SEND;
                }
                else if ( (radioStatusBitlist & RI_SEND_LOCAL_TEMP) != 0 )
                {
                    gw.send(msgGeneral.setSensor(CHILD_ID_LOCAL_TEMP).setType(V_TEMP).set(lastTemperature,1));
                    //timestamp();  Serial.printf(F("SEnding temperature update\n\r"));
                    radioStatusBitlist &= ~RI_SEND_LOCAL_TEMP;
                    radioTimeoutTenths = (unsigned char)countTenths;
                    radioTickState = RTS_WAITING_FOR_SEND;

                }
                break;

            case RTS_GETTING_INFO :
                if ( (unsigned char) ( (unsigned char)countTenths - radioTimeoutTenths) > 5)
                {
                    timestamp();
                    if ( --radioTryCtr < 0 )
                    {
                        //timestamp();Serial.printf(F("Get info Timed out \n\r"));
                        radioLostContact = true;
                        radioStatusBitlist = 0;
                        radioTickState = RTS_IDLE;
                    }
                    else
                    {
                        //timestamp(); Serial.printf(F("Get info retry - ctr %d\n\r"),radioTryCtr);
                        radioTickState = RTS_RETRY;
                    }
                }
                break;

            case RTS_WAITING_FOR_SEND :
                if ( (unsigned char) ( (unsigned char)countTenths - radioTimeoutTenths) > 2)
                {
                    // allow a bit of time to send
                    //timestamp();  Serial.printf(F("End of RTS_WAITING \n\r"));

                    radioTickState = RTS_IDLE;
                }
                break;
            default :
                break;
        }
    }
}


#define TEMP_IDLE 0
#define TEMP_CHECK 1

static char tempTriggered;
static unsigned int localTempTenths;

void requestTemperatures(void)
{
    tempTriggered = TEMP_CHECK;
    sensors.requestTemperatures();
    localTempTenths= countTenths;
}

static int oldIntTemp = 0;
static void localTempTick(void)
{
    if ( tempTriggered != TEMP_IDLE)
    {
        // we are awaiting a read coming back
        if (sensors.isConversionAvailable(DSdeviceAddress))
        {
            lastTemperature = static_cast<float>(static_cast<int>((gw.getConfig().isMetric?sensors.getTempCByIndex(0):sensors.getTempFByIndex(0)) * 10.)) / 10.;
            int intTemp = lastTemperature * 100;
            if ( abs(oldIntTemp - intTemp) > 12 ) // less than 0.12 ignore
            {
                //timestamp(); Serial.printf("Old temp %d new %d trig %d\n\r",oldIntTemp, intTemp, tempTriggered);
                oldIntTemp  = intTemp;
                if ( oldIntTemp > (-15 *100) && oldIntTemp < (80 *100) )
                {
                    updateRadioInfo(RI_SEND_LOCAL_TEMP);
                }
                else
                {
                    // it went wrong - try again
                    requestTemperatures();
                }

            }
            timestampTempInfo = countTenths;
            localTempTenths= countTenths;
            tempTriggered = TEMP_IDLE;
        }
        else if ((unsigned int)(countTenths - localTempTenths) > (5))
        {
            // try again, timed out
            timestamp(); Serial.printf(F("ERROR: tempture read timed out\n\r"));
            requestTemperatures();
        }
    }
}


static void backgroundDataCheck(void)
{
    // evry hour approx
    // get latest text info
    updateRadioInfo( RI_REQUEST_INFO | RI_SEND_BATTERY );

    // and get a temperature update
    oldIntTemp = 0; // set old to 0 to force sending radio update
    requestTemperatures();
    checkBatteryState();
}


void setup()
{ 
    // clear the info stores
    pinMode(4,OUTPUT); // for debug

    pinMode(PIN_DISPLAY_ENABLE, OUTPUT);
    digitalWrite(PIN_DISPLAY_ENABLE, 0);
    delay(10);

    // Startup OneWire
    sensors.begin();

    //sensors.setResolution(11);

    // Startup and initialize MySensors library. Set callback for incoming messages.
    gw.begin(incomingMessage, SENSOR_ID);  // had porblems with auto - gateway not seeing the device


    // Send the sketch version information to the gateway and Controller
    gw.sendSketchInfo(SENSOR_NAME, SENSOR_REVISION);

    // Fetch the number of attached temperature sensors
    sensors.getDeviceCount();
    // get the address of the dallas temp sensor, we'll need it later
    sensors.getAddress(DSdeviceAddress, 0);

    // Present temperature sensor to controller
    gw.present(CHILD_ID_LOCAL_TEMP, S_TEMP);

    // present the text sensor. This allows us to get general info from Domoticz -
    // in this case timers, flags, setpoint and name value string
    gw.present( CHILD_ID_TEXT,S_INFO);


    // when we kickoff temp reading - don't wait - use isConversionAvailable() to check when we have a reading
    sensors.setWaitForConversion(false);


    stayAwake = 10;

    buttonsInit();

    checkBatteryState();

    // do an initial check of battery state, update data etc
    backgroundDataCheck();
    Serial.printf("Hello\n\r");

    menuInit();
}

void loop()
{

    // keep elapsedMillis more or less up to date
    if ( (millis() - oldmillis) > 0 )
    {
        elapsedMillis += millis() - oldmillis;
        oldmillis = millis();
    }
    // keep countTenths more or less up to date
    if ( (elapsedMillis - oldTenthsMillis ) > 100L )
    {
        // every 10th of a second if we are awake only
        unsigned int tenthsjumped = (elapsedMillis - oldTenthsMillis)/100L;
        countTenths += tenthsjumped;
        oldTenthsMillis += tenthsjumped * 100L;
        handleDisplayTimer10ths();
        if ( stayAwake > 0 )
        {
            --stayAwake;
        }
    }

    if ((unsigned int)(countTenths - oldTenths) > domoticzUpdate)
    {
        // every minute initially - or as set by domoticz,

        // keep the elapsed time counters more or less correct
        while ( (unsigned int)(countTenths - oldTenths) > 300 )
        {
            ++countHalfMinutes;
            oldTenths += 300;
        }
        // kick off aa read of temperature
        requestTemperatures();
    }

    localTempTick();
    radioTick();
    static unsigned char oldHalfMinutes;
    if ((unsigned char)(countHalfMinutes - oldHalfMinutes) > (120 + (gw.getNodeId() *20)))
    {
        // evry hour approx
        oldHalfMinutes = countHalfMinutes;
        backgroundDataCheck();
    }
    if (menuPageTick() )
    {
        stayAwake = 3;
    }
    // Process incoming messages (like config from server)
    gw.process();
    // debounce and act on button presses
    buttonsProcess();

    if ( stayAwake == 0 )
    {

        enableButtonInterrupts();
        gw.sleep(domoticzUpdate * 100L);
        oldmillis = millis();
        //Serial.printf("WakeUP\n\r");
    }


}
/* TO DO STILL 25/10/2015

b) Rename info functionaluty everywhere
g) Chnage bootloader so BOD is lower than 2.7

Make forec update work correctly
Update battery on boot

Check all timing. Make sure regular updates happen where expected
a) temperature
b) Get info


Skip screens that don't make sense

Get and display server time

Tidy up generallh




 */

/*
    Experimenting with data timeouts. With Data timeout of 2 seconds, the first one nearly always times out
    but the second gets through in mS.
    Altered the send messages to wait for 0.5 seconds before doing anything else


    Try increasing timeout to 5secs
*/

