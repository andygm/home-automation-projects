#ifndef __buttons_h
#define  __buttons_h

/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

	Last change: AC 13/02/2016 17:06:26
*/

extern void initButtons(void);
extern void handleButtons(void);

extern byte fadeUpBitlist;


#endif //  __buttons_h
