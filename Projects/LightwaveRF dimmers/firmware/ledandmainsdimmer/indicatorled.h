/** @package 

    indicatorled.h
    
    Copyright(c) Microsoft 2000
    
    Author: Andrew Copsey
    Created: AC  13/02/2016 17:37:30
	Last change: AC 13/02/2016 17:39:01
*/
#ifndef __indicatorled_h
#define  __indicatorled_h

typedef enum
{
    LEDS_PAIRING_FLASH,
    LEDS_RX_FLASH,
    LEDS_START_RX_FLASH,
    LEDS_START_PAIRING_SUCCESS,
    LEDS_PAIRING_SUCCESS,
    LEDS_START_PAIRING_REMOVE_SUCCESS,
    LEDS_PAIRING_REMOVE_SUCCESS,
    LEDS_START_ALLCLEAR_SUCCESS,
    LEDS_ALLCLEAR_SUCCESS,
    LEDS_START_MOODSTORE_SUCCESS,
    LEDS_MOODSTORE_SUCCESS,
    LEDS_SWITCH_LED_OFF,
    LEDS_LED_OFF

}t_led_state;

extern void setIndicatorLED(t_led_state state);

extern void handleLED(void);

#endif //  __indicatorled_h
