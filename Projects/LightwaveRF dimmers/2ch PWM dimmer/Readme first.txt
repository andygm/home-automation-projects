This two channel PWM dimmer is based on Arduino MiniPro code rev 0.04 described in the accompanying docs. Only channels 1 & 2 are used in the four channel core, channels 3& 4 are left unconnected.

The schematic was captured using Altium Designer and built using veroboard and small plastic enclosure.

The schematics and BOMs can be found in the project outputs sub directory

APC 8th April 2016
