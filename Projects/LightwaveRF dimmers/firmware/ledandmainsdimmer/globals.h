#ifndef __globals_h
#define __globals_h
/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

	Last change: AC 15/02/2016 19:29:27
*/


#define SERIAL_BAUD 115200
#define MAINS_FREQ 50 // in hertz

// io pin definitions
/* RXD	PD0/30	PCINT16/Debug Rx    */
/* TXD	PD1/31	PCINT17/Debug Tx    */
/* D2	PD2/32	PCINT18/INT0        */ #define ZEROCROSS_PIN 2
/* D3	PD3/1	PCINT19/INT1/PWM    */ #define RXIO_PIN 3
/* D4	PD4/2	PCINT20/            */ #define nNODIMMING1_PIN 4
/* D5	PD5/9	PCINT21/T1/PWM      */ #define nNODIMMING34_PIN 5
/* D6	PD6/10	PCINT22/AIN0/PWM    */ #define CHANNEL4_OUT_PWM_PIN 6
/* D7	PD7/11	PCINT23/AIN1        */ #define nNODIMMING2_PIN 7
/* D8	PB0/12	PCINT0/             */ #define CHANNEL4_OUT_TRIAC_PIN 8
/* D9	PB1/13	PCINT1/PWM          */ #define CHANNEL1_OUT_PIN 9
/* D10	PB2/14	PCINT2/PWM          */ #define CHANNEL2_OUT_PIN 10
/* D11	PB3/15	PCINT3/MOSI/PWM     */ #define CHANNEL3_OUT_PIN 11
/* D12	PB4/16	PCINT4/MISO         */ #define PWMMODE_PIN 12
/* D13	PB5/17	PCINT5/SCK (green LD */ #define LED_PIN 13
/* A0	PC0/23	PCINT8/ADC0         */ #define BUTTON1_PIN A0
/* A1	PC1/24	PCINT9/ADC1         */ #define BUTTON2_PIN A1
/* A2	PC2/25	PCINT10/ADC2        */ #define BUTTON3_PIN A2
/* A3	PC3/26	PCINT11/ADC3        */ #define BUTTON4_PIN A3
/* A4	PC4/27	PCINT12/ADC4/SDA    */
/* A5	PC5/28	PCINT13/ADC5/SCL    */
/* 	PC6/29	PCINT14/Reset           */
/* A7	22	ADC7                    */
/* A6	19	ADC6                    */
/*                                  */
// Note that Channel 4 output uses a different pin for PWM than it does for TRIAC mode


// The module can operate as a 4 channel PWM (low voltage LED lighting) or
// as a 4 channel leading edge TRIAC controller.
// If PWMMODE_PIN is pulled low it is TRIAC controller, otherwise PWM
//
// Each of the 4 channels can be a dimmer or just ON/OFF and has individual control
// Pins nNODIMMING?_PIN disable dimming if tied low


// the number of lighting channels is effectively hardcoded because of the
// capability of the IO pins above
#define LIGHTING_CHANNELS 4


// define the max number of moods we can store in total. Each mood holds a setting for all
// lighting channels
#define MAX_MOODS 5

// define the value in EEPROM that means no mood is stored
#define EMPTYMOOD 0xFF

//sets maximum number of pairings which can be held for all 4 channels in the device
// this could be shared equally or all belong to one channel, doesn't matter
#define RX_MAXPAIRS 20
// the number of bytes needed to store a pair in EEPROM
#define BYTES_PER_PAIR 8

//=============================================
// define the eeprom addresses

//define default EEPROMaddr to location to store message addr
#define EEPROM_ADDR_DEFAULT 0

// the pairings are stored next. There is a byte for number of pairs and then
// BYTES_PER_PAIR bytes per pair.
#define  PAIRS_EEPROMaddr EEPROM_ADDR_DEFAULT

// Then the moods
#define MOOD_EEPROMaddr EEPROM_ADDR_DEFAULT + 1 + (RX_MAXPAIRS * BYTES_PER_PAIR)


#endif

