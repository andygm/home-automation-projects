/*
 *
 *
 * 
 *
 * 
 *
 *
 *
 *
 *
 *
 * 
 *
 * 
 * 
	Last change: AC 05/04/2016 18:29:04
 */
#include <MySensorapc.h>
#include "myglobals.h"
#include <SPI.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <Time.h>
#include <Wire.h>  // I2C library
#include <SSD1306_textapc.h> // the text vewrsion only for the OLED display
#include "buttons.h"
#include "menus.h"
#include <avr/pgmspace.h>

extern boolean checkUpToDateInfo(unsigned int numseconds);

extern boolean hasMainInfoChanged(void);
extern void sendInfoToDomoticz(void);
extern void timestamp(void);
extern MySensor gw;
SSD1306_text oled;
extern boolean radioLostContact;
extern unsigned char getFlag1(void);
extern void putFlag1(unsigned char value);
extern unsigned char getFlag2(void);
extern void putFlag2(unsigned char value);
extern void setRoomOn(boolean roomOn);
extern void setHouseOn(boolean houseOn);
extern boolean getHouseOn(void);
extern boolean getRoomOn(void);
extern void getRoomName(char *dest);
extern int getHouseTime(void);
extern void putHouseTime(int value);
extern int getRoomTime(void);
extern void putRoomTime(int value);
extern int getSetPoint(void);
extern void putSetPoint(int value);
extern boolean getBoilerOn(void);
extern boolean getRadiatorOn(void);


// max imum length assuming type two characters, 1 pixel spacing
#define OLED_LINE_LEN 128/ ((5 *2) + 1)


// Data store
float setPoint;  // in deg C * 10
float localTemp;
unsigned int roomOverrideTime_m; // override time in mins
unsigned int houseOverrideTime_m; // override time in mins
boolean roomOccupied;
boolean houseOccupied;

static unsigned char displayTimer10ths;
static boolean displayTimerTriggered;
char buff[OLED_LINE_LEN * 3]; // temp buff for all display strings 4 lines worth

static unsigned char scrolltimestamp;
static unsigned char scrolloffset;

char scrollbuff[(OLED_LINE_LEN * 3) + 2];
static void setScrollingLine(char *string, char line)
{
    scrollbuff[0] = line;
    strcpy(&scrollbuff[1], string);
    // force immediate update
    scrolltimestamp = (unsigned char)((unsigned char)countTenths - 20);
    scrolloffset = 1;

}
static void stopScrollingLine(void)
{
    scrolloffset = 0;
}
static void handleScrollingLine(void)
{

    if ( scrolloffset != 0 )
    {
        if ( (unsigned char)((unsigned char)countTenths - scrolltimestamp) > 14 )
        {
            scrolltimestamp = (unsigned char)countTenths;
            memcpy(buff,&scrollbuff[scrolloffset],OLED_LINE_LEN);
            buff[OLED_LINE_LEN] ='\0';

            oled.setCursor(scrollbuff[0],0);
            oled.write(buff);
            if ( scrolloffset == 1 )
            {
                scrolloffset = strlen(scrollbuff) - (OLED_LINE_LEN);
            }
            else
            {
                scrolloffset = 1;
            }
        }
    }
}
void handleDisplayTimer10ths(void)
{
    if ( displayTimer10ths > 0 )
    {
        handleScrollingLine();
        if (--displayTimer10ths == 0)
        {
            displayTimerTriggered = true;
        }
    }
}
static void putOledRoomState(void)
{
    if (getRoomOn())
    {
        sprintf_P(buff,PSTR("Occupied\n"));
    }
    else
    {
        sprintf_P(buff,PSTR("UN-occupied\n"));
    }
    oled.write(buff);
}
static void  putOledHouseTime(unsigned char ypos)
{
    oled.setCursor(ypos, 0);
    oled.setTextSize(2, 1);       // 2X character size, spacing 1 pixels

    if ( getHouseTime() == 0 )
    {
        sprintf_P(buff,PSTR("continuous"));
    }
    else
    {
        sprintf_P(buff,PSTR("for %2d:%02dh"),getHouseTime()/60, getHouseTime()%60);
    }
    oled.write(buff);
}

static void putOledOnOff(boolean on)
{
    if ( on )
    {
        sprintf_P(buff,PSTR("ON"));
    }
    else
    {
        sprintf_P(buff,PSTR("OFF"));
    }
    oled.write(buff);
}

static void putOledSetpoint(boolean edittime)
{
    oled.setTextSize(2, 1);       // 2X character size, spacing 2 pixels
    oled.setCursor(6, 0);
    if ( !getRoomOn() )
    {
        sprintf_P(buff,PSTR("UN-occupied"));
    }
    else if (getRoomTime() == 0)
    {
        if ( edittime )
        {
            oled.setCursor(4, 0);
            sprintf_P(buff,PSTR("Override\nis OFF"));
        }
        else
        {
            sprintf_P(buff,PSTR("Set@ %2d.%dC"),getSetPoint()/10, getSetPoint()%10);
        }
    }
    else
    {
        sprintf_P(buff,PSTR("%d:%02dh @%2d.%dC"),
                            getRoomTime()/60, getRoomTime()%60,
                            getSetPoint()/10, getSetPoint()%10);
    }
    oled.write(buff);
}
static void putOledHouseState(boolean change)
{
    sprintf_P(buff,PSTR("House heat"));
    oled.write(buff);
    oled.setCursor(2, 0);
    oled.write("is:");
    oled.setCursor(2, 42);
    oled.setTextSize(4, 4);       // 4X character size, spacing 4 pixels
    putOledOnOff(getHouseOn());
    if ( change )
    {
        oled.setTextSize(2, 1);
        oled.setCursor(6, 0);
        oled.write("change +/-");
    }
    else
    {
        putOledHouseTime(6);
    }
}


typedef enum
{
    MP_BLANK_SCREEN,
    MP_REVISION_SCREEN,
    MP_CHECK_RADIO,
    MP_CHECK_DATA,
    MP_MAIN_SCREEN,
    MP_OVERRIDE_SCREEN,
    MP_HOUSEHEAT_SCREEN,
    MP_INFO_SCREEN,
    MP_EDIT_OVERRIDE_TIME_SCREEN,
    MP_EDIT_OVERRIDE_TEMP_SCREEN,
    MP_OVERRIDE_SUMMARY_SCREEN,
    MP_EDIT_HOUSEHEAT_SCREEN,
    MP_EDIT_HOUSEHEAT_TIME_SCREEN,
    MP_HOUSEHEAT_SUMMARY_SCREEN,
    MP_ROOM_OCCUPIED_SUMMARY_SCREEN,
    MP_SHOW_ROOM_OCCUPIED_SCREEN,
    MP_EDIT_ROOM_OCCUPIED_SCREEN

}t_MenuPage;
static t_MenuPage menuPage;
static t_MenuPage nextMenuPage;





// This is called to display or refresh the page. it returns busy status
// if busy it needs to be regularly refreshed
// It will need to be called for the following events:
//      if the display timeout has run down
//      A button event has occurred
//      A data change has occurred







//============================================================
static void displayPowerDown(void)
{
     oled.sendCommand(SSD1306_DISPLAYOFF);

}

//============================================================
static void displayPowerUp(void)
{
     oled.sendCommand(SSD1306_DISPLAYON);
     delay(20); //
}
//=============================================



void menuInit(void)
{ 
    if ( (true) )
//    if (isDisplayPresent() == true)
    {
        oled.init();
        menuPage = MP_INFO_SCREEN;
        // frig a button press to get you to this screen
        savedButtons = SHORTBUTTONPRESSMASK;
    }
}

boolean changesmade = false;

t_MenuPage nextPage;

boolean menuPageTick(void)
{
    int calc;
    boolean refreshscreen = false;
    #define DEFAULT_SCREEN_TIMEOUT10ths 60
    boolean repeat=true;
    char apcctr = 0;
    t_MenuPage oldpage;
    unsigned int displayTimerUpdate;
    boolean busystatus = false;
    while ( repeat )
    {

        displayTimerUpdate = DEFAULT_SCREEN_TIMEOUT10ths;
        oldpage = menuPage;
        repeat = false;
        switch ( menuPage )
        {
            case MP_BLANK_SCREEN :
                // only button presses have any effect here
                if ( (savedButtons & (SHORTBUTTONPRESSMASK | LONGBUTTONPRESSMASK) ) != 0 )
                {
                    //timestamp(); Serial.printf("Button pressed\n\r");
                    savedButtons = 0;
                    menuPage = MP_CHECK_RADIO;
                    displayPowerUp();
                    checkUpToDateInfo(2); // request new data if its older than 2 seconds
                    hasMainInfoChanged();
                    repeat = true;
                }
                else if ( displayTimerTriggered || refreshscreen)
                {
                    oled.clear();
                    //timestamp();   Serial.printf("Display power down\n\r");
                    displayPowerDown();
                    displayTimerUpdate = 0;
                }
                break;
            case MP_CHECK_RADIO :
                if ( radioLostContact )
                {
                    //Serial.printf("Lost contact\n\r");
                    // quick press of button takes you to
                    // long press takes you to
                    // timeout takes you back to blank screen
                    if ( (savedButtons != 0 )|| displayTimerTriggered)
                    {
                        menuPage = MP_MAIN_SCREEN;
                        repeat = true;
                    }
                    else if (refreshscreen)
                    {
                        oled.write("Lost contact\nwith base\nstation");
                    }
                }
                else
                {
                    menuPage = MP_MAIN_SCREEN;
                    repeat = true;
                }

                break;
            case MP_MAIN_SCREEN :
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    nextPage = getRoomOn()?MP_OVERRIDE_SCREEN:MP_HOUSEHEAT_SCREEN;
                    menuPage = MP_CHECK_DATA;
                    repeat = true;
                }
                else if ( (savedButtons & LONGBUTTONPRESSMASK ) != 0 )
                {
                    nextPage = getRoomOn()?MP_EDIT_OVERRIDE_TEMP_SCREEN:MP_EDIT_ROOM_OCCUPIED_SCREEN;
                    menuPage = MP_CHECK_DATA;
                    repeat = true;
                }
                else if (displayTimerTriggered)
                {
                    menuPage = MP_BLANK_SCREEN;
                    repeat = true;
                }
                else if ( hasMainInfoChanged() )
                {
                    repeat = true;
                    oldpage = MP_CHECK_DATA;// force a redraw
                }
                else if (refreshscreen)
                {
                    displayTimerUpdate = 90; // stay longer on this screen
                    // one or more bits of info on this screen have changed, refresh
                    getRoomName(buff);
                    oled.write(buff);

                    // display local temperature
                    oled.setCursor(2, 0);
                    oled.setTextSize(4, 4);       // 4X character size, spacing 4 pixels
                    calc = lastTemperature * 10;
                    sprintf_P(buff,PSTR("%d.%d"),(calc/10),(calc%10));
                    oled.write(buff);
                    oled.setTextSize(2, 4);       // 2X character size, spacing 4 pixels
                    oled.write('0');
                    oled.setTextSize(4, 4);       // 4X character size, spacing 4 pixels
                    oled.write("C");
                    putOledSetpoint(false);

                }
                break;
            case MP_CHECK_DATA :
                if ( nextPage != MP_BLANK_SCREEN )
                {
                    // we are awaiting data refresh before we move on
                    if (checkUpToDateInfo(8))
                    {
                        menuPage = nextPage;
                        nextPage = MP_BLANK_SCREEN;
                        repeat = true;
                    }
                    else
                    {
                        if (refreshscreen)
                        {
                            sprintf_P(buff,(PSTR("Waiting for\ndata from\nbase station")));
                            oled.write(buff);
                        }
                        else if ( displayTimerTriggered )
                        {
                             menuPage = MP_BLANK_SCREEN;
                            nextPage = MP_BLANK_SCREEN;
                        }
                    }
                }
                break;

            case MP_REVISION_SCREEN :
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    menuPage = MP_CHECK_RADIO;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    oled.write(REVISIONSTRING);
                }
                else if ( displayTimerTriggered )
                {
                    menuPage = MP_BLANK_SCREEN;
                    repeat = true;
                }
                break;

            case MP_OVERRIDE_SCREEN :
                // quick press of button takes you to MP_HOUSEHEAT_SCREEN
                // long press takes you MP_EDIT_OVERRIDE_TIME_SCREEN
                // timeout takes you back to blank screen
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    menuPage = MP_HOUSEHEAT_SCREEN;
                    repeat = true;
                }
                else if ( (savedButtons & LONGBUTTONPRESSMASK ) != 0 )
                {
                    menuPage = MP_EDIT_OVERRIDE_TEMP_SCREEN;
                    repeat = true;
                }
                else if (displayTimerTriggered)
                {
                    menuPage = MP_BLANK_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    calc = getRoomTime();
                    sprintf_P(buff,PSTR("Override is"));
                    oled.write(buff);
                    oled.setCursor(2, 0);
                    oled.setTextSize(4, 4);       // 4X character size, spacing 4 pixels
                    putOledOnOff(calc > 0);
                    if (calc > 0)
                    {
                        oled.setCursor(2, 68);
                        oled.setTextSize(3, 2);       // 3X character size, spacing 2 pixels
                        sprintf(buff,"for");
                        oled.write(buff);
                        // displaying ovveride time/temp
                        putOledSetpoint(false);
                    }
                    else
                    {
                        putOledSetpoint(false);
                    }
                }
                break;
            case MP_HOUSEHEAT_SCREEN :
                // quick press of button takes you to MP_HOUSEHEAT_SCREEN
                // long press takes you MP_EDIT_OVERRIDE_TIME_SCREEN
                // timeout takes you back to blank screen
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    menuPage = MP_SHOW_ROOM_OCCUPIED_SCREEN;
                    repeat = true;
                }
                else if ( (savedButtons & LONGBUTTONPRESSMASK ) != 0 )
                {
                    menuPage = MP_EDIT_HOUSEHEAT_SCREEN;
                    repeat = true;
                }
                else if (displayTimerTriggered )
                {
                    menuPage = MP_BLANK_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    putOledHouseState(false);
                }
                break;
            case MP_INFO_SCREEN :
                // quick press of button takes you to MP_HOUSEHEAT_SCREEN
                // long press takes you MP_EDIT_OVERRIDE_TIME_SCREEN
                // timeout takes you back to blank screen
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    menuPage = MP_REVISION_SCREEN;
                    repeat = true;
                }
                else if ( (savedButtons & LONGBUTTONPRESSMASK ) != 0 )
                {
                }
                else if (displayTimerTriggered)
                {
                    menuPage = MP_BLANK_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    sprintf_P(buff,PSTR("Boiler: "));
                    oled.write(buff);
                    putOledOnOff(getBoilerOn());

                    oled.setCursor(2, 0);
                    sprintf_P(buff,PSTR("Rad'tor: "));
                    oled.write(buff);
                    putOledOnOff(getRadiatorOn());

                    oled.setCursor(4, 0);
                    sprintf_P(buff,PSTR("Battery %d%%"),oldbatteryPercent);
                    oled.write(buff);
                    oled.setCursor(6, 0);
                    sprintf_P(buff,PSTR("Id= 0x%02x"), gw.getNodeId());
                    oled.write(buff);
                }
                break;
            case MP_EDIT_OVERRIDE_TEMP_SCREEN :
                // quick press of button takes you to
                // long press takes you to
                // timeout takes you back to blank screen
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    int jump = 10;
                    getRoomName(buff);
                    if ( memcmp(buff,"HotWater",8 ) == 0)
                    {
                        jump = 25;
                    }

                    //Serial.printf("APC Hot water %d\n\r",jump);
                    if (getRoomTime() == 0)
                    {
                        // override is OFF - so starting point is the
                        // current room temperature
                        calc = lastTemperature * 10;
                        //Serial.printf("ROOMtime 0 - using current temp %d\n\r", calc);
                    }
                    else
                    {
                        calc = getSetPoint();
                    Serial.printf("ROOMtime Non zero - using current setp %d\n\r", calc);
                    }
                    if (getRoomTime() < 60)
                    {
                        // set room time to min of 60 mins
                        putRoomTime(60);
                    }

                    if (((savedButtons >> SHORTBUTTONBITSHIFT) & 0x01) != 0)
                    {
                        calc +=jump;
                    }
                    else
                    {
                        calc -=jump;
                    }
                    if ( jump >20 )
                    {
                        // hot water zone
                        if ( calc < 400 ) calc = 400;
                        if ( calc > 650 ) calc = 650;
                    }
                    else
                    {
                        if ( calc < 120 ) calc = 120;
                        if ( calc > 250 ) calc = 250;
                    }
                    putSetPoint(calc);
                    repeat = true;
                    changesmade = true;
                    oldpage = MP_MAIN_SCREEN;// force a redraw
                }
                else if ( (savedButtons & LONGBUTTONPRESSMASK ) != 0 )
                {
                    menuPage = MP_EDIT_OVERRIDE_TIME_SCREEN;
                    repeat = true;
                }
                else if (displayTimerTriggered)
                {
                    menuPage = MP_OVERRIDE_SUMMARY_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    oled.write("+/- change\nTEMPERATURE");
                    setScrollingLine("Long press  edits TIME", 4);
                    putOledSetpoint(false); // bottom line
                }
                break;
            case MP_SHOW_ROOM_OCCUPIED_SCREEN :
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    repeat = true;
                    menuPage = MP_INFO_SCREEN;
                }
                else if ( ((savedButtons & LONGBUTTONPRESSMASK ) != 0) || displayTimerTriggered)
                {
                    menuPage = MP_EDIT_ROOM_OCCUPIED_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    sprintf_P(buff,PSTR("ROOM is:\n"));
                    oled.write(buff);
                    putOledRoomState();
                }
                break;

            case MP_EDIT_ROOM_OCCUPIED_SCREEN :
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    setRoomOn(!getRoomOn());
                    changesmade = true;
                    repeat = true;
                    oldpage = MP_MAIN_SCREEN;// force a redraw
                }
                else if ( ((savedButtons & LONGBUTTONPRESSMASK ) != 0) || displayTimerTriggered)
                {
                    menuPage = MP_ROOM_OCCUPIED_SUMMARY_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    sprintf_P(buff,PSTR("Edit ROOM\n"));
                    oled.write(buff);
                    putOledRoomState();
                    sprintf_P(buff,PSTR("+/- change"));
                    oled.write(buff);
                }
                break;

            case MP_EDIT_OVERRIDE_TIME_SCREEN :
                // quick press of button takes you to
                // long press takes you to
                // timeout takes you back to blank screen
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    #define ROOM_TIME_STEP 30  // round to nearest x minutes
                    calc = ((getRoomTime()+ROOM_TIME_STEP/2)/ROOM_TIME_STEP)*ROOM_TIME_STEP;

                    if (((savedButtons >> SHORTBUTTONBITSHIFT) & 0x01) != 0)
                    {
                        calc += ROOM_TIME_STEP;
                    }
                    else
                    {
                        if ( calc >= ROOM_TIME_STEP )
                        {
                            calc -= ROOM_TIME_STEP;
                        }
                        else
                        {
                            calc = 0;
                        }
                    }
                    if ( calc > 60 *8 ) calc = 60 * 8;
                    putRoomTime(calc);
                    changesmade = true;
                    repeat = true;
                    oldpage = MP_MAIN_SCREEN;// force a redraw
                }
                else if ( ((savedButtons & LONGBUTTONPRESSMASK ) != 0) || displayTimerTriggered)
                {
                    menuPage = MP_OVERRIDE_SUMMARY_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    oled.write("+/- change\nTIME");
                    // changing override time
                    putOledSetpoint(true); // bottom line
                }
                break;
            case MP_ROOM_OCCUPIED_SUMMARY_SCREEN :
            case MP_OVERRIDE_SUMMARY_SCREEN :
            case MP_HOUSEHEAT_SUMMARY_SCREEN :
                // if changes have been made - show brief changes accepted screen and then go back
                // to next sensible screen
                // to override screen. Else go to blank screen
                if ( displayTimerTriggered)
                {
                    if ( menuPage == MP_OVERRIDE_SUMMARY_SCREEN )
                    {
                        menuPage = MP_CHECK_RADIO;
                    }
                    else if (menuPage == MP_HOUSEHEAT_SUMMARY_SCREEN  )
                    {
                        menuPage = MP_HOUSEHEAT_SCREEN;
                    }
                    else
                    {
                        menuPage = MP_CHECK_RADIO;
                    }
                    repeat = true;
                }
                else if ( refreshscreen)
                {
                    if ( changesmade )
                    {
                        // last thing. Override the house timer with 0
                        if ( getHouseOn() )
                        {
                            putHouseTime(0);
                        }
                        if (!getRoomOn())
                        {
                            putRoomTime(0);
                        }
                        oled.write("Changes\nsaved");
                        sendInfoToDomoticz();
                        changesmade = false;
                        displayTimerUpdate = 15;
                    }
                    else
                    {
                        menuPage = MP_BLANK_SCREEN;
                        repeat = true;
                    }
                }
                break;
            case MP_EDIT_HOUSEHEAT_SCREEN :
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    setHouseOn(!getHouseOn());
                    changesmade = true;
                    repeat = true;
                    oldpage = MP_MAIN_SCREEN;// force a redraw
                }
                else if ( ((savedButtons & LONGBUTTONPRESSMASK ) != 0) || displayTimerTriggered)
                {
                    if ( getHouseOn() )
                    {
                        menuPage = MP_HOUSEHEAT_SUMMARY_SCREEN;
                    }
                    else
                    {
                        menuPage = MP_EDIT_HOUSEHEAT_TIME_SCREEN;
                    }
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    putOledHouseState(true); // 4 size chars on row 2
                }
                break;
            case MP_EDIT_HOUSEHEAT_TIME_SCREEN :
                if ( (savedButtons & SHORTBUTTONPRESSMASK ) != 0 )
                {
                    #define HOUSE_TIME_STEP 60  // round to nearest x minutes
                    calc = ((getHouseTime()+HOUSE_TIME_STEP/2)/HOUSE_TIME_STEP)*HOUSE_TIME_STEP;

                    if (((savedButtons >> SHORTBUTTONBITSHIFT) & 0x01) != 0)
                    {
                        calc += HOUSE_TIME_STEP;
                    }
                    else
                    {
                        if ( calc >= HOUSE_TIME_STEP )
                        {
                            calc -= HOUSE_TIME_STEP;
                        }
                        else
                        {
                            calc = 0;
                        }
                    }
                    if ( calc > 60 *12 ) calc = 60 * 12; // 12 hours max
                    putHouseTime(calc);
                    changesmade = true;
                    repeat = true;
                    oldpage = MP_MAIN_SCREEN;// force a redraw
                }
                else if ( ((savedButtons & LONGBUTTONPRESSMASK ) != 0) || displayTimerTriggered)
                {
                    menuPage = MP_HOUSEHEAT_SUMMARY_SCREEN;
                    repeat = true;
                }
                else if (refreshscreen)
                {
                    oled.write("+/- change\nHouse\nOFF time");
                    putOledHouseTime(6); // line 6
                }
                break;


            default :
                break;
        }
        if ( refreshscreen )
        {
            displayTimer10ths = displayTimerUpdate;
        }
        if ( oldpage != menuPage)
        {
            //timestamp();  Serial.printf("Change menu page - clear all old %d new %d\n\r",oldpage,menuPage);
            stopScrollingLine();
            savedButtons = 0;
            oled.clear();
            refreshscreen  = true;
            oled.setCursor(0, 1);
            oled.setTextSize(2, 1);       // 2X character size, spacing 1 pixels
            displayTimer10ths = displayTimerUpdate;
            displayTimerTriggered = false;
        }
    }
    displayTimerTriggered = false;

    if (menuPage != MP_BLANK_SCREEN || displayTimer10ths != 0)
    {
        busystatus = true;
    }
    return busystatus;
}



