/** @package 

    rfmessages.h
    
    Copyright(c) Microsoft 2000
    
    Author: Andrew Copsey
    Created: AC  14/02/2016 12:31:37
	Last change: AC 15/02/2016 19:24:11
*/
#ifndef __rfmessages_h
#define  __rfmessages_h

extern void actionRadioStoreCommand(void);



#endif //  __rfmessages_h
