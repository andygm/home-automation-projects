This eight channel PWM dimmer is based on Arduino MiniPro code rev 0.04 described in the accompanying docs. 

Two arduinos are used, one operting as a 4 chanel PWM dimmer and the other as a 4 channel TRIAC mains voltage dimmer. They share a 433.9MHz radio receiver

The schematic was captured using Altium Designer and built using veroboard and small plastic enclosure.

The schematics and BOMs can be found in the project outputs sub directory

APC 8th April 2016
