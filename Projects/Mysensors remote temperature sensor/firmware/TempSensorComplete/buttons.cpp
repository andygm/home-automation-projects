/* This sketch is a battery power temperature sensor fitted with OLED display and buttons to control setpoint.
 * It takes around 12uA from a 3v battery, wakes up every so often to check the temperature. If it has changed or it hasn't phoned home for a long while it sends
 * an update of temperature and requests the latest info back - via a text sensor message.
 * 
 * If any button is pressed it wakes up and turns on the OLED display for a few seconds. Any button press also requests an text update from the gateway. 
 * 
 * Domoticz responds to the text request with a multi line string, comma seperated. Nomrally this woudl be 
 *  17.2,60,Kitchen      which is setpoint, timer in minutes and zone name
 *  
 *  Domoticz responds to the plus and minus buttons by operating the BOOST+ or BOOST- functions on this zone.
 * Button presses are sent to Domoticz, the plus button is normally handled as a boost + function (increase setpoint by 1 deg for 60 mins) 
 * The minus button decreases setpoint by 1 degree for 60minutes
 * 
 * The gateway is a serial gateway on arduino connected to a RPI running Domoticz.
 * 
 * 
	Last change: AC 05/04/2016 18:29:32
 */
#include <MySensorapc.h>
#include "myglobals.h"
#include <SPI.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <Time.h>
#include <Wire.h>  // I2C library
#include <SSD1306_textapc.h> // the text vewrsion only for the OLED display
#include "buttons.h"

extern MySensor gw;
extern void timestamp(void);

// Initialize messages for sensor network
MyMessage textMsg( 0, V_TEXT );

extern void requestInfoUpdate(void);

// extern flah in atmega hardware timers
extern int8_t pinIntTrigger;

//#include <PciManager.h> // button interrupts etc
// #include <PciListenerImp.h>

#define KEYTICKmS 5
#define MIN_DEBOUNCEms 50
#define LONG_DEBOUNCEms 800

extern MySensor gw;





static unsigned long buttonmillis;
static unsigned char newButtons = 0, oldButtons = 0, debounceCtr = 0;
unsigned char savedButtons;

#ifdef DEBUG_MYSENSORS_PERF
extern int apctx;
extern int apcrx;
#endif

bool buttonsPressed(void)
{
    return (newButtons != 0);
}

//apczzz
unsigned char getbuttons(void)
{
    unsigned char status = savedButtons;
    savedButtons = 0;
    return status;
}

void buttonsProcess(void)
{
    if ( (elapsedMillis -buttonmillis) > KEYTICKmS )
    {

//    Serial.printf("ButtonsMS %u elapsed milis %u, debou ct %d\n\r",buttonmillis, elapsedMillis , debounceCtr);
        // every KEYTICKmS
        buttonmillis = elapsedMillis;
        newButtons = (digitalRead(PIN_BUTTON1)? 0x0:0x01) | (digitalRead(PIN_BUTTON2)? 0x0:0x02);
        if ( newButtons != 0 )
        {
            stayAwake = 10;
        }
        if ( newButtons != oldButtons )
        {
            debounceCtr = 0;
            oldButtons = newButtons;
        }
        else
        {
            if ( debounceCtr < LONG_DEBOUNCEms/KEYTICKmS )
            {
                stayAwake = 10;
                if (++debounceCtr == MIN_DEBOUNCEms/KEYTICKmS)
                {
                    if ( newButtons != 0)
                    {
                        //timestamp();   Serial.printf("Button down %d\n\r",newButtons);

                        // a button down has just stabilised - short press
                        savedButtons =  (newButtons << BUTTONDOWNBITSHIFT);
                    }
                    else
                    {
                        //all buttons up has just stabilised - send both buttons up message
                        // timestamp();  Serial.printf("Buttons up\n\r");
                        if ( (savedButtons & BUTTONDOWNMASK) != 0 )
                        {
                            // we have just released a button after a short press, signal the short press
                            savedButtons >>= BUTTONDOWNBITSHIFT - SHORTBUTTONBITSHIFT;
                        }
//                        gw.send(msgButton1.set(0));
//                        gw.send(msgButton2.set(0));
//                        gw.send(textMsg.setSensor(TEXT_CHILD_ID).set("andy woz ere"));

                    }
                }
                else if (debounceCtr == LONG_DEBOUNCEms/KEYTICKmS)
                {
                    if ( newButtons != 0)
                    {
                        // timestamp();   Serial.printf("Long Button down %d\n\r",newButtons);
                        // a button down has just stabilised - LONG press
                        savedButtons = (newButtons <<  LONGBUTTONBITSHIFT) | (newButtons << BUTTONDOWNBITSHIFT);
                    }
                    //actionButtonDown(true);
                }

            }
        }
    }
}

void enableButtonInterrupts(void)
{
    // button1 D7 + is INT23
    // button2 D8 - is INT0
    // button3 D6 is INT22
    // PCMSK2 // 16-23  a 1 enables pin change
    // PCMSK1 // 8-14
    // PCMSK0 // 0-7
    // PCICR // bit 0 enables 0-7, bit 1 8-14, bit 2 16-23

    cli();

    PCIFR = 0x7; // clear any pending
    PCMSK2 |= 0b10000000; // unmask int23
    PCMSK0 |= 0b00000001; // unmask int0
    PCICR |= 0b00000101; // bit 0 enables 0-7, bit 1 8-14, bit 2 16-23
    sei();		// turn interrupts back on
}

static void disableButtonInterrupts(void)
{
    cli();
    PCICR &= ~0b00000101; // bit 0 enables 0-7, bit 1 8-14, bit 2 16-23
    sei();		// turn interrupts back on
}
#ifdef apczzz
void InitialiseInterrupt(){
  cli();		// switch interrupts off while messing with their settings  
  PCICR =0x02;          // Enable PCINT1 interrupt
  PCMSK1 = 0b00000111;
  sei();		// turn interrupts back on
}
#endif

ISR(PCINT0_vect)
{    // Interrupt service routine. Every single PCINT8..14 (=ADC0..5) change
            // will generate an interrupt: but this will always be the same interrupt routine
    disableButtonInterrupts();
    pinIntTrigger =1;
}
ISR(PCINT2_vect)
{    // Interrupt service routine. Every single PCINT8..14 (=ADC0..5) change
     // will generate an interrupt: but this will always be the same interrupt routine
    disableButtonInterrupts();
    pinIntTrigger =1;
}



void buttonsInit(void)
{
    // now present the buttons
    pinMode(6,INPUT_PULLUP);
    pinMode(7,INPUT_PULLUP);
    pinMode(8,INPUT_PULLUP);
    enableButtonInterrupts();
}


