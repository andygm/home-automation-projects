/** @package 

    menus.h
    
    Copyright(c) Microsoft 2000
    
    Author: Andrew Copsey
    Created: AC  17/03/2016 18:14:19
	Last change: AC 24/03/2016 19:36:17
*/
#ifndef __menus_h
#define  __menus_h
#include "myglobals.h"





extern void handleDisplayTimer10ths(void);
extern void menuInit(void);
extern boolean menuPageTick(void);

#endif

