--[[
This script got from:
http://www.domoticz.com/forum/viewtopic.php?f=24&t=4190&sid=a951bf47b807b7ac87312762344a1a5a&start=40
Heating Coordinator Script (http://www.domoticz.com/forum/viewtopic.php?f=24&t=4190)
==========================


This is a devics script

Three scripts make up this functionality together with csv files containing the temperature profiles
a) script_time_heatingcoordinator.lua
b) This one: script_device_boostchange.lua
c) Utils scripts in zzutils.lua

For full documentation Refer to APC document "Heating Coordinator Manual.docx" REv 1.00 or later. Dated 28/3/2016

Coppo. 28/3/2016  

]]


local scriptRevision = "Devices V1.02 2/4/2016 APC"

-- where the lua utility scripts are kept
package.path = package.path .. ';' .. '/home/pi/domoticz/scripts/lua/?.lua' 
luautils = require("luautils")

-- Include some util fuctions - declared as locals to improve speed
local logtext = luautils.logtext
local issueDeviceUpdate = luautils.issueDeviceUpdate
local getFirstDeviceSvalue = luautils.getFirstDeviceSvalue
local testflag = luautils.testflag
local splitUpString = luautils.splitUpString
local getIdxList = luautils.getIdxList
local updateInfoString = luautils.updateInfoString
local issueURLCommand = luautils.issueURLCommand
local getSecondsSinceUpdate = luautils.getSecondsSinceUpdate
local heatingProfileVariableName = "HCN_HeatingProfile"
--local getLastUpdatedTime = luautils.getLastUpdatedTime

local DomoticzIP="192.168.1.170"
local DomoticzPort="8080"

--globals - also referred to in scripts so cannot be locals
MySensorsDelayVariableName = "HCN_MySensorsDelay"
hcPrefix = uservariables["HCN_NamePrefix"]
hotWaterZoneName = "HotWater"
updateAllIDXDeviceName = "UpdateAllIds"
heatingProfileSet = uservariables[heatingProfileVariableName]
function updateAllIdx(myDeviceName)
  local DeviceID
  local DeviceFileName = "/var/tmp/apc1AllDevDat.tmp"
  local DeviceList =''
  local InfoDeviceList =''
  local handle
  
  if devicechanged[myDeviceName] =='On' then
    
    commandArray['SendNotification']='Domoticz: Scanning new ids - will take 20-30 seconds to update'
    -- check this...
    --https://www.domoticz.com/forum/viewtopic.php?f=28&t=262 
  
    -- Update the list of device names and ids to be checked later
    --curl -s -i -H -f -0 --max-time 2 --connect-timeout 2 - add ending & so it is non blocking
    os.execute("curl 'http://" .. DomoticzIP .. ":" .. DomoticzPort .. "/json.htm?type=devices&order=name' 2>/dev/null |   /usr/local/bin/jq -r '.result[]|{(.Name): .idx}' >" .. DeviceFileName.."&")
    commandArray[myDeviceName] = 'Off AFTER 20'
  
  else
    commandArray['SendNotification']='Domoticz: Scanning ids is complete'
    print(string.format("updateAllIdx: Searching through %d heating zones",#heatingZones))

    for hZIndex = 1, #heatingZones do
  
      DeviceName = hcPrefix..'_'..heatingZones[hZIndex]..'_'..statSuffix
      
      print(string.format("\n========Index "..hZIndex.." Name is "..DeviceName)  )
  
       handle = io.popen("/usr/local/bin/jq -r '.\"" .. DeviceName .. "\" | values' " .. DeviceFileName)
       DeviceID = handle:read("*n")
       handle:close()
      if DeviceID == nil then
        DeviceID = 0
        print(string.format("updateAllIdx: ERROR:Device: %s NOT FOUND",DeviceName))
      else
        print(string.format("updateAllIdx: Found %d:%s",DeviceID,DeviceName))
      end
      DeviceList = DeviceList..tonumber(DeviceID)..','
      
      DeviceName = hcPrefix..'_'..heatingZones[hZIndex]..'_Info'
      handle = io.popen("/usr/local/bin/jq -r '.\"" .. DeviceName .. "\" | values' " .. DeviceFileName)
      DeviceID = handle:read("*n")
      handle:close()
       
      if DeviceID == nil then
        print(string.format("updateAllIdx: ERROR:Device: %s NOT FOUND",DeviceName))
        DeviceID = 0
      else
        print(string.format("updateAllIdx: Found %d:%s",DeviceID,DeviceName))
      end
      --logtext(string.format(DeviceID)  )
      --logtext(string.format("zzzz"))
      InfoDeviceList = InfoDeviceList..tonumber(DeviceID)..','
    end
    commandArray["Variable:HCN_Setpoint_idxList"] = DeviceList
    commandArray["Variable:HCN_Info_idxList"] = InfoDeviceList
    print(string.format("updateAllIdx: Success - \nVariable:HCN_Setpoint_idxList= "..DeviceList)  )
    print(string.format("updateAllIdx: Success - \nVariable:HCN_Info_idxList= "..InfoDeviceList)  )
    print(string.format("updateAllIdx Finished\n\r\n")  )
  end
end



--##############################################################################

logtext(string.format("START. %s", scriptRevision)) -- for timing purposes
commandArray = {}
commandArrayIndex = 1


 -- ===================================================================
 -- A bit of DEBUG code to print out which device caused this change
 if uservariables["HCN_LogActivity"] == 1 then
  device = ""
  for i, v in pairs(devicechanged) do
    if (#device == 0 or #i < #device) then device = i end
  end
  print("Triggered by " .. device .. " now " .. otherdevices[device]) 
end
-- end of logging code
-- ===================================================================
 
local mydevice = tostring(next(devicechanged))

-- Break the device name that caused this into sections split with underscore character
--must not be local
deviceNameSplit = splitUpString(mydevice,"_")
  
if deviceNameSplit[1] == hcPrefix then
  -- useful debug print("START of ALL Heating coordinator actions")  

  -- its a heating control device so we are interested in it
  --  heatingZones must be global for some of the utils functions to access it
  heatingZones = splitUpString(uservariables["HCN_Zones"],",")
  -- must not be local
  statSuffix = uservariables["HCN_ThermostatNameSuffix"]
  occupiedSuffix = uservariables["HCN_OccupiedSuffix"]
  local oldHouseState = otherdevices["HC_HouseHeatingOn"]      
  if oldHouseState == nil then
    print("ERROR:Force house heating away")
    oldHouseState = "Off"    
  end
  local newHouseState = oldHouseState
  
  local oldHouseTimer = uservariables["HCN_HouseTimer"]
  if oldHouseTimer == nil then 
    print("ERROR:Force house timer")
    oldHouseTimer = 0
  end
  local newHouseTimer = oldHouseTimer
  --print(string.format("Old house timer %s", oldHouseTimer))
  if (deviceNameSplit[2] == updateAllIDXDeviceName) then
    -- find the ids of the relevent devices that are part of this heating zone
    -- do this after any device changes or additions
    updateAllIdx(mydevice)
  elseif (deviceNameSplit[2] == "HouseHeatingOn") then
    print("House state changed")
    if (newHouseState == 'On') then
      -- house heating has just changed to on - need to zero the house timer
      print("zero house timer")
      newHouseTimer = 0;
    end
  elseif deviceNameSplit[2] == "SetHeatingProfile" then
    if deviceNameSplit[3] =="Holiday" then
      commandArray["Variable:"..heatingProfileVariableName] = "holiday"
    else
      commandArray["Variable:"..heatingProfileVariableName] = "normal"
    end
  elseif  (deviceNameSplit[3] == occupiedSuffix) or
    (deviceNameSplit[3] == statSuffix) or
    (deviceNameSplit[3] == 'Info') then
    logtext("START of Zone related  Heating coordinator actions")  
    -- we are going to fiddle with some values that may need putting back after
    HC_FrostSetting = uservariables["HCN_FrostSetting"]
    timerSuffix = uservariables["HCN_TimerSuffix"]
    radSuffix = uservariables["HCN_RadiatorNameSuffix"]
    -- these must not be local as they are needed by various utils
    zoneSetpointIDX, zoneInfoIDX = getIdxList(deviceNameSplit[2])

    local oldZoneInfo = getFirstDeviceSvalue(hcPrefix..'_'..deviceNameSplit[2]..'_Info')
    if oldZoneInfo == nil then
      oldZoneInfo = 'No zone info'
    end
  
    local oldSetpoint = tonumber(getFirstDeviceSvalue(hcPrefix..'_'..deviceNameSplit[2]..'_'..statSuffix))
    if oldSetpoint == nil then
      print("ERROR: Forcing frost setting");
      oldSetpoint = HC_FrostSetting
    end
    local newSetpoint = oldSetpoint
    
    
    local oldZoneTimer = uservariables[hcPrefix..'_'..deviceNameSplit[2]..'_'..timerSuffix]
    if oldZoneTimer == nil then 
      print("ERROR:Force zone timer")
      oldZoneTimer = 0
    end
    local newZoneTimer = oldZoneTimer
    
  	local oldZoneState = otherdevices[hcPrefix..'_'..deviceNameSplit[2]..'_'..occupiedSuffix]
    if oldZoneState == nil then
      print(string.format("ERROR:Zone %s can't read old state. Force zone off",hcPrefix..'_'..deviceNameSplit[2]..'_'..occupiedSuffix))
      oldZoneState = "Off"    
    end
    local newZoneState = oldZoneState
    if (deviceNameSplit[3] == statSuffix) then
      -- Something - probably android app or web gui has just updated the setpoint. We ONLY want to react to this if
      -- a) The zone timer hasn't been recently set (in the last 10 secs) - as would happen if the remote sensor sent in a boost override request
      -- b) The boost/override zone timer is less than 60 minutes
      local defaultBoostMins = uservariables["HCN_DefaultBoostMins"]

      if defaultBoostMins < 20 then
        defaultBoostMins = 60
      end
      local timeSinceUpdate = getSecondsSinceUpdate ( hcPrefix..'_'..deviceNameSplit[2]..'_'..timerSuffix, false)
      if timeSinceUpdate > 5 and  newZoneTimer < defaultBoostMins then
        newZoneTimer = defaultBoostMins -- lets fix it at 
      local emailString = string.format("%s Dom secs %s name %s, time %s defult %s",os.date("time is %H:%M:%S"),timeSinceUpdate, hcPrefix..'_'..deviceNameSplit[2]..'_'..timerSuffix, newZoneTimer, defaultBoostMins)
      commandArray['SendNotification']= emailString
      print(string.format("Time %s. zzzzzz Overideing timer",os.date("time is %H:%M:%S")))
      end
      
    elseif (deviceNameSplit[3] == occupiedSuffix) then
      -- we have just enabled or disabled the heating on this zone, lets update relevant info
      -- we always zero the zone timer which disables zone override
      newZoneTimer = 0
      -- we also may have changed the setpoint as a result of this so go and recalculate it
      newSetpoint = getSetpoint(deviceNameSplit[2], oldSetpoint, newZoneTimer, newZoneState, newHouseState)

    elseif (deviceNameSplit[3] == 'Info') then
      print("Received info string from temp sensor. Processing it")  
  
      --[[ The remote sensor has sent in a new string - lets deal with it
        a) The zone name may be wrong - check and  overwrite it with the correct name
        b) Setpoint. If
        b) If we are requested to turn ON the zone/room - also accept the Setpoint and zone
        override timer values in the newInfo, otherwise use current Setpoint and zone timer
        c) If we are requested to turn OFF the zone/room. Zero zone timer and put Setpoint to frost setting
        d) If we are requested to turn OFF the house, use the house timer value inthe message
        e) If we are requested to turn ON the house. Zero the house timer
        f) always clear flag2 after using it
      ]]--
      -- get the old zone info string
      
      local flags2 = string.byte (string.sub(oldZoneInfo,23,23)) 
      if flags2 == nil then
        flags2 = 0
      end
      flags2 = flags2 -32
      
      ---= check for any commands from remote sensor in flags2
      if testflag(flags2,4) == true then
        -- disable zone heating
        newZoneState = 'Off'
        newZoneTimer = 0
        newSetpoint = HC_FrostSetting
      elseif testflag(flags2,8) == true then
        -- enable zone heating
        newZoneState = 'On'
      end
      if testflag(flags2,1) == true then
        -- disable house heating
        newHouseState = 'Off'
        newHouseTimer = tonumber(string.sub(oldZoneInfo,16,18))
        if newHouseTimer == 'nil' or newHouseTimer > 900 then
          newHouseTimer = 0
        end
      elseif testflag(flags2,2) == true then
        -- enable house heating
        newHouseState = 'On'
        newHouseTimer = 0
      end
      newZoneTimer = tonumber(string.sub(oldZoneInfo,13,15))
      if newZoneTimer == 'nil' or newZoneTimer > 900 then
        newZoneTimer = 0
      end
      newSetpoint = tonumber(string.sub(oldZoneInfo,19,21))
      print(string.format("Got setpoint %s from string",newSetpoint))
      if newSetpoint == 'nil' or newSetpoint > 300 or newSetpoint < 130 then
        newSetpoint = HC_FrostSetting
      else 
        -- the number is handled as an integer. 230 is 23.0DegC
        newSetpoint = newSetpoint/10
      end
      -- check if we need to overwrite this setpoint for any reason
      newSetpoint = getSetpoint(deviceNameSplit[2], newSetpoint, newZoneTimer, newZoneState, newHouseState)

            
    end  
    -- end of any heating cooordinator changes
    updateInfoString(oldZoneInfo,         --infOldString,
                      1,               -- infFlags2, Clear them as we have reacted to the status
                      deviceNameSplit[2], --infZoneName,
                      zoneInfoIDX, --infZoneInfIDX
                      otherdevices[hcPrefix..'_'..deviceNameSplit[2]..'_'..radSuffix], --infRadiatorState, 
                      otherdevices[uservariables["HCN_BoilerReceiverSwitchName"]], --infBoilerState, 
                      newHouseState,      --infHouseState,
                      newZoneState,       --infZoneState,
                      newZoneTimer,       -- infZoneTimer,
                      newHouseTimer,      --infHouseTimer,
                      newSetpoint)        --infSetPoint
    -- Check for changes and update any of the variables and/or devices that we have changed
  
    if newZoneState ~= oldZoneState then
      print(string.format("Issued zone state update is %s was %s ",newZoneState,oldZoneState)) 
      commandArray[hcPrefix..'_'..deviceNameSplit[2]..'_'..occupiedSuffix] = newZoneState
    end
    if newSetpoint ~= oldSetpoint or newZoneTimer ~= oldZoneTimer then
      if newZoneTimer ~= oldZoneTimer then
        print(string.format("Issue zone timer update was %s is now %s",oldZoneTimer,newZoneTimer))
      end
      -- always update the zone timer so it acts as a timestamp on script updated setpoint
      commandArray["Variable:"..hcPrefix..'_'..deviceNameSplit[2]..'_'..timerSuffix] = tostring(newZoneTimer)
      if newSetpoint ~= oldSetpoint then
        print("Issue set point update was ".. oldSetpoint ..'is '..newSetpoint ..'id= '..zoneSetpointIDX)
        -- device update did not work for thermostat - don't know why. The value changed but the script ran for more than 10 seconds, changed to URLCommand
        -- method instead
        --issueDeviceUpdate(zoneSetpointIDX, newSetpoint)
      
        issueURLCommand('http://'.. DomoticzIP..':'..DomoticzPort..'/json.htm?type=command&param=udevice&idx='..zoneSetpointIDX..'&nvalue=0&svalue='..tostring(newSetpoint))
      end
    end
    
    --end of Heating coordinator zone related actions
    --print("end of Zone related Heating coordinator actions")  
  end
  --print("Checking for changes in house timer")
  if newHouseState ~= oldHouseState then
    print(string.format("Issued house state update is %s was %s ",newHouseState,oldHouseState)) 
    commandArray["HC_HouseHeatingOn"] = newHouseState
  end
  if newHouseTimer ~= oldHouseTimer then
    print(string.format("Issue house timer update was %s is now %s",oldHouseTimer,newHouseTimer))
    commandArray["Variable:HCN_HouseTimer"] = tostring(newHouseTimer)
  end

  --print("end of ALL Heating coordinator actions")  
  -- end of all heating coordinator related actions
end
  

logtext(string.format("END")) -- for timing purposes

return commandArray

