--[[
This script got from:
http://www.domoticz.com/forum/viewtopic.php?f=24&t=4190&sid=a951bf47b807b7ac87312762344a1a5a&start=40

Heating Coordinator Script (http://www.domoticz.com/forum/viewtopic.php?f=24&t=4190)
==========================

This script is used to manage the firing of a boiler to support multiple zone requirements using temperature sensor readings and thermostat setpoints in each.

User variable requirements
--------------------------

apczzz this all needs writing up

Name: "HeatingCoordinator_ThermostatNamePrefix"
Type: "String"
Description: Defines the thermostat device name prefix, a device with a name made from this prefix followed by the zone should be present
Example: "ValveThermostat"

Name: "HeatingCoordinator_TempSensorNamePrefix"
Type: "String"
Description: Defines the temp sensor device name prefix, a device with a name made from this prefix followed by the zone should be present
Example: "TempSensor_"

Name: "HeatingCoordinator_WindowSwitchNamePrefix"
Type: "String"
Description: Defines the window switch device name prefix, a device with a name made from this prefix followed by the zone should be present
Example: "WindowSwitch_"

Name: "HeatingCoordinator_BoilerReceiverSwitchName"
Type: "String"
Description: Defines the boiler receiver device naming
Example: "BoilerReceiver_CentralHeating"

Name: "HeatingCoordinator_Zones"
Type: "String"
Description: Defines the zone names in a comma delimited list with no spaces
Example: "LivingRoom,Kitchen,BedroomFrontLeft,BedroomFrontRight,BedroomBackLeft"

Name: "HeatingCoordinator_BoilerStateChangeWaitTime"
Type: "Integer"
Description: Defines the length of time in seconds before a boiler state change is allowed after a previous update
Example: 300

Name: "HeatingCoordinator_BoilerAliveTime"
Type: "Integer"
Description: Defines the length of time in seconds since a previous boiler state change before a keep alive update is required. Some receivers will switch off automatically after a given time if not told to stay on, this is normally done by a room thermostat but without one this is required.
Example: 2700

Name: "HeatingCoordinator_TempHysterisis"
Type: "Float"
Description: Defines the amount of temperature allowance either side of the on / off heating states to stop constant switching
Example: 0.5

Name: "HeatingCoordinator_LogActivity"
Type: "Integer"
Description: Defines if all activity is logged ot nor, 1 = yes and 0 = No. Activity relating to state changes is always logged however.
Example: 1

Name: "HeatingCoordinator_TempSensorMaxTimeSinceLastSeen"
Type: "Integer"
Description: Defines the maximum time between two readings of one TempSensor. If no data has been received, the reading will be ignored. This is a fail-safe against the boiler remaining 'On' indefinitely.
Example: 900

This is a time script so recieves the following tables:
otherdevices, otherdevices_lastupdate, otherdevices_svalues, uservariables and uservariables_lastupdate

]]


commandArray = {}


function splitString(str, delim, maxNb)
    -- Eliminate bad cases...
    if string.find(str, delim) == nil then
        return { str }
    end
    if maxNb == nil or maxNb < 1 then
        maxNb = 0    -- No limit
    end
    local result = {}
    local pat = "(.-)" .. delim .. "()"
    local nb = 0
    local lastPos
    for part, pos in string.gmatch(str, pat) do
        nb = nb + 1
        result[nb] = part
        lastPos = pos
        if nb == maxNb then break end
    end
    -- Handle the last field
    if nb ~= maxNb then
        result[nb + 1] = string.sub(str, lastPos)
    end
    return result
end

function getLastUpdatedTime(deviceName)

   lastUpdatedString = otherdevices_lastupdate[deviceName]

   year = string.sub(lastUpdatedString, 1, 4)
   month = string.sub(lastUpdatedString, 6, 7)
   day = string.sub(lastUpdatedString, 9, 10)
   hour = string.sub(lastUpdatedString, 12, 13)
   minutes = string.sub(lastUpdatedString, 15, 16)
   seconds = string.sub(lastUpdatedString, 18, 19)

   return os.time{year=year, month=month, day=day, hour=hour, min=minutes, sec=seconds}

end

function logtext(text, override)
   if logActivity == 1 or override == true then
      print("(HeatingCoordinator) "..text)
   end
end


--[[
Get number from the device name svalue. If the device name does not exist return nil. If the value does not parse as a number return 0
]]
function getFirstDeviceSvalue(deviceName, errorReport)
 deviceValue = otherdevices_svalues[deviceName]
    
  if deviceValue == nil then 
    if errorReport then
      logtext(string.format("ERROR: Device name %s Not Found", deviceName), true)  
    end
    return nil
  end
  -- if device has more than just one value get the first one
  deviceValueDelimited = string.find(deviceValue, ";")
  if deviceValueDelimited ~= nil then
    deviceValue = string.sub(deviceValue, 0, deviceValueDelimited - 1)
    logtext(string.format("apctemp: Device name %s value again %s", deviceName,deviceValue), true)  
  
  end
  
  return tonumber(deviceValue)
end


function issueDeviceUpdate(deviceID, deviceValue)
  --logtext(string.format("Issue update to %s, idx %s, value %s", deviceName, deviceID, deviceValue))
  commandArray[commandArrayIndex] = {['UpdateDevice'] = deviceID .. "|0|" .. tostring(deviceValue)}
  commandArrayIndex = commandArrayIndex + 1
end



function getSetPoint(zoneName, zoneTimer)
  
  local fileTime
  local settingDetails
  local newSetPoint = -1

  if HC_HouseHeating == "Off" then
      -- this overrides all settings, boost or otherwise
      newSetPoint = HC_FrostSetting

  else

    local currentSetPoint = getFirstDeviceSvalue(hcPrefix..'_'..zoneName..'_'..statSuffix)

    if currentSetPoint == nil then
        logtext(string.format("ERROR: Device %s missing, using frost setting", hcPrefix..'_'..zoneName..'_'..statSuffix), true)
        currentSetPoint = HC_FrostSetting
    end
  
    if otherdevices[hcPrefix..'_'..zoneName..'_'..occupiedSuffix] ~= "Off" then
        --print("Zone is ON") -- or the heating enable device is missing
        if zoneTimer ~= nil and zoneTimer > 1 then
            logtext(string.format("boost timer active. Staying at %s degC for %s minutes", currentSetPoint, zoneTimer -1 ))
            newSetPoint = currentSetPoint
        else
  
          -- Not on boost timer, lets see if we can find the setpoint in the  file
          dayToday = string.lower(os.date("%a"))
          timeNow = (os.date("%H") * 60) + os.date("%M")
  
          fileName = "//home/pi/domoticz/zoneprofiles/"..hcPrefix..'_'..zoneName.."_Profile.csv"
          --logtext(string.format(" Looking for '%s' '%s' '%s' ",fileName,dayToday, timeNow ))
          fh = io.open(fileName,"r")
          if fh == nil then
              fileName = "//home/pi/domoticz/zoneprofiles/HC_Default_Profile.csv"
              fh = io.open(fileName,"r")
              if fh ~= nil then
                  logtext(string.format("WARNING: Using default profile %s", fileName), true)
              end
          end

          --+++++++
          if fh then
              --[[ logtext(string.format("Time Now %s:%s. Found profile '%s' ",
                  math.floor(timeNow / 60),
                  timeNow % 60,
                  fileName ))
                  ]]

              --###############
              while true do
                  line = fh.read(fh)
                  if not line then
                       break
                  end
                  lineTable = splitString(line,',')
        
                  if string.lower( lineTable[1]) == dayToday then
                      -- logtext(string.format(" Found '%s' ",dayToday ))
                      lineIndex = 2
                      repeat
                          -- logtext(string.format(" Trying Index = '%s' ",lineIndex ))
                          fileTime = (string.sub(lineTable[lineIndex], 1, 2) * 60) + string.sub(lineTable[lineIndex], 4, 5)
  
                          if timeNow >= fileTime   then
                              newSetPoint = lineTable[lineIndex +1]
                              settingDetails = string.format("Found Day %s, starttime %s:%s, Temp required = %s deg C",
                                dayToday,
                                math.floor (fileTime/60),
                                fileTime % 60,
                                newSetPoint)
                          else
                              break
                          end
                          lineIndex = lineIndex +2
                      until lineIndex >= #lineTable
                  end

                  if lineIndex == 2 then
                      --We havent got to the first switch time today so still running on last setpoint value, switch to frost setting and warn
                      logtext(string.format("WARNING: %s, No setting for %s %s:%s. Using frost setting",
                      fileName,
                      dayToday,
                      math.floor(timeNow / 60),
                      timeNow % 60,
                      fileName,
                      true ))
                      newSetPoint = HC_FrostSetting
                      break
                  end
              end
              --#########

              if settingDetails ~= nil then
                  logtext(settingDetails)
              end
           end
        end
        else
            logtext(string.format("ERROR: No profile.csv %s Not Found. assume Zone is OFF", fileName), true)
            newSetPoint = HC_FrostSetting
        end
        --+++++++

        if zoneName == "HotWater" then
            maxTemp = 65
        else
            maxTemp = 30
        end

        --we have now either read a value from the csv file OR got the current setting coz its still valid
        if newSetPoint == nil or tonumber(newSetPoint) < 2 or tonumber(newSetPoint) > maxTemp then
          -- Either coudln't find a value or it's out of range, keep the current setting
          logtext(string.format("ERROR: Got invalid setpoint -keeping current value(%s)", currentSetPoint), true)
          newSetPoint = currentSetPoint
        end
      else
        logtext("Zone is Unoccupied and turned Off")
        -- The zone is turned OFF
        newSetPoint = HC_FrostSetting
      end
    end
    return newSetPoint
end
           



function getZoneHeatNeeded(heatingZonesIndex)

  local zoneName = heatingZones[heatingZonesIndex]
  local zoneHeatNeeded = "NotApplicable"
  local radiatorValveDevice = "NotApplicable"
  local currentZoneTemp = getFirstDeviceSvalue(hcPrefix..'_'..zoneName..'_'..tempSuffix)
  local zoneTimer = uservariables[hcPrefix..'_'..zoneName..'_'..timerSuffix]
  local desiredZoneSetPoint = getSetPoint(zoneName, zoneTimer)
  
  -- Get the ids of the setpoint thermostat for this zone.
  -- This is because we cannot update these by name - we need the id
  currentSetpointidx = setpointDeviceIdx[heatingZonesIndex]
  
  if zoneTimer == nil then
    zoneTimer = 0
  elseif zoneTimer > 0 then
    zoneTimer = zoneTimer -1
  end
  if uservariables[hcPrefix..'_'..zoneName..'_'..timerSuffix] ~= zoneTimer then
    --Timer has changed, write the new value back
    commandArray["Variable:"..hcPrefix..'_'..zoneName..'_'..timerSuffix] = tostring(zoneTimer)
  end

  -- write the setpoint back if it has changed
  if desiredZoneSetPoint ~= currentZoneTemp  then
  issueDeviceUpdate( currentSetpointidx, desiredZoneSetPoint)
  end

  -- if the reading is recent enough - lets use it, otherwise ignore
  if currentZoneTemp ~= -1 and 
    tempSensorMaxTimeSinceLastSeen < os.difftime(os.time(),getLastUpdatedTime(hcPrefix..'_'..zoneName..'_'..tempSuffix)) then
    -- Its old, ignore it
    currentZoneTemp = -1 
    logtext(string.format("ERROR: Temp sensor not seen for %s minutes", 
        os.difftime(os.time(),getLastUpdatedTime(hcPrefix..'_'..zoneName..'_'..tempSuffix) )/60),
        true) 
  end

  -- if nothing valid for zone readings assume not applicable
  if (currentZoneTemp == -1) then
    zoneHeatNeeded = "NotApplicable"
  else
      -- determine heating requirements including hysterisis
      if (currentZoneTemp < (desiredZoneSetPoint - tempHysteresis)) then
        zoneHeatNeeded = "Yes"
      elseif (currentZoneTemp > (desiredZoneSetPoint + tempHysteresis)) then      
        zoneHeatNeeded = "No"
      else
        zoneHeatNeeded = "NotApplicable"
      end
  end
   
  logtext(string.format("Temp = '%s', SetPoint = '%s', Heat Needed = '%s'",
    currentZoneTemp,
    desiredZoneSetPoint, 
    zoneHeatNeeded))

  return zoneHeatNeeded

end



function processZones()

   -- loop our zones and see if any needs heat
   newBoilerState = "NotSet"
   
  if HC_HouseHeating == "Off" then
    logtext(string.format("Away , All zones set to Frost setting %s degC",HC_FrostSetting))
  end
  
  
  for heatingZonesIndex = 1, #heatingZones do
      -- determine if heat needed. If so turn on boiler
      print(string.format('\n================= ZONE: %s ======================================',heatingZones[heatingZonesIndex])) -- put nice header in log
      radiatorValveName = hcPrefix..'_'..heatingZones[heatingZonesIndex]..'_'..radSuffix
            
      zoneHeatNeeded = getZoneHeatNeeded(heatingZonesIndex)
      
      --[[ control the valve for this zone
      apczzz - rename - deal with hotwater as a valve, also or together all zones to see if boiler needs to be on
      Turn boiler on after 60 seconds, turn off immediatley
      turn radiator valve son immediately, off after n seconds
      
      Timers/boost are being restarted randomly
      ]]
      
      if (zoneHeatNeeded == "Yes") then
        if otherdevices[radiatorValveName] == 'Off' then
          commandArray[radiatorValveName] = 'On'
          logtext(string.format(" Radiator: '%s' turned ON", radiatorValveName))
        end
      else
        if otherdevices[radiatorValveName] == 'On' then
          commandArray[radiatorValveName] = 'Off AFTER 300'
          logtext(string.format(" Radiator: '%s' turned OFF after 300", radiatorValveName))
        end
      end

      if newBoilerState ~= "On" then
         if (zoneHeatNeeded == "Yes") then
            newBoilerState = "On"
         elseif (zoneHeatNeeded == "NotApplicable") then
            newBoilerState = "NotApplicable"
         elseif (zoneHeatNeeded == "No" and newBoilerState ~= "NotApplicable") then
            newBoilerState = "Off"
         end
      end
   end
   print(string.format('\n******** BOILER **********')) -- Nice header in log
    
   boilerWaitTime = os.difftime(os.time(),getLastUpdatedTime(boilerReceiverSwitchName))

   if newBoilerState == "On" then
      if newBoilerState ~= otherdevices[boilerReceiverSwitchName] then
         if boilerWaitTime >= boilerStateChangeWaitTime then
            commandArray[boilerReceiverSwitchName] = newBoilerState
            logtext("Boiler State Change: Off -> HeatOn", true)
         else
            logtext(string.format("Boiler State Change: Off -> HeatOn (in %s secs)",boilerStateChangeWaitTime - boilerWaitTime), true)
         end
      else
         if boilerWaitTime >= boilerAliveTime then
            commandArray[boilerReceiverSwitchName] = newBoilerState
            logtext("**** Boiler State **** Remains: HeatOn (Keep Alive Update)", true)
         else
            logtext("**** Boiler State **** Remains: HeatOn")
         end
      end
   elseif newBoilerState == "Off" then
      if newBoilerState ~= otherdevices[boilerReceiverSwitchName] then
         if boilerWaitTime >= boilerStateChangeWaitTime then
            commandArray[boilerReceiverSwitchName] = newBoilerState
            logtext("oooo Boiler State oooo Change: HeatOn -> Off", true)
         else
            logtext(string.format("^^^^ Boiler State ^^^^ Change: HeatOn -> Off (in %s secs)",boilerStateChangeWaitTime - boilerWaitTime), true)
         end
      else
         logtext("**** Boiler State **** Remains: Off")
      end

   elseif newBoilerState == "NotApplicable" then

      if boilerWaitTime >= boilerAliveTime and otherdevices[boilerReceiverSwitchName] == "On" then
         commandArray[boilerReceiverSwitchName] = "On"
         logtext("**** Boiler State **** Remains: HeatOn (Hysterisis / Keep Alive Update)", true)
      else
         if otherdevices[boilerReceiverSwitchName] == "On" then
            logtext("**** Boiler State **** Remains: HeatOn (Hysterisis)")
         else
            logtext("**** Boiler State **** Remains: Off (Hysterisis)")
         end
      end

   end

   return
end

-- User variable population

logActivity = uservariables["HeatingCoordinator_LogActivity"]
logtext(string.format("START")) -- for timing purposes
hcPrefix = uservariables["HeatingCoordinator_NamePrefix"]
timerSuffix = uservariables["HeatingCoordinator_TimerSuffix"]
radSuffix = uservariables["HeatingCoordinator_RadiatorNameSuffix"]
statSuffix = uservariables["HeatingCoordinator_ThermostatNameSuffix"]
tempSuffix = uservariables["HeatingCoordinator_TempSensorNameSuffix"]
occupiedSuffix = uservariables["HeatingCoordinator_OccupiedSuffix"]
boilerReceiverSwitchName = uservariables["HeatingCoordinator_BoilerReceiverSwitchName"]
heatingZones = splitString(uservariables["HeatingCoordinator_Zones"],",")
setpointDeviceIdx = splitString(uservariables["HeatingCoordinator_Setpoint_idx"],",")
boilerStateChangeWaitTime = uservariables["HeatingCoordinator_BoilerStateChangeWaitTime"]
boilerAliveTime = uservariables["HeatingCoordinator_BoilerAliveTime"]
tempHysteresis = uservariables["HeatingCoordinator_TempHysterisis"]
tempSensorMaxTimeSinceLastSeen = uservariables["HeatingCoordinator_TempSensorMaxTimeSinceLastSeen"]
HC_FrostSetting = uservariables["HC_FrostSetting"]
HC_HouseHeating = otherdevices["HC_HouseHeating"]


commandArrayIndex = 1

-- Process heating requirements
processZones()

logtext(string.format("END")) -- for timing purposes
return commandArray

--[[
To do still 13/9/2015

Sort out boiler requirements for both water and heating.
  It currently turns on boiler of any zone needs heat

Experiment with delays:
  When radiator valves are turned on, need to dealy starting boiler if not already on
  When all radiator valves are not wanted, need to dealy closing them after boiler turned off

The description at the top all needs writing up

Add the rest of the + and - switch devices

]]
