local functionlist = {}


local defaultTemps = "default,temp,14"
local defaultTimes = "default,time,00:00"
      

function issueDeviceUpdate(deviceID, deviceValue)
  --print(string.format("Index %s Update device %s to %s",commandArrayIndex ,deviceID, deviceValue))
  commandArray[commandArrayIndex] = {['UpdateDevice'] = deviceID .. "|0|" .. tostring(deviceValue)}
  commandArrayIndex = commandArrayIndex + 1
end

 function issueURLCommand (URLString)
  --print(string.format("Index %s URL = [%s]",commandArrayIndex ,URLString))
  commandArray[commandArrayIndex] = {['OpenURL']= URLString }
  commandArrayIndex = commandArrayIndex + 1
 end
 

function getFirstDeviceSvalue(deviceName, errorReport)
  --print("getsvalue: "..deviceName)
  deviceValue = otherdevices_svalues[deviceName]
  
  --print(string.format("getfirstsvalue Dev[%s] value is [%s]",deviceName, deviceValue))
  errorReport = true
  if deviceValue == nil then 
    if errorReport then
      logtext(string.format("ERROR: Device name %s Not Found", deviceName), true)  
    end
    return nil
  end
  -- if device has more than just one value get the first one
  deviceValueDelimited = string.find(deviceValue, ";")
  if deviceValueDelimited ~= nil then
    deviceValue = string.sub(deviceValue, 0, deviceValueDelimited - 1)
  end
  
  return deviceValue
end


function logtext(text, override)
   if uservariables["HCN_LogActivity"] == 1 or override == true then
      print("(HeatingCoordinator) "..text)
   end
end

function testflag(set, flag)
  return (set % (2*flag)) >= flag
end

function setflag(set, flag)
  if set % (2*flag) >= flag then
    return set
  end
  return set + flag
end

function clrflag(set, flag) -- clear flag
  if set % (2*flag) >= flag then
    return set - flag
  end
  return set
end

function splitUpString(str, delim, maxNb)
    
    -- Eliminate bad cases...
    if string.find(str, delim) == nil then
        return { str }
    end
    if maxNb == nil or maxNb < 1 then
        maxNb = 0    -- No limit
    end
    local result = {}
    local pat = "(.-)" .. delim .. "()"
    local nb = 0
    local lastPos
    for part, pos in string.gmatch(str, pat) do
        nb = nb + 1
        result[nb] = part
        lastPos = pos
        if nb == maxNb then break end
    end
    -- Handle the last field
    if nb ~= maxNb then
        result[nb + 1] = string.sub(str, lastPos)
    end
    return result
end



-- returns setpoint idx followed by textsetpoint idx
function getIdxList(zoneName)
	local setpointDeviceIdxList = splitUpString(uservariables["HCN_Setpoint_idxList"],",")
	local setpointTextDeviceIdxList = splitUpString(uservariables["HCN_Info_idxList"],",")
	for heatingZonesIndex = 1, #heatingZones do
	   if zoneName == heatingZones[heatingZonesIndex] then
	       return setpointDeviceIdxList[heatingZonesIndex], setpointTextDeviceIdxList[heatingZonesIndex]
	   end
	end
end



function updateInfoString(oldZoneInfo,
                        infFlags2,
                        infZoneName,
                        infZoneIDX,
                        infRadiatorState, 
                        infBoilerState, 
                        infHouseState,
                        infZoneState,
                        infZoneTimer,
                        infHouseTimer,
                        infSetPoint)
  --[[  INFO String mapping is as follows. 25 bytes to match Mysensors payload
          (NOTE : LUA byte numbering shown - starts at 1 whereas C starts at 0
        1-12 Fixed 12 character (No end NULL) name padded with spaces - don't touch
        13-15 digit room override timer in minutes
        16-18 3 digit house override timer in minutes
        19-21 set point in form of three digits EG 192 means 19.2C
        22  flag byte 1 - subtract 0x20 then 7 bits of flag data
        23  flag byte 2 - subtract 0x20 then 7 bits of flag data
        24-25 not used

      Flags - subtract 32 then...
        Flag1==============
          bits 0-2:   0-7. (0 = 15sec, 1 = 30s etc. 15s/30s/60s/2m/4m/8m/16m/32m). This is the update rate for mysensor in minutes -          getfrom domoticz. Typical is 2 for every 1 minute
          bit 3 :     This zone valve is on (from domoticz)
          bit 4 :     zone enabled if 1 (zone occupied from domoticz)
          bit 5 :     House enabled if 1  (house occupied from domoticz)
          bit 6 :     Boiler is ON if 1 (from domoticz)

      Flag2==============
        bit 0 - if set - disable house heating (from sensor)
        bit 1 - if set - enable house heating (from sensor)
        bit 2 -if set - disable room heating (from sensor)
        bit 3 -if set - enable room heating (from sensor)
    ]]
  local errorFound = false
  if infZoneIDX == 0 or infZoneIDX == 'nil' then
    errorFound = true   
    print(string.format("ERROR: Zone %s_%s_Info id is %s - cannot save. Please Check",
    hcPrefix,infZoneName,infZoneIDX))
    infZoneIDX = 999 -- just something to show in the error string
  end  
  if oldZoneInfo == nil then
    print(string.format("ERROR: zone info %s_%s_Info not found",hcPrefix,infZoneName))
    print(string.format("If above exists and you continue to get this error, First update all the idx list by operating device %s_%s.",hcPrefix,updateAllIDXDeviceName))
    print(string.format("If that doesn't work try updating the device from the real remote sensor device or URL as follows"))
    errorFound = true   
    -- BUG &param was being replaced by new para mark. Workaround is to replace &param by &amp;param
    print(string.format("http://192.168.1.170:8080/json.htm?type=command&amp;param=udevice&idx=%s&nvalue=0&svalue=andywasere",
        infZoneIDX))
    
    --create a dummy oldZoneInfo string
    oldZoneInfo = string.format("%12.12s00000002500","ERROR:NoZoneInfo")
  end
  if errorFound then    
    print(string.format("ERROR: May not be able to update %s_%s_Info string",hcPrefix,infZoneName))
  end
    
  local mySensorsDelay = tonumber(uservariables[MySensorsDelayVariableName])
  if mySensorsDelay > 7 or mySensorsDelay == 'nil' then
    mySensorsDelay = 2 -- default to once per minute updates
  end
  
                        
--  print(string.format("mySensorsDelay %s",mySensorsDelay))
--  print(string.format("infZoneTimer %s",infZoneTimer))
--  print(string.format("infHouseTimer %s",infHouseTimer))
--  print(string.format("infSetPoint %s",infSetPoint))
  
  -- get old flags 2. We don't touch them unless they are invalid or a zero tells us to clear them
  -- never touch flags2 - they contain commands from the remote sensor
  if infFlags2 == 0 then
    -- use the old value from the string
    infFlags2 =  string.byte (string.sub(oldZoneInfo,23,23)) 
  end
  if infFlags2 == nil or infFlags2 < 32 then
    infFlags2 = 32
  end
  
  -- build up flags1 fromt the data passed to us
  local flags1 = 32 + mySensorsDelay -- start with zero value(32)
  if infRadiatorState =='On' then
    flags1 = flags1 + 8
    --print(string.format("infRadiatorState %s",infRadiatorState))
  end
  if infZoneState == 'On' then
    flags1 = flags1 + 16
  --print(string.format("infZoneState %s",infZoneState))
  end
  if infHouseState == 'On' then
    flags1 = flags1 + 32
    --print(string.format("infHouseState %s",infHouseState))
  end
      
  if infBoilerState == 'On' then
    flags1 = flags1 + 64
    --print(string.format("infBoilerState %s",infBoilerState))
  end
  --print(string.format("Flags1 = 0x%2x",flags1))  
  -- BUG. Cannot pad strings with spaces, use underscore instead
  newZoneInfo = string.format("%s%03d%03d%03d%c%c",string.sub(infZoneName.."_________",1,12),
        infZoneTimer,  infHouseTimer, tonumber(infSetPoint)*10,flags1,infFlags2)
  if newZoneInfo ~= oldZoneInfo then
    logtext(string.format("Attempting text update [%s] to id %s",newZoneInfo,zoneInfoIDX))
    issueDeviceUpdate(infZoneIDX, newZoneInfo)
  end
end


function getFileSettings(zoneName)
  --[[
  This function expects a file containing a pair of lines for every day eg
  eg  
      sun,time,00:00,19:00,22:30,
      sun,temp,7,13,5.2,
      etc
      The special case
  ]]
  
  
  local dayToday = string.lower(os.date("%a"))
  local timeNow = (os.date("%H") * 60) + os.date("%M")
  local nextTime = "xx:xx"
  local newNextTime
  local newFileSetpoint = HC_FrostSetting -- set a default
  --local fileName = "//home/pi/domoticz/zoneprofiles/"..hcPrefix..'_'..zoneName.."_ProfilePrs.csv"
  local fileName = "//media/networkshare/protected/"..heatingProfileSet.."_profiles/"..hcPrefix..'_'..zoneName.."_ProfilePrs.csv"
  --logtext(string.format(" Looking for '%s' '%s' '%s' ",fileName,dayToday, timeNow ))
  local fh = io.open(fileName,"r")
  local matchedTableOfTimes = ' '
  local matchedTableOfTemps = ' '
  local defaultTableOfTimes = ' '
  local defaultTableOfTemps = ' '
  
  if fh == nil then
    print(string.format("WARNING: Profile File [%s] not found, trying default file",fileName))
    fileName = "//media/networkshare/protected/"..heatingProfileSet.."_profiles/HC_Default_ProfilePrs.csv"
    fh = io.open(fileName,"r")
    if fh ~= nil then
      logtext(string.format("WARNING: Using default profile %s", fileName), true)
    else
      print(string.format("ERROR: Default Profile File [%s] not found, using fixed zone settings",fileName))
    end
  end

  if fh then
    --[[logtext(string.format("Time Now %s:%s. Found profile '%s' ",
        math.floor(timeNow / 60),
        timeNow % 60,
        fileName ))
    ]]

    while true do
      line = fh.read(fh)
      --print(string.format("Line = [%s]",line))
      if not line then
         break
      end
      lineTable = splitUpString(line,',')
        
      if string.lower( lineTable[1]) == dayToday then
        --logtext(string.format(" Found '%s' ",dayToday ))
        if string.lower( lineTable[2]) == 'temp' then
          matchedTableOfTemps = lineTable
        elseif string.lower( lineTable[2]) == 'time' then
          matchedTableOfTimes = lineTable
        end
      elseif string.lower( lineTable[1]) == 'default' then
        --logtext(string.format(" Found 'default'" ))
        if string.lower( lineTable[2]) == 'temp' then
          defaultTableOfTemps = lineTable
        elseif string.lower( lineTable[2]) == 'time' then
          defaultTableOfTimes = lineTable
        end
      end  
    end
  end
  
  -- finished scanning the file, lets check what we found
  if #matchedTableOfTemps < 3 or #matchedTableOfTimes < 3 then
    -- the matched info does not work or isn't formatted, lets try the default
    if #defaultTableOfTemps == #defaultTableOfTimes and #defaultTableOfTemps > 2 then
      -- the default ones may work
      matchedTableOfTemps = defaultTableOfTemps
      matchedTableOfTimes = defaultTableOfTimes
    end
    if #matchedTableOfTemps ~= #matchedTableOfTimes or #matchedTableOfTemps < 3 then
      matchedTableOfTemps = splitUpString(defaultTemps,',')
      matchedTableOfTimes = splitUpString(defaultTimes,',')
      print(string.format("ERROR. No real info found in file. Using default values [%s] and [%s]",defaultTemps,defaultTimes))
    end
  end
  --for k,v in pairs(matchedTableOfTemps) do print(k,v) end
  --for k,v in pairs(matchedTableOfTimes) do print(k,v) end
  
  -- now search the time string for the best match to timenow
  local lineOffset = 3 -- start the search at what should be the first time
    
  repeat
    --print(string.format("Tring index %d", lineOffset))
    if string.sub(matchedTableOfTimes [lineOffset], 3, 3) ~= ':' then
      print(string.format("ERROR: Time Line does not contain properly formatted times eg hh:mm. Using frost setting"))
      newFileSetpoint = HC_FrostSetting
      break
    else
      fileTime = (string.sub(matchedTableOfTimes [lineOffset], 1, 2) * 60) + string.sub(matchedTableOfTimes[lineOffset], 4, 5)
      if timeNow >= fileTime then
        newFileSetpoint = matchedTableOfTemps[lineOffset ]
        settingDetails = string.format("Found Day %s, starttime %02s:%02s, Temp required = %s deg C",
          dayToday,
          math.floor (fileTime/60),
          fileTime % 60,
          newFileSetpoint)
      else
        break
      end
    end
    lineOffset = lineOffset +1
  until lineOffset >= #matchedTableOfTimes
  if settingDetails ~= nil then
    logtext(settingDetails)
  end
  return tonumber(newFileSetpoint)
end
 
-- needs globals hcPrefix statSuffix

function getSetpoint(zoneName, currentSetpoint, zoneTimer, presentZoneState, presentHouseState)
  
  local newDesiredSetpoint = -1

  if currentSetpoint == nil then
      logtext(string.format("ERROR: Device %s missing, using frost setting", hcPrefix..'_'..zoneName..'_'..statSuffix), true)
      newDesiredSetpoint = HC_FrostSetting
  end
  
  if newDesiredSetpoint == -1 then
    if presentHouseState == "Off" then
      -- all zones are off
      newDesiredSetpoint = HC_FrostSetting
    elseif presentZoneState ~= "On" then
      -- The zone is turned OFF
      logtext("Zone is Unoccupied and turned Off")
      newDesiredSetpoint = HC_FrostSetting
    end
  end
  if newDesiredSetpoint == -1 then
    if zoneTimer ~= nil and zoneTimer > 1 then
      logtext(string.format("boost timer active. Staying at %s degC for %s minutes", currentSetpoint, zoneTimer -1 ))
      newDesiredSetpoint = currentSetpoint
    else
      -- Not on boost timer, lets see if we can find the setpoint in the  file
      newDesiredSetpoint, nextTime = getFileSettings(zoneName)
      
      if zoneName == hotWaterZoneName then
        maxTemp = 65
      else
        maxTemp = 26
      end
      --we have now either read a value from the csv file OR got the current setting coz its still valid
      if newDesiredSetpoint == nil or tonumber(newDesiredSetpoint) < 2 or tonumber(newDesiredSetpoint) > maxTemp then
        -- Either coudln't find a value or it's out of range, keep the current setting
        logtext(string.format("ERROR: Got invalid setpoint -keeping current value(%s)", currentSetpoint), true)
        newDesiredSetpoint = currentSetpoint
      end
    end
  end
     
  return newDesiredSetpoint, nextTime
end
           

function getLastUpdatedTime(deviceName, deviceNotVariable)
  --print(string.format("Get Updated time DEvice name = [%s][%s]",deviceName,deviceNotVariable))
  local lastUpdatedString = ' '
  if deviceName == 'nil' or deviceName == '' then
     print("ERROR: DEvice name is nil or zero length")
  end
  if deviceNotVariable then
    -- its a device, check the devices table for update time
    lastUpdatedString = otherdevices_lastupdate[deviceName]
  else
    -- its a variable - check a different table for variable update time
    lastUpdatedString = uservariables_lastupdate[deviceName]
  end
  if lastUpdatedString == nil or string.len(lastUpdatedString) < 19 then
    lastUpdatedString = '1970:01:01:00:00:00'
    print(string.format("ERROR: No last updated time for device %s",deviceName))
  end
  
  year = string.sub(lastUpdatedString, 1, 4)
  month = string.sub(lastUpdatedString, 6, 7)
  day = string.sub(lastUpdatedString, 9, 10)
  hour = string.sub(lastUpdatedString, 12, 13)
  minutes = string.sub(lastUpdatedString, 15, 16)
  seconds = string.sub(lastUpdatedString, 18, 19)

  return os.time{year=year, month=month, day=day, hour=hour, min=minutes, sec=seconds}
end

function getSecondsSinceUpdate (deviceName, deviceNotVariable)
  return os.difftime(os.time(),getLastUpdatedTime(deviceName, deviceNotVariable))
end
  


functionlist.logtext = logtext
functionlist.issueDeviceUpdate = issueDeviceUpdate
functionlist.getFirstDeviceSvalue = getFirstDeviceSvalue
functionlist.testflag = testflag
functionlist.setflag = setflag
functionlist.clrflag = clrflag
functionlist.splitUpString = splitUpString
functionlist.getIdxList = getIdxList
functionlist.updateInfoString = updateInfoString
functionlist.getSetpoint = getSetpoint
functionlist.getLastUpdatedTime = getLastUpdatedTime
functionlist.issueURLCommand = issueURLCommand
functionlist.getSecondsSinceUpdate = getSecondsSinceUpdate
return functionlist

