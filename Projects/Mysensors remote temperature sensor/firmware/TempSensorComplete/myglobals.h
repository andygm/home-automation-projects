#ifndef globals__h
#define globals__h

/*
 *  globals.h
    // project wide globals


 * 
	Last change: AC 04/04/2016 19:29:14
 */

#define INFO_MESSAGE_SIZE MAX_PAYLOAD

// Some hardware defines
#define PIN_ONE_WIRE_BUS 3 // Pin where dallas sensor is connected 
#define PIN_BUTTON1 7
#define PIN_BUTTON2 8
#define PIN_BUTTON3 6
#define MAX_ATTACHED_DS18B20 2
#define PIN_DISPLAY_ENABLE 5


//The id of the whole sensor module
#define SENSOR_ID 255 // 2- 254, 255 is AUTO
// then the child IDs
#define CHILD_ID_LOCAL_TEMP 0
#define CHILD_ID_TEXT  1


#define BATTERYEMPTYmV 2900
#define BATTERYFULLmV 3250

extern unsigned int countTenths;
extern unsigned long elapsedMillis;
extern byte stayAwake;
extern int oldbatteryPercent;


extern void checkBatteryState(void);





extern byte zoneTimer;
extern char zoneOn;
extern char zoneEnabled;
extern char houseEnabled;

extern boolean useTemperatureValues(void);
extern void requestTemperatures(void);



extern float lastTemperature;


typedef unsigned char t_bitlist8;


extern void updateRadioInfo(t_bitlist8 value);

#define  RI_SEND_LOCAL_TEMP 0x01
#define  RI_SEND_INFO  0x02
#define  RI_REQUEST_INFO  0x04
#define RI_SEND_BATTERY 0x08

// #define RI_REQUEST_TIME 0x10

#define SENSOR_NAME "APC Temp Sensor"
#define SENSOR_REVISION "1.07"
#define REVISIONSTRING "APC v" SENSOR_REVISION "\n\r4/04/2016\n\rDomoticz\r\nMysensors"

#endif

