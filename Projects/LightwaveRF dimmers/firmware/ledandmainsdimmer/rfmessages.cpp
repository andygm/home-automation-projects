/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

    Thsi module handles the lightwaveRF messages from the RF driver and passes them on to
    the lighting controller.

	Last change: AC 14/02/2016 13:31:26
*/


#include "globals.h"
#include "LwRxapc.h"
#if EEPROM_EN == 1
#include <EEPROM.h>
#endif


#include "lights.h"
#include "indicatorLED.h"
#include <avr/pgmspace.h>

// define the gap in radio messages where we treat the first message as GONE AWAY
// (We have to time some messages as a short message means one thing and the same
// message when longer means something different
// This is mostly because the siemens lightwaveRF transmitters are full of bugs and
// occasionaly accidentally send a short but incorrect message
#define MESSAGE_GAPmS 500

extern byte lastONLevel[LIGHTING_CHANNELS];


// a store mechanism for radio commands. Some commands cannot be immediately parsed
// it depends on how long the transmission goes on for. So we store a command
// and then after a delay, act on it if its still there. It may have been
// overwritten or cleared in that time. An example is
// short transmission of mood says select the mood.
// long transmission of mood says store the current status as the new mood
static t_lampCommand  radioStoreCommand = LC_LIGHTS_NO_COMMAND;;
static byte radioStoreChannelBitlist;
static byte radioStoreLevel;
static byte radioStoreDelay;




// The RF levels are 0 - 31 and are our internal Lamp levels are 0-255
static byte mapRFLevelToLamp (byte RFLevel)
{
    return (RFLevel * 8) + RFLevel/4;
}


// Retrieve a stored mood level scaled as a lamp level
static byte mapMoodLevelToLamp (byte moodLevel)
{
    if ( moodLevel > 200 )
    {
        ++moodLevel;
    }
    // ensure we don't store a mood of 255 - that is the empty marker.
    // we actually waste level 201 as it never gets stored exactly
    return moodLevel;
}



// We have received a mood save or apply radio message, deal with it
static boolean actionMoodCommand(boolean save, byte mood ,byte channelBitlist)
{
    if ( mood >= MAX_MOODS )
    {
        return false;
    }
    else
    {
        byte channelCtr = 0;
        byte eepromOffset;
        byte level;
        // Serial.printf("Handle mood %d, save %d bitlist 0x%x\n\r", mood,save,channelBitlist);
        for ( channelCtr = 0; channelCtr < LIGHTING_CHANNELS;channelCtr++ )
        {
            if ( (channelBitlist & 0x01) != 0 )
            {
                eepromOffset = mood * LIGHTING_CHANNELS + channelCtr;
                if ( save )
                {
                    level = mapLampLevelToMood(channelCtr);
                    EEPROM.write(MOOD_EEPROMaddr + eepromOffset, level );
                    //Serial.printf(F("Saving "));
                }
                else
                {
                    level = EEPROM.read(MOOD_EEPROMaddr + eepromOffset);
                    if ( level < EMPTYMOOD )
                    {
                        //Serial.printf(F("Applying "));
                        startFade(channelCtr, FM_APPLY_MOOD, mapMoodLevelToLamp(level));
                    }
                }
                // Serial.printf(F("mood %d ch %d offset %d at %d\n\r"), mood, channelCtr,eepromOffset, level);
            }
            channelBitlist >>= 0x01;
        }

        if ( save )
        {
            setIndicatorLED(LEDS_START_MOODSTORE_SUCCESS);
        }
        return true;
    }
}



// act on a stored radio command if the delay has expired or the TX has ended
void actionRadioStoreCommand(void)
{
    if
        (
        (radioStoreCommand != LC_LIGHTS_NO_COMMAND) &&
        ((lwrx_packetinterval() > MESSAGE_GAPmS ) || (radioStoreDelay-- == 0))
        )
    {
        #ifdef apcdebug
        unsigned int time = (long(millis()/100L));
        Serial.printf(F("%02d.%02d: Perform cmd '%s', channels 0x%x, level %d\n\r"),
            time/10, time%10, commandmsgs[radioStoreCommand], radioStoreChannelBitlist,radioStoreLevel   );
        #endif
        if ( radioStoreCommand == LC_LIGHTS_SAVE_MOOD )
        {
             actionMoodCommand(true, radioStoreLevel ,radioStoreChannelBitlist);
        }
        else if ( radioStoreCommand == LC_LIGHTS_USE_MOOD )
        {
             actionMoodCommand(false, radioStoreLevel ,radioStoreChannelBitlist);
        }
        else
        {
            byte channelCtr = 0;
            do
            {
                if ( (radioStoreChannelBitlist & 0x01) != 0)
                {
                    switch ( radioStoreCommand )
                    {
                        case LC_LIGHTS_ON:
                            // Serial.printf("ON. ");
                            if ( radioStoreLevel != 0 )
                            {
                                radioStoreLevel = mapRFLevelToLamp(radioStoreLevel);
                            }
                            // Serial.printf("Requested new level %d\n\r",levelCalc16);
                            startFade(channelCtr, FM_ON, radioStoreLevel);
                            break;

                        case LC_LIGHTS_DIM:
        //                    Serial.printf("DIM. ");
                            startFade(channelCtr, FM_RADIO_DIM, 0);
                            break;

                        case LC_LIGHTS_BRIGHTEN:
        //                    Serial.printf("BRIGHTEN ");
                            startFade(channelCtr, FM_RADIO_BRIGHTEN, 0);
                            break;

                        case LC_LIGHTS_OFF:
        //                    Serial.printf("OFF. ");
                            if ( radioStoreLevel != 0 )
                            {
                                lastONLevel[channelCtr ] = mapRFLevelToLamp(radioStoreLevel);
                            }
                            startFade(channelCtr, FM_OFF, 0);
                            break;

                        default:
                            break;
                    }
        //            Serial.printf(" level %d\n\r", newLevel[channelCtr -1]);
                }
                radioStoreChannelBitlist >>= 1;
            }while ( channelCtr++ < LIGHTING_CHANNELS );
        }
        radioStoreCommand = LC_LIGHTS_NO_COMMAND;
    }
}


static void queueLightingMessage(t_lampCommand command, byte channelBitlist, byte level, byte delay)
{
    // some messages should not be acted upon until we are sure they are not goingto morph
    //into others. An example is the off command. We may get a short OFF command that turns into a dim
    // we must wait till we know it is not a dim command before acting on it as an OFF command
    // Act on or save a mood behaves this way as well. A short message is act on the saved mood,
    // a long message is svae the current status as the mood
    // we have one stored message of 4 bytes:
    // action. channel bitlist, level, delayticks
    // always overwrite any current command
    radioStoreCommand = command;
    radioStoreChannelBitlist = channelBitlist;
    radioStoreLevel = level;
    radioStoreDelay = delay;
    #ifdef apcdebug
    unsigned int time = (long(millis()/10L));
    Serial.printf(F("\n\r%02d.%02d: Store cmd '%s', channels 0x%x, level %d, delay %d\n\r"),
        time/100, time%100, commandmsgs[radioStoreCommand], radioStoreChannelBitlist,radioStoreLevel, radioStoreDelay  );
    #endif
}



void handleRFMessage(void)
{
    byte msg[6];
    boolean handledOK = false;
    lwrxapc_getmessage(msg, 6); // command,param,room, device, lighting channel bitlist, repeat count
    // msg buffer contains a message that we are interested in. The pairing will have filtered out
    // those not of interest


    #ifdef apcdebug
    unsigned int time = (long(millis()/100L));
    Serial.printf(F("\n\rRXMSG COM  PARAM  ROOM  DEVICE CHANNELs TXLENGTH\n\r"));
    Serial.printf(F("%02d.%02d %d    %d      %d     %d      %02x       %d\n\r"),
        time/10, time%10,msg[0],msg[1],msg[2],msg[3],msg[4],msg[5]);
    #endif

    if ( msg[0] == 0 )
    {
        // OFF command - lets check the parameter
        if ( msg[1] >= 0 && msg[1] <= 127 )
        {
            // switch device off. Need to wait and see if it is followed by a DIM
            // before acting on it. Store it as a delayed command
            queueLightingMessage(LC_LIGHTS_OFF, msg[4] & 0x0F, 0,600 /LAMP_UPDATEmS);
        }
        else if ( msg[1] >= 160 && msg[1] <= 191 )
        {
            // step down brightness
            queueLightingMessage(LC_LIGHTS_DIM, msg[4] & 0x0F, 0, 0);
        }
        else if ( msg[1] >= 192 )
        {
            // all off
            queueLightingMessage(LC_LIGHTS_OFF, msg[4] & 0x0F, 0, 0);
        }
        else
        {
            //setbrightness level param 0 -31 (param - 128)
            queueLightingMessage(LC_LIGHTS_OFF, msg[4] & 0x0F, (msg[1]-128), 0);
        }
        handledOK = true;
    }
    else if ( msg[0] == 1 )
    {
        //ON command, lets check parameter
        if ( msg[1] >= 0 && msg[1] <= 31)
        {
            queueLightingMessage(LC_LIGHTS_ON, msg[4] & 0x0F, 0, 0);
        }
        else if ( msg[1] >= 32 && msg[1] <= 159 )
        {
            // set level
            queueLightingMessage(LC_LIGHTS_ON, msg[4] & 0x0F, ( msg[1] & ~0xE0), 0);
        }
        else if ( msg[1] >= 161 && msg[1] <= 191 )
        {
            // brighten. However our 10 button LWRF Siemens handsets send code 160
            // by mistake sometimes. So Lets ignore it
            queueLightingMessage(LC_LIGHTS_BRIGHTEN, msg[4] & 0x0F, 0, 0);
        }
        else
        {
            // set all to level apczzzz needs attention, match all devices
            // not sure how this command is supposed to work
            Serial.printf("ERROR: This command not handled\n\r");
//apczzz            changeLightsSettings (0x0F, LC_LIGHTS_ON, ( msg[1] & 0xE0));
        }
        handledOK = true;
    }
    else if ( msg[0] == 2 )
    {
        //mood command
        if (msg[1] >= 130)
        {
            if (  msg[5] == 5 )
            {
                // bug in remote control handset. It occasionally sends  a short
                //copy of this msg before it sends the SAVE MOOD msg. Queue this message
                // with a short delay so that if the save mood arrives, it overrides the
                // stored apply mood
                queueLightingMessage(LC_LIGHTS_USE_MOOD, msg[4] & 0x0F, msg[1] - 130, 500/LAMP_UPDATEmS);
            }
        }
        else
        {
            queueLightingMessage(LC_LIGHTS_SAVE_MOOD, msg[4] & 0x0F, msg[1] - 2, 0);
        }
        handledOK = true;
    }

    if ( !handledOK )
    {
        Serial.printf("Message not understood\n\r");
    }
}

