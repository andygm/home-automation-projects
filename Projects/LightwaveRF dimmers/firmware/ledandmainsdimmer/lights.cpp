/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

	Last change: AC 15/02/2016 19:21:06
*/


#include "globals.h"
#include "LwRxapc.h"

#include "lights.h"
#include "buttons.h"
#include "rfmessages.h"
#include "indicatorLED.h"
#include <avr/pgmspace.h>



typedef enum
{
    LS_FADING_DOWN,
    LS_FADING_UP,
    LS_START_FADING,
    LS_ON,
    LS_OFF
}t_lampState;
static t_lampState lampState[LIGHTING_CHANNELS];


// a bitlist read in from port pins on boot. Has a bit for every channel defining if dimming is allowed
static byte dimmingAllowed;

// Main mode setting, read in from port pin on boot
static boolean triacMode = false; // TRiac or LED PWM mode. Cannot mix the two



// ===============================================================================
// ================== TRIAC MODE DEFINATIONS & Variables==========================
// DESCRIPTION of OPERATION:-
// Triac mode works by a free running timer timer 1 which is sync'd to the mains cross over
// interrupt so it counts up from a low value to a high value starting at every crossover.
// Compare register OCR1A auto resets the counter every mains half cycle and turns all
// the TRIAC outputs off.
// OC1RB is used to turn on the triac outputs but because there is only one OCR1A and multiple
// outputs which may be at different brightness levels the following method is used:
//
//      Function setupTRIACTimerList() is called after every change to desired lamp levels and
//      it sets up a buffered table of values for OC1RB and a matching buffered bitlist of
//      channel outputs. The list is in ascending order of OC1RB value
//
//      Function copyTRIACTimerList() is called in the OC1RA interrupt and copies the buffered
//      list into a working copy. A semaphore flag ensures that half completed changes are not
//      picked up by the interrupt code.
//
//      As OC1RB interrupts occur, the ISR steps through the working copy list, setting the
//      output channels according to the bitlist table entry and loading the next value for OC1RB.
//
//
// Timer 1 needs to overflow every half mains cycle (10mS assuming 50 hz)
#define TIMER1_OFLOW_NOM (F_CPU/64)/(2 * MAINS_FREQ)

// a capture value that ensures the triacs remain off -say 10% bigger than the overflow
#define TRIAC_OFF_VALUE ((TIMER1_OFLOW_NOM *11)/10)

// if we try and handle two triac channels but the two interrupts are too close together
// the second one will be missed
// How close? This is a bit empirical but lets try about a 64th of the half wave time.
// Its only a brightness step of 64th but still gives us around 150uS at 50Hz so is enough time
// for the interrupt code not to run out of time.
#define MINGAP (TIMER1_OFLOW_NOM/64)

// The schema is designed so that timer 1 is at some small value around 3-5% at the time of the zero cross.
// This makes it easy to do a phase locked loop timer control based on the zero crossing. We will fix this
// at 4% of full scale count. If you could observe TIMER1 value on a scope alongside the mains waveform you
// see a sawtooth waveform with the falling edge occuring about 4/100 * 10mS or 400uS before the zero crossover
//(Assuming 50hertz mains)
#define ZEROCROSS (TIMER1_OFLOW_NOM/25)

// define the max and min brightnesses for triac control in percent. MAX can be up to 100. MIN must be no less
// than 10 as time is needed in the sequence for setting up the next half cycle. We reserve 10% time for that purpose
// These are the limits applied to any action such as fade up or fade down by button or radio command.
// In practice 25% is about the minimum for incandescant bulbs as they are only just glowing at this setting
#define TRIAC_MAX 100
#define TRIAC_MIN 25

static int16_t triacTimerValues[LIGHTING_CHANNELS]; // the working values for the OC1RB loads
static byte triacOutputList[LIGHTING_CHANNELS]; // the working values for the output channels

static int16_t triacTimerValuesBuffer[LIGHTING_CHANNELS]; // the buffered values
static byte triacOutputListBuffer[LIGHTING_CHANNELS]; // the buffered values

volatile static byte triacChannel;  // the current working channel stepped though in the ISR

// =============================================================================================
// =============================== PWM MODE DEFINATIONS ========================================
// define the max and min brightnesses for PWM control in percent. MAX can be up to 100. MIN can be as low as 1
// These are the limits applied to any action such as fade up or fade down by button or radio command.
#define PWM_MAX 100
#define PWM_MIN 2


// =============================================================================================
// ========================= GENERIC LIGHTING DEFINATIONS & variables===========================

// this is used when applying fading to the raw 0-255 values. it stops us accidentally fading down to
// a very low power level that looks OFF but is actually consuming electricity
#define MINIMUM_DIM_LEVEL 4  // max brightness is 255, OFF is 0


// some arrays to make setting up the pins easier
static const byte channelPWMOutputs[LIGHTING_CHANNELS] = { CHANNEL1_OUT_PIN, CHANNEL2_OUT_PIN, CHANNEL3_OUT_PIN, CHANNEL4_OUT_PWM_PIN};
static const byte noDimmingPins[LIGHTING_CHANNELS] = { nNODIMMING1_PIN, nNODIMMING2_PIN, nNODIMMING34_PIN, nNODIMMING34_PIN};

// The Internal calculations of light levels are done on a scale of 0 (off) to 255 fully ON
// Keep stores so we know where we are:
byte newLevel[LIGHTING_CHANNELS]; // a command has just been received telling us to go to this level if we are on
byte currentLevel[LIGHTING_CHANNELS]; // this is the current level snapshot, may be mid fadeup or down
byte lastONLevel[LIGHTING_CHANNELS]; // when we were last on and stable it was at this level

// A flag so we know to to get the latest updates mid way through a set of changes
static boolean disableTriacUpdatesFlag;

// this is called  in the dead time after all the output matches have occured
// it copies the list of match values and output channels from the buffers
static void copyTRIACTimerList(void)
{
    if ( disableTriacUpdatesFlag == false )
    {
        memcpy(triacOutputList,triacOutputListBuffer, sizeof(triacOutputList));
        memcpy(triacTimerValues, triacTimerValuesBuffer, sizeof(triacTimerValuesBuffer));
    }
}


// this sets up the buffered list of triac outputs when a brightness level changes
// it can be called at any time - eg after a fade level change. It disables updates
// from the interrupt controlled TRIAC driving so no half cooked changes get picked
// up by the interrupts
static void setupTRIACTimerList(void)
{
    #if ( TRIAC_MIN <= 10)
    #error TRIAC_MIN must be greater than 10
    #endif
    #if ( TRIAC_MAX > 100)
    #error TRIAC_MAX must be no greater than 100
    #endif

    #define TIMER1_MAX (ZEROCROSS + ((100-TRIAC_MIN) * TIMER1_OFLOW_NOM/100L))
    #define TIMER1_MIN (ZEROCROSS + ((100-TRIAC_MAX) * TIMER1_OFLOW_NOM/100L))

    byte brightnesscopies[LIGHTING_CHANNELS];
    byte searchctr;
    byte resultsoffset = 0;
    byte rptctr;
    byte value;
    byte match;
    unsigned long calc;
    disableTriacUpdatesFlag = true; // prevent the interrupts getting a half setup scenario

    // get a local list of current brightness values
    memcpy (brightnesscopies,currentLevel, sizeof(currentLevel));

    // clear the TRIAC bit list
    memset (triacOutputListBuffer, 0, sizeof(triacOutputListBuffer));

    for ( searchctr = 0; searchctr < LIGHTING_CHANNELS  ;searchctr++ )
    {
        // search for the highest value in the current brightnes array
        // Serial.printf("\n\r=\t=\t=\t===SEARCH no %d=\t\t=====\n\r",searchctr);
        value = 0;
        match = 255;
        for ( rptctr = 0; rptctr< LIGHTING_CHANNELS ;rptctr++ )
        {
            if (brightnesscopies[rptctr] >= value)
            {
                match = rptctr;
                value = brightnesscopies[rptctr];
            }
        }
        if ( value != 0 )
        {
            // highest current brightness found is in value and it's channel number is in match
            // value is 1(nearly off) to 255 (ON) need to convert it to a timer value
            triacTimerValuesBuffer[resultsoffset] = ((255-value) * (long)(TIMER1_MAX-TIMER1_MIN))/255L + ZEROCROSS;

            if ( resultsoffset > 0 )
            {
                // we have stored at least one result
                if(( triacTimerValuesBuffer[resultsoffset]- triacTimerValuesBuffer[resultsoffset-1] ) < MINGAP)
                {
                    // two are very similar so don't save both
                    resultsoffset--;
                }
            }
            // update the bitlist of matches
            triacOutputListBuffer[resultsoffset] |= 0x1<<match;
            resultsoffset++;
        }
        brightnesscopies[match] = 0; // clear that one - we have used it now
    }
    if ( resultsoffset < LIGHTING_CHANNELS)
    {
        // make sure the next value in the list is an OFF value
        triacTimerValuesBuffer[resultsoffset] = TRIAC_OFF_VALUE;
    }
    disableTriacUpdatesFlag = false; // we've finished fiddling. Allow interrupts to get at the numbers again
}

// Create a sort of phase locked loop where timer 1 is shuffled until it locks on
// to the mains zero crossover detect rising edge at a count of ZEROCROSS
ISR (INT0_vect)
{
    static int16_t snapshot;
    snapshot = TCNT1;
    if ( snapshot < ZEROCROSS )
    {
        OCR1A = TIMER1_OFLOW_NOM - (((ZEROCROSS +2)-snapshot)/4);
    }
    else if ( snapshot > ZEROCROSS )
    {
        OCR1A = TIMER1_OFLOW_NOM + (((snapshot -(ZEROCROSS + 2))/4) &0x1F);
    }
}

// created this - but it should never occur
ISR(TIMER1_OVF_vect)
{
    Serial.printf("OVF - error\n\r");
}

// OCR1B interrupt is used as the match when we turn one or more TRIAC lighting outputs on
ISR(TIMER1_COMPB_vect)
{
    // output the list of bits that should be set to 1
    PORTB |= triacOutputList[triacChannel];
    ++triacChannel;
    // and get the next OCR1B match
    OCR1B = triacTimerValues[triacChannel];
}


// OCR1A interrupt occurs just before the zero crossover and is used to turn
// all TRIAC outputs off and update the timer value list for the next half cycle
ISR(TIMER1_COMPA_vect)
{
    // this is the end of the half cycle, turn off all outputs. They are allm on port
    // B so this is easy
    PORTB &= ~0x0F;
    copyTRIACTimerList(); // get the latest lamp status

    // and set up the first timer match
    OCR1B = triacTimerValues[0];
    triacChannel = 0;
}




#ifdef apcdebug
int apcctr;
void apcdebug(void)
{
    if ( ++apcctr > 80 )
    {
        apcctr = 0;
        Serial.printf("Levels %5d%5d%5d%5d\n\r",
            currentLevel[0],
            currentLevel[1],
            currentLevel[2],
            currentLevel[3]);
        Serial.printf("Timers %5d%5d%5d%5d\n\r",
            triacTimerValues[0],
            triacTimerValues[1],
            triacTimerValues[2],
            triacTimerValues[3]);
        Serial.printf("masks %2x  %2x  %2x  %2x\n\r",
            triacOutputList[0],
            triacOutputList[1],
            triacOutputList[2],
            triacOutputList[3]);

        Serial.printf("Ch %d OCR1A %d OCR1B %d  triacTimerValues[0] %d TCNT1 %d\n\r",triacChannel, OCR1A, OCR1B,triacTimerValues[0],TCNT1);
    }
}
#endif

// Retrieve a mood level calculated from the lamp level
byte mapLampLevelToMood(byte lightingChannel)
{
    byte lampLevel = currentLevel[lightingChannel];

    if ( lampLevel > 200 )
    {
        --lampLevel;
    }
    // ensure we don't store a mood of 255 - that is the empty marker
    return lampLevel;
}

// turns all lights off and sets the last ON state back to max brightness
void initLightLevels(void)
{
    for (byte i = 0; i < LIGHTING_CHANNELS; i++)
    {
        lastONLevel[i] = 255;
        currentLevel[i] = 0;
        newLevel[i] = 0;
    }
 }


// called on boot. Does all the basic light setup, IOs, interrupts etc
void initLights(void)
{
    pinMode(PWMMODE_PIN, INPUT_PULLUP);
    initLightLevels();
    for (byte i = 0; i < LIGHTING_CHANNELS; i++)
    {
        pinMode(noDimmingPins[i], INPUT_PULLUP);
        pinMode(channelPWMOutputs[i], OUTPUT);
        digitalWrite(channelPWMOutputs[i], LOW);
        if ( digitalRead(noDimmingPins[i]) == HIGH)
        {
            // This channel can use dimming
            dimmingAllowed |= 0x01<< i;
        }

    };
    //Serial.printf("Dimming allowed 0x%x\n\r",dimmingAllowed);
    if (digitalRead(PWMMODE_PIN) == LOW)
    {
        // we are a TRIAC controller
        // set up the channel 4 output which is different to PWM channel 4
        pinMode( CHANNEL4_OUT_TRIAC_PIN, OUTPUT);
        digitalWrite(CHANNEL4_OUT_TRIAC_PIN, LOW);

        pinMode(ZEROCROSS_PIN, INPUT_PULLUP);
 
        triacMode = true; // using TRIACs not PWM LEDs

        // need to setup 16 bit timer 1 as follows:
        // a) overflow and reset every 10mS using compare A. Use div by 64
        // b) On compare b check the next output, turn it on and setup
        // for the next compare B

        TCCR1A = 0x0; // no output compares allowed
        OCR1A = TIMER1_OFLOW_NOM;
        OCR1B = TRIAC_OFF_VALUE;

        setupTRIACTimerList();
        copyTRIACTimerList();

        TCCR1B = B00001011; // start running timer1, div 64, Clear TIMER1 on OCR1A match
        TIMSK1 |= B00000111; // enable interrupts on compare A, compare B and overflow

        // enable the mains crossover interrupt
        EICRA |= B00000010; // set falling change on iNT 0 causes interrupt
        EIMSK |= B00000001; // enable INT0
    }
}


// called when any fading process first starts, this could be a fade up a step command from
// button or radio or switch on or switch off command from either.
// It is given the fade command type, the channel number and the end value needed when
// the fade is complete.
//
// The actual fade from current to new level is handled by manageLampOutputs()
void startFade(byte thisChannel, t_fade_mode thisFadeMode, int16_t thisCurrentLevel)
{
    int16_t step;
    boolean powerOff_On = false;
    //Serial.printf("%d Start Fade Channel%d, mode %d lev %d ",time, thisChannel, thisFadeMode, thisCurrentLevel);
    switch ( thisFadeMode )
    {
        case FM_RADIO_BRIGHTEN:
        case FM_RADIO_DIM:
            // this calculation sets up how much one radio 'blip' affects the
            // lamp brightness. If this value is bigger than the gaps in the
            // fade mechanism the lamp will take a few 'mini' fade steps to
            // get to the new level
            thisCurrentLevel = lastONLevel[thisChannel];
            if ( triacMode )
            {
                step = (thisCurrentLevel + 42)/28; // make it appear non linear
            }
            else
            {
                // LED PWM mode
                step = (thisCurrentLevel + 42)/28; // make it appear non linear
            }
            #ifdef apcdebug
            Serial.printf("Triac %d %d %d %d\n\r",
                triacTimerValuesBuffer[0],
                triacTimerValuesBuffer[1],
                triacTimerValuesBuffer[2],
                triacTimerValuesBuffer[3]);
            #endif
            break;
        case FM_BUTTON_BRIGHTEN:
        case FM_BUTTON_DIM:
            // as above but for button blips rather than radio
            thisCurrentLevel = newLevel[thisChannel];
            if ( triacMode )
            {
                step = (thisCurrentLevel + 42)/28; // make it appear non linear
            }
            else
            {
                // LED PWM mode
                step = (thisCurrentLevel + 42)/28; // make it appear non linear
            }
            break;
        case FM_APPLY_MOOD :
            powerOff_On = true;
            // we will fade to the requested level
            break;
        case FM_ON:
            // we are aiming to fade up to the last known ON setting
            powerOff_On = true;
            fadeUpBitlist |= 1<< thisChannel; // we will fade UP next time

            if ( thisCurrentLevel == 0 )
            {
                // if we were OFF, go to last known ON level
                thisCurrentLevel = lastONLevel[thisChannel ];
                if ( thisCurrentLevel == 0 )
                {
                    thisCurrentLevel = 255; // shoud only happen on first boot
                }
            }
            // else we were ON, change to the requested level
            break;
        case FM_OFF:
            // we are aiming to fade down to OFF
            powerOff_On = true;
            thisCurrentLevel = 0;
        default :
            break;
    };
    if ( powerOff_On == false )
    {
        if ( step < 1 )
        {
            step = 1;
        }
        if ( (thisFadeMode &  FM_FLAG) != 0 )
        {
            // we are dimming, not brightening
            thisCurrentLevel -= step;
            if ( thisCurrentLevel < 4 )
            {
                thisCurrentLevel = MINIMUM_DIM_LEVEL;
            }
        }
        else
        {
            thisCurrentLevel += step;
            if ( thisCurrentLevel > 255 )
            {
                thisCurrentLevel = 255;
            }
        }
    }

    // Serial.printf("next lev %d\n\r", thisCurrentLevel);
    newLevel[thisChannel] = thisCurrentLevel;
    lampState[thisChannel] = LS_START_FADING;
}





#ifdef apcdebug
char  *const commandmsgs[] =
{
    "No Command",
    "Lights OFF",
    "Lights ON",
    "BRIGHTEN",
    "DIM",
    "SAVE MOOD",
    "USE MOOD"
};
#endif







// set the PWM output level.
static void setPWMLampLevel(byte channel, byte level)
{
//  Serial.printf("Set ch %d to %d\n\r",channel,level);

    if ((dimmingAllowed & (0x01 << channel)) == 0)
    {
        if ( level > 0 )
        {
            level = 255;
        }
    }
    analogWrite(channelPWMOutputs[channel], level);
}


// This is the main lamp managment tick called by the main loop.
// It calls other ticks such as buttons and indicatorLED and is responsible
// from changing lamp brightnesses from onelevel to the next. The rate at which
// it ticks directly affects how quickly the lamp out puts fades up or down
void manageLampOutputs(void)
{
    static unsigned long lastCheck = 0;

    if ( (millis() - lastCheck) > LAMP_UPDATEmS )
    {
        // every LAMP_UPDATEmS
        lastCheck = millis();
        byte channel = 0;
        int16_t oldLevel16;
        int16_t currentLevel16;
        int16_t newLevel16;
        int16_t step;
        boolean valuesChanged = false;

        // first check if there is a stored radio command to action
        actionRadioStoreCommand();

        // call the button and indicator LED handlers
        handleButtons();
        handleLED();
        // now check all the lamp channels and see iof any changes need to be made
        do
        {
            currentLevel16 = oldLevel16 = currentLevel[channel];
            newLevel16 = newLevel[channel];

            switch ( lampState[channel] )
            {
                case LS_FADING_DOWN:
                    // empirically set the step size in conjunction with LAMP_UPDATEmS
                    // to adjust the rate of fade down
                    step = (currentLevel16 + 30)/16;
                    if ( step < 1 )
                    {
                        step = 1;
                    }
                    currentLevel16 -= step;
                    if ( currentLevel16 <= 0 )
                    {
                        currentLevel16 = 0;
                        lampState[channel] = LS_OFF;
                    }
                    else if ( currentLevel16 <= newLevel16 )
                    {
                        currentLevel16 = newLevel16;
                        lampState[channel] = LS_ON;
                        if ( currentLevel16 != 0 )
                        {
                            lastONLevel[channel] = currentLevel16;
                        }
                    }
                    break;
                case LS_FADING_UP:
                    // empirically set the step size in conjunction with LAMP_UPDATEmS
                    // to adjust the rate of fade up
                    step = (currentLevel16 + 80)/32; // slower fade up than down
                    if ( step < 1 )
                    {
                        step = 1;
                    }
                    currentLevel16 += step;
                    if ( currentLevel16 >= newLevel16 )
                    {
                        currentLevel16 = newLevel16;
                        lampState[channel] = LS_ON;
                        lastONLevel[channel] = currentLevel16;
                    }

                    break;
                case LS_START_FADING:

Serial.printf("Start fade ch%d, dimming allowed %d, new %d\n\r",
    channel,
    ((dimmingAllowed & (0x01 << channel)) != 0),
    newLevel16);
                    if ((dimmingAllowed & (0x01 << channel)) != 0)
                    {
                        if ( newLevel16 == currentLevel16 )
                        {
                            // we've been told to fade but are already at that level so go to the idle state
                            lampState[channel] = ( newLevel16 == 0 )?LS_OFF:LS_ON;
                        }
                        else
                        {
                            lampState[channel] = ( newLevel16 > currentLevel16 )?LS_FADING_UP:LS_FADING_DOWN;
                        }
                    }
                    else
                    {
                        // don't do dimming, so non zero level is fully on and 0 is off
                        if ( newLevel16 != 0 )
                        {
                            currentLevel16 = 255;
                            lampState[channel] = LS_ON;
                        }
                        else
                        {
                            currentLevel16 = 0;
                            lampState[channel] = LS_OFF;
                        }
                    }
                    break;

                case LS_ON:
                case LS_OFF:
                default :
                    // idle here doing nothing, no fades needed
                    break;
            }
            currentLevel[channel] = currentLevel16;

            if ( oldLevel16 != currentLevel16 )
            {
                if ( triacMode == false )
                {
                    setPWMLampLevel(channel, currentLevel16);
                }
                valuesChanged = true;
            }
        }
        while ( ++channel < LIGHTING_CHANNELS );
        if ( valuesChanged == true && triacMode == true )
        {
            setupTRIACTimerList();
        }

    }
}

boolean lampStateOn(byte channel)
{
    return (lampState[channel] == LS_OFF)?false:true;
}
/*
APCZZZ to do list
lots of tidy up
make no dimming work
check LED PWM dimming
magic numbers - create defines

*/
