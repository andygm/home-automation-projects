

commandArray = {}

function splitString(str, delim, maxNb)
    -- Eliminate bad cases...
    if string.find(str, delim) == nil then
        return { str }
    end
    if maxNb == nil or maxNb < 1 then
        maxNb = 0    -- No limit
    end
    local result = {}
    local pat = "(.-)" .. delim .. "()"
    local nb = 0
    local lastPos
    for part, pos in string.gmatch(str, pat) do
        nb = nb + 1
        result[nb] = part
        lastPos = pos
        if nb == maxNb then break end
    end
    -- Handle the last field
    if nb ~= maxNb then
        result[nb + 1] = string.sub(str, lastPos)
    end
    return result
end

function getLastUpdatedTime(deviceName)

   lastUpdatedString = otherdevices_lastupdate[deviceName]

   year = string.sub(lastUpdatedString, 1, 4)
   month = string.sub(lastUpdatedString, 6, 7)
   day = string.sub(lastUpdatedString, 9, 10)
   hour = string.sub(lastUpdatedString, 12, 13)
   minutes = string.sub(lastUpdatedString, 15, 16)
   seconds = string.sub(lastUpdatedString, 18, 19)

   return os.time{year=year, month=month, day=day, hour=hour, min=minutes, sec=seconds}

end

function logtext(text, override)
   if logActivity == 1 or override == true then
      print("(HeatingCoordinator) "..text)
   end
end


function getSetPoint(zoneName)

  logtext("APC testing profile file read")
  dayToday = string.lower(os.date("%a"))
  hourNow = os.date("%H")
  minuteNow = os.date("%M")
  --timeNow = os.time{ year=1970, month=1, day = 1, hour = os.date("%H"), min = os.date("%M") }
  timeNow = os.time{ year=1970, month=1, day = 1, hour = 17, min = 31  }
  
  setPoint = -99.9 
  fileName = "//home/pi/domoticz/zoneprofiles/"..heatingCoordinatorNamePrefix..zoneName.."_Profile.csv"
  --logtext(string.format(" Looking for '%s' '%s' '%s' ",fileName,dayToday, timeNow ))
  fh = io.open(fileName,"r")
  if fh then
    logtext(string.format(" Found '%s' ",fileName ))
        
     while true do
        line = fh.read(fh)
        if not line then 
           break 
        end
        --print (line)
        lineTable = splitString(line,',')
          
          for lineIndex = 1, #lineTable do
            print (lineTable[lineIndex])
          end
                 
        
        if string.lower( lineTable[1]) == dayToday then
          logtext(string.format(" Found '%s' ",dayToday ))
          lineIndex = 2
          repeat
            logtext(string.format(" Trying Index = '%s' ",lineIndex ))
            if timeNow > os.time{ year=1970, month=1, day = 1, hour = string.sub(lineTable[lineIndex], 1, 2), min = string.sub(lineTable[lineIndex], 4, 5) } then
     
              setPoint = lineTable[lineIndex +1]
              logtext(string.format("setPoint found = '%s' time = '%s':'%s'",setPoint, string.sub(lineTable[lineIndex], 1, 2), string.sub(lineTable[lineIndex], 4, 5)))
            else break
            end
            lineIndex = lineIndex +2   
          until lineIndex >= #lineTable
        end
          
      end
        
        
    else
      logtext(string.format("ERROR: Zoneprofile %s Not Found", fileName), true)  
    end
        
   return setPoint
end



function processZones()

   
   for heatingZonesIndex = 1, #heatingZones do
    zoneSetPoint = getSetPoint(heatingZones[heatingZonesIndex])
    logtext(string.format(" ZoneSet Point = %s",zoneSetPoint ))
     
   end
   
   return
end

-- User variable population
heatingCoordinatorNamePrefix = uservariables["HeatingCoordinator_NamePrefix"]
radiatorNameSuffix = uservariables["HeatingCoordinator_RadiatorNameSuffix"]
thermostatNameSuffix = uservariables["HeatingCoordinator_ThermostatNameSuffix"]
tempSensorNameSuffix = uservariables["HeatingCoordinator_TempSensorNameSuffix"]
windowSwitchNameSuffix = uservariables["HeatingCoordinator_WindowSwitchNameSuffix"]
boilerReceiverSwitchName = uservariables["HeatingCoordinator_BoilerReceiverSwitchName"]
heatingZones = splitString(uservariables["HeatingCoordinator_Zones"],",")
boilerStateChangeWaitTime = uservariables["HeatingCoordinator_BoilerStateChangeWaitTime"]
boilerAliveTime = uservariables["HeatingCoordinator_BoilerAliveTime"]
tempHysteresis = uservariables["HeatingCoordinator_TempHysterisis"]
logActivity = uservariables["HeatingCoordinator_LogActivity"]
tempSensorMaxTimeSinceLastSeen = uservariables["HeatingCoordinator_TempSensorMaxTimeSinceLastSeen"]

-- Process heating requirements
processZones()

return commandArray

