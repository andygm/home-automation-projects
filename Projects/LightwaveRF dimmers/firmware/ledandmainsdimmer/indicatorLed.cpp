/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

    This manager controls the one LED output on which we indicate status about any of
    the lighting channels.
    
	Last change: AC 15/02/2016 19:23:24
*/


#include "globals.h"
#include "LwRxapc.h"

#include "lights.h"
#include "buttons.h"
#include "indicatorLED.h"
#include <avr/pgmspace.h>



volatile static t_led_state LEDState;
static int ledPhase;


void handleLED(void)
{
    // called every LAMP_UPDATEmS
    if ( LEDState  != LEDS_LED_OFF)
    {
        --ledPhase;
        switch ( LEDState )
        {
            case LEDS_PAIRING_FLASH:
                if ( pairingActive() )
                {
                    digitalWrite(LED_PIN, ((ledPhase & 0x0E) ==0x6)?HIGH:LOW);
                }
                else
                {
                    LEDState = LEDS_SWITCH_LED_OFF;
                }
                break;

            case LEDS_START_PAIRING_SUCCESS:
                ledPhase = 60;
                LEDState = LEDS_PAIRING_SUCCESS;
                break;
            case LEDS_PAIRING_SUCCESS:
                if ( ledPhase == 0 )
                {
                    LEDState = LEDS_SWITCH_LED_OFF;
                }
                else
                {
                    digitalWrite(LED_PIN, ((ledPhase & 0x02) ==0)?HIGH:LOW);
                }
                break;

            case LEDS_START_ALLCLEAR_SUCCESS:
                ledPhase = 200;
                LEDState = LEDS_ALLCLEAR_SUCCESS;
                break;
            case LEDS_ALLCLEAR_SUCCESS:
                if ( ledPhase == 0 )
                {
                    LEDState = LEDS_SWITCH_LED_OFF;
                }
                else
                {
                    digitalWrite(LED_PIN, ((ledPhase & 0x06) !=0)?HIGH:LOW);
                }
                break;

            case LEDS_START_MOODSTORE_SUCCESS:
                ledPhase = 200;
                LEDState = LEDS_MOODSTORE_SUCCESS;
                break;
            case LEDS_MOODSTORE_SUCCESS:
                if ( ledPhase == 0 )
                {
                    LEDState = LEDS_SWITCH_LED_OFF;
                }
                else
                {
                    digitalWrite(LED_PIN, ((ledPhase & 0x3C) !=0)?HIGH:LOW);
                }
                break;

            case LEDS_START_PAIRING_REMOVE_SUCCESS:
                ledPhase = 200;
                LEDState = LEDS_PAIRING_REMOVE_SUCCESS;
                break;
            case LEDS_PAIRING_REMOVE_SUCCESS:
                if ( ledPhase == 0 )
                {
                    LEDState = LEDS_SWITCH_LED_OFF;
                }
                else
                {
                    digitalWrite(LED_PIN, ((ledPhase & 0x1E) !=0)?HIGH:LOW);
                }
                break;

            case LEDS_START_RX_FLASH:
                ledPhase = 3;
                digitalWrite(LED_PIN, HIGH);
                LEDState = LEDS_RX_FLASH;
                break;
            case LEDS_RX_FLASH:
                if ( ledPhase == 0 )
                {
                    LEDState = LEDS_SWITCH_LED_OFF;
                }
                break;

            case LEDS_SWITCH_LED_OFF :
                digitalWrite(LED_PIN,LOW);
                LEDState = LEDS_LED_OFF;
                break;
            default :
                break;
        }
    }
}

// Called to set the LED state machine. Coud be called from interrupt
// or from foreground code
void setIndicatorLED(t_led_state state)
{
    // only do the Receive indicate if the LED is NOT showing some other state
    if (state == LEDS_START_RX_FLASH && LEDState != LEDS_LED_OFF)
    {
        return;
    }
    LEDState = state;
}

