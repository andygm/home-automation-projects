/** @package 

    buttons.h
    
    Copyright(c) Microsoft 2000
    
    Author: Andrew Copsey
    Created: AC  18/10/2015 17:50:31
	Last change: AC 17/03/2016 17:55:18
*/
#ifndef buttons__h
#define buttons__h

extern void enableButtonInterrupts(void);
extern void buttonsProcess(void);
extern bool buttonsPressed(void);
extern void buttonsInit(void);

#define SHORTBUTTONBITSHIFT 0x0
#define LONGBUTTONBITSHIFT 0x02
#define BUTTONDOWNBITSHIFT 0x04

#define SHORTBUTTONPRESSMASK (0x03 << SHORTBUTTONBITSHIFT)
#define LONGBUTTONPRESSMASK (0x03 << LONGBUTTONBITSHIFT)
#define BUTTONDOWNMASK (0x03 << BUTTONDOWNBITSHIFT)

extern unsigned char getbuttons(void);
extern unsigned char savedButtons;

#endif

