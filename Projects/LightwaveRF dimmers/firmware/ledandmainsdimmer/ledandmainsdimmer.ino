/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

	Last change: AC 15/02/2016 19:33:47
*/


#include "globals.h"
#include "LwRxapc.h"
#if EEPROM_EN == 1
#include <EEPROM.h>
#endif

#include "lights.h"
#include "buttons.h"

#include <avr/pgmspace.h>

extern void handleRFMessage(void);




#define revisionstring F("433 Light dimmer Rev 0.04 15-02-2016\n\r")

#define feedback
#ifdef feedback
  #define pr(x) Serial.print(x)
  #define prln(x) Serial.println(x)
#else
  #define pr(x)
  #define prln(x)
#endif
 
#define echo true



unsigned long millisOffset = millis();

//Msg data
byte msg[10];
byte msglen = 10;

//Repeats data
static byte repeats = 0;
static byte timeout = 20;

//pair data
static byte pairtimeout = 50;
static byte pairEnforce = 0;
static byte pairBaseOnly = 0;

//Serial message input
const byte maxvalues = 10;
byte indexQ;
boolean newvalue;
int invalues[maxvalues];

void setup() {
   // set up with rx into pin RXIO_PIN
   lwrx_setup(RXIO_PIN);
   Serial.begin(SERIAL_BAUD);
   prln(revisionstring);
   indexQ = 0;
   invalues[0] = 0;
   newvalue = false;
   initButtons();
   initLights();

}


void loop() {
   //collect any incoming command message and execute when complete
//    manageButtons();
//    manageLampOutputs();
//    manageRadioMessages();

   if(getMessage()){
      switch(invalues[0]) {
         case 1: // Get Stats
            printStats();
            break;
         case 2: // Reset Stats
            lwrx_setstatsenable(false);
            lwrx_setstatsenable(true);
            prln("Stats reset and enabled");
            break;
         case 3: // Disable Stats
            lwrx_setstatsenable(false);
            prln("Stats disabled");
            break;
         case 4: // Add pair
            if (indexQ >=8) {
               byte pair[8];
               pair[0] = invalues[1];
               pr(pair[0]);
               for (byte i = 2; i <= 7; i++) {
                  pair[i] = invalues[i];
                  pr("-");
                  pr(pair[i]);
               }
               prln(" Pair added");
               lwrx_addpair(pair);
            } else {
               prln("Insufficient pair data.");
            }
            break;
         case 5: // Clear pair
            lwrxapc_clearpairing();
            break;
         case 6: // Set repeat filter
            if( indexQ > 1) repeats = invalues[1];
            if (indexQ > 2) timeout = invalues[2];
            lwrx_setfilter(repeats, timeout);
            pr("LwRx set filter repeat=");
            pr(repeats);
            pr(" timeout=");
            prln(timeout);
            break;
         case 7: // Set message display
            // dump all pairs
            dumpPairs();
#ifdef aaaa
            if (indexQ > 1) msglen = invalues[1];
            pr("Set message display len=");
            pr(msglen);
            if (indexQ > 2) {
               lwrx_settranslate(invalues[2] !=0);
               pr(" translate ");
               pr(invalues[2] !=0);
            }
            prln();
#endif
            break;
          case 8: // Put in pairing mode
            {
                byte channel = 0;
                if (indexQ > 1) pairtimeout = invalues[1];
                if (indexQ > 2) channel = invalues[2];
                pr("Set into pair mode for 100ms * ");
                prln(pairtimeout);
                lwrxapc_makepair(pairtimeout, channel);
            }
            break;
         case 9: // Get and display a pair
            byte pair[8];
            byte paircount;
            paircount = lwrx_getpair(pair, invalues[1]);
            pr("Paircount=");
            pr(paircount);
            if (invalues[1] < paircount) {
               pr(" Pair addr");
               for(byte i = 0; i< 8; i++) {
                  if(i != 1) {
                     pr(" ");
                     pr(pair[i]);
                  }
               }
               Serial.printf(" Channel %d",pair[1]);
            }
            prln();
            break;
         case 10: // Time since last packet
            pr("Last packet delay mS=");
            prln(lwrx_packetinterval());
            break;
         case 11: // Pair controls
            if( indexQ > 1) pairEnforce = invalues[1];
            if (indexQ > 2) pairBaseOnly = invalues[2];
            lwrx_setPairMode(pairEnforce, pairBaseOnly);
            pr("Pair mode enforce=");
            pr(pairEnforce);
            pr(" baseOnly=");
            prln(pairBaseOnly);
            break;
       default:
            help();
            break;
      }
      indexQ = 0;
      invalues[0] = 0;
   }
   if (lwrx_message())
   {
      handleRFMessage();
   }
    manageLampOutputs();
}


/**
   Retrieve and print out current pulse width stats
**/
void printStats() {
}

/**
   Check and build input command and paramters from serial input
**/
boolean getMessage() {
   int inchar;
   if(Serial.available()) {
      inchar = Serial.read();
      if (echo) Serial.write(inchar);
      if(inchar == 10 || inchar == 13) {
         if (newvalue) indexQ++;
         newvalue = false;
         if (echo && inchar != 10) Serial.println();
         return true;
      } else if ((indexQ < maxvalues) && inchar >= 48 && inchar <= 57) {
         invalues[indexQ] = invalues[indexQ] * 10 + (inchar - 48);
         newvalue = true;
      } else if (indexQ < (maxvalues - 1)) {
         indexQ++;
         invalues[indexQ] = 0;
         newvalue = false;
      }
   }
   return false;
}

void help() {
   Serial.println(F("Commands:"));
   Serial.println(F("   1,  Get Stats"));
   Serial.println(F("   2,  Reset stats"));
   Serial.println(F("   3,  Disable stats"));
   Serial.println(F("   4,dev,ad1,ad2,ad3,ad4,ad5,room Add pair"));
   Serial.println(F("   5,  Reset pairs"));
   Serial.println(F("   6,repeats,timeout  Set repeat filter"));
   Serial.println(F("   all pairs"));
//   Serial.println("   7,n,t Message display len(2,4,10),translate"));
   Serial.println(F("   8,n,ch Put into pairing mode for up to n * 100mSec for channel 0-3"));
   Serial.println(F("   9,n Get pairdata n"));
   Serial.println(F("  10,  Time in mS since last packet"));
   Serial.println(F("  11,e,b  Pair controls e=enforce pairing, b=address only"));
   Serial.println(F("[] Defaults to last value if not entered"));
}

/*


*/

