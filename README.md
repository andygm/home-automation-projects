This repository contains my Home Automation Projects based around Domoticz running on a Raspberry Pi B+.

The Pi is attached to a 2.4GHZ Mysensors serial gateway, a RFXCOM 433MHz gateway and some OneWire based temperature sensors. 

The house has some LightwaveRF based lighting systems which I am extending to cover more rooms. Generally, the transmitters are all genuine LightwaveRF but most receivers are home grown as I need more functionality and greater reliability than I experienced with off the shelf lightwaveRF kit. The lighting system described here is in use and works well, though I ma adding more functionality all the time.

The 2.4GHZ Mysensors kit is currently only used for remote sensors for heating but is intended to do other things such as security as time permits. The heating is a fully zoned system controlling every radiator and the gas boiler and is based on lua scripting in Domoticz. At the time of writing its is mostly working on the bench and planned to be installed this summer.

Please bear with me as I am new to Git, Domoticz and lua and also Arduino so am on a steep learning curve. Please feel free to use any of this but although I'll try and make sure anything on the master branch builds and works there is no guarantee.

For help on each project please delve. You should find a readme of some sort at the root of each project. If that doesn't exist I probably didn't intend to share the contents yet.

Coppo (coppothebike AT gmail.com)