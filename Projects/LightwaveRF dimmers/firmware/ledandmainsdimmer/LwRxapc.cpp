/* LwRx.cpp

 LightwaveRF 434MHz receiver interface for Arduino

 Author: Bob Tidey (robert@tideys.net)

 Modified by APC Jan 2016 as follows
    The timing was altered to be based on a resolution of 16uS ticks for better accuracy.

    The RX data input is inverted and a hardware schmitt trigger added to filter noise.
    The interrupt routine was rewritten and made non blocking
    Repeat count capability added so held TX buttons can act as fade up or fade down events
    lots of othe minor changes that make it incompatible with the original BT code

	Last change: AC 15/02/2016 19:31:22
*/

#include "LwRxapc.h"
#include "globals.h"
#include "lights.h"
#include "indicatorLED.h"
#include <avr/pgmspace.h>


static const byte rx_nibble[16] = {0xF6,0xEE,0xED,0xEB,0xDE,0xDD,0xDB,0xBE,0xBD,0xBB,0xB7,0x7E,0x7D,0x7B,0x77,0x6F};
static const byte rx_cmd_off     = 0xF6; // raw 0
static const byte rx_cmd_on      = 0xEE; // raw 1
static const byte rx_cmd_mood    = 0xED; // raw 2
static const byte rx_par0_alloff = 0x7D; // param 192-255 all off (12 in msb)
static const byte rx_dev_15      = 0x6F; // device 15

static int rx_pin = 2;




#define RX_MSGLEN 10// expected length of rx message


//Receive mode constants and variables
static byte rx_msg[RX_MSGLEN+1]; // raw message received + 1 for repeat count
static byte rx_incoming_buf[RX_MSGLEN]; // message buffer during reception
static byte rx_oldbuf[RX_MSGLEN]; // message buffer for debouncing recption

static unsigned long rx_prev; // time of previous interrupt in microseconds

//set to -1 until message arrives. It then contains the bitlist of matching lighting channels
// range of values 0x01 to 0x0F assuming LIGHTING_CHANNELS = 4
static signed char rx_msgcomplete = -1;
static boolean rx_translate = true; // Set false to get raw data

static byte rx_state = 0;
static const byte rx_state_idle = 0;
static const byte rx_state_msgstartfound = 1;
static const byte rx_state_bytestartfound = 2;
static const byte rx_state_getbyte = 3;
static const byte rx_state_startbit = 4;
static byte missedframes;


volatile static byte rx_num_bits = 0; // number of bits in the current byte
volatile static byte rx_num_bytes = 0; // number of bytes received

//Pairing data
static byte rx_paircount = 0;


static byte rx_pairs[RX_MAXPAIRS][BYTES_PER_PAIR];
static byte rx_pairtimeout = 0; // 100msec units
//set false to responds to all messages if no pairs set up
static boolean rx_pairEnforce = false;
//set false to use Address, Room and Device in pairs, true just the Address part
static boolean rx_pairBaseOnly = false;

// this is the local RX channel 0-3. This is set when looking to pair. The pairing id is stored
//against this channel number. This gives us the effect of multiple devices in one box
static byte lastrxlightingchannel = 0;


// Repeat filters
static byte rx_repeats = 2; //msg must be repeated at least this number of times
byte rx_repeatcount = 0;
static byte rx_timeout = 20; //reset repeat window after this in 100mSecs
static unsigned long rx_prevpkttime = 0; //last packet time in milliseconds
static unsigned long rx_pairstarttime = 0; //last msg time in milliseconds

#ifdef nostats
// Gather stats for pulse widths (ave is x 16)
static const uint16_t lwrx_statsdflt[rx_stat_count] = {5000,0,5000,20000,0,2500,4000,0,500};	//usigned int
static uint16_t lwrx_stats[rx_stat_count];	//unsigned int
static boolean lwrx_stats_enable = true;
#endif



boolean pairingActive(void)
{
    if ( rx_pairtimeout > 0 && (((millis() - rx_pairstarttime)/100) < rx_pairtimeout))
    {
        return true;
    }
    else
    {
         return false;
    }
}


void stopMakePair(void)
{
    rx_pairtimeout =0;
}


/**
  Pin change interrupt routine that identifies 1 and 0 LightwaveRF bits
  and constructs a message when a valid packet of data is received.
	Last change: AC 13/02/2016 17:39:01
**/

#define MINHIGHuS 120  // a high pulse shorter than this is just noise and will be ignored

// define bit periods. These times include a high pulse (typ 220-280us) plus the
//length of the low
#define MINSHORTLOWuS 350
#define MAXSHORTLOWuS 770
#define MINLONGLOWuS 1000
#define MAXLONGLOWuS 2100
#define MINMSGGAPuS 3300
#define RESuS 16L  // the resolution of the timers (use ints rather than longs)

#define NOISEPULSE  0
#define ONEPULSE  1
#define ONEZEROPULSE  2
#define SYNCPULSE  3


#ifdef errdebug
int apcerr1 =0;
int apcerr2 =0;
int apcerr3 =0;
#endif

volatile int lastgonehightimestamp;
volatile int lastgonelowtimestamp;

void rx_process_bits() { }

ISR (INT1_vect, ISR_NOBLOCK){
    volatile static byte oldevent;
    volatile static boolean skippedonealready;
    byte event = !digitalRead(rx_pin); // start setting event to the current value
    unsigned int curr = (int)(micros()/RESuS); // the current time in microseconds x16
    unsigned int lastpulse = 0;

    if ( oldevent != event )
    {
        // it has changed state - are we interested?
        if ( event != 0 )
        {
            // its just gone high
            lastgonehightimestamp = curr;
        }
        else
        {
            // its just gone low, check if it was high long enough
            if ( (curr - lastgonehightimestamp) >= MINHIGHuS/RESuS)
            {
                lastpulse = curr -lastgonelowtimestamp;
                lastgonelowtimestamp = curr;
            }
        }
        oldevent = event;
    }

    if ( lastpulse > 0 )
    {
        // we have the time period of the last low plus clock period
        // lest turn this onto a 1 or 10 or sync or something else
        if ( lastpulse > MINSHORTLOWuS/RESuS && lastpulse < MAXSHORTLOWuS/RESuS)
        {
            lastpulse = ONEPULSE;
        }
        else if ( lastpulse > MINLONGLOWuS/RESuS && lastpulse < MAXLONGLOWuS/RESuS)
        {
            lastpulse = ONEZEROPULSE;
        }
        else if ( lastpulse > MINMSGGAPuS/RESuS)
        {
            lastpulse = SYNCPULSE;
        }
        else
        {
            lastpulse = NOISEPULSE;
        }

        switch(rx_state)
        {
            default:
            case rx_state_idle:
                // wer're waiting for sync/message gap
                if ( lastpulse == SYNCPULSE )
                {
                    rx_state = rx_state_startbit;
                }
                break;

            case rx_state_startbit:
                // we want a 1. nothing else will do
                if ( lastpulse == ONEPULSE)
                {
                    rx_num_bytes = 0;
                    rx_num_bits = 0;
                    rx_state = rx_state_bytestartfound;
                }
                else
                {
                    #ifdef errdebug
                    apcerr1++;
                    #endif
                    rx_state = rx_state_idle;
                }
                break;

            case rx_state_bytestartfound:
                if ( lastpulse == ONEPULSE)
                {
                    rx_num_bits = 0;
                    rx_state = rx_state_getbyte;
                }
                else if ( lastpulse == ONEZEROPULSE)
                {
                    rx_state = rx_state_getbyte;
                    // Starts with 0 so put this into byte
                    rx_num_bits = 1;
                    rx_incoming_buf[rx_num_bytes] = 0;
                }
                else
                {
                    #ifdef errdebug
                    apcerr2++;
                    #endif
                    rx_state = rx_state_idle;
                }
                break;

            case rx_state_getbyte:
                if ( lastpulse == ONEPULSE)
                {
                    rx_incoming_buf[rx_num_bytes] = rx_incoming_buf[rx_num_bytes] << 1 | 1;
                    rx_num_bits++;
                }
                else if ( lastpulse == ONEZEROPULSE)
                {
                    rx_incoming_buf[rx_num_bytes] = rx_incoming_buf[rx_num_bytes] << 2 | 2;
                    rx_num_bits++;
                    rx_num_bits++;
                }
                else
                {
                    #ifdef errdebug
                    apcerr3++;
                    #endif
                    rx_state = rx_state_idle;
                }
                break;

        }

        if(rx_num_bits >= 8)
        {
            // end of a byte
            rx_num_bytes++;
            rx_num_bits = 0;
            if(rx_num_bytes >= RX_MSGLEN)
            {
                unsigned long currMillis = millis();
                if(rx_repeats > 0)
                {
                    if((currMillis - rx_prevpkttime) / 100 > rx_timeout)
                    {
                        rx_repeatcount = 1;
                    }
                    else
                    {
                        //Test message same as last one
                        int16_t i = RX_MSGLEN;	//int
                        do
                        {
                            i--;
                        }  while((i >= 0) && (rx_incoming_buf[i] == rx_msg[i]));
                        if(i < 0)
                        {
                            // its the same as the last one
                            memcpy(rx_msg, rx_incoming_buf, RX_MSGLEN);
                            ++rx_repeatcount;
                            //Serial.printf("Same %d\n\r",rx_repeatcount);
                            missedframes = 0;
                        }
                        else
                        {

                            // its not the same as the last one, see how we deal
                            if ( rx_repeatcount <= 2 || ++missedframes >= 2)
                            {
                                // we are allowed 1 skip only
                                rx_repeatcount = 1;
                                missedframes = 0;
                                memcpy(rx_msg, rx_incoming_buf, RX_MSGLEN);
                            }
                            else
                            {
                                // we can ignore this missed frame
                                //Serial.printf("MISSED %d\n\r",rx_repeatcount);
                            }
                        }
                        memcpy(rx_oldbuf, rx_incoming_buf, RX_MSGLEN);

                    }
                }
                else
                {
                    rx_repeatcount = 0;
                }
                rx_prevpkttime = currMillis;


                // stuff the repeat count as a hidden byte at the end of the buffer
                rx_msg[RX_MSGLEN] = rx_repeatcount;
                if(rx_repeats == 0 || rx_repeatcount >= rx_repeats)
                {
                    if ( rx_repeatcount == rx_repeats )
                    {
                        if(rx_pairtimeout != 0)
                        {
                            if((currMillis - rx_pairstarttime) / 100 <= rx_pairtimeout)
                            {
                                if(rx_msg[3] == rx_cmd_on)
                                {
                                    lwrxapc_addpairfrommsg();

                                    setIndicatorLED(LEDS_START_PAIRING_SUCCESS);
                                }
                                else if(rx_msg[3] == rx_cmd_off)
                                {
                                    rxapc_removePair(&rx_msg[2]);
                                   setIndicatorLED(LEDS_START_PAIRING_REMOVE_SUCCESS);
                                }
                            }
                       }
                    }
                    if ( rx_repeatcount >= rx_repeats && (((rx_repeatcount - rx_repeats) % 3) == 0) )
                    {
                        // auto repeat the message every 3 frames
                        rx_msgcomplete = rx_reportMessage();
                        if (rx_msgcomplete != -1)
                        {
                            setIndicatorLED(LEDS_START_RX_FLASH);
                        }
                        #ifdef errdebug
                        Serial.printf("APCZZZ %d %d %d\n\r",apcerr1, apcerr2, apcerr3);
                        apcerr1=  apcerr2 = apcerr3 = 0;
                        #endif
                    }
                    rx_pairtimeout = 0;
                }
                // And cycle round for next one
                rx_state = rx_state_idle;
            }
            else
            {
                rx_state = rx_state_bytestartfound;
            }
        }
    }
}


/**
  Test if a message has arrived
**/
boolean lwrx_message() {
   return (rx_msgcomplete != -1);
}

/**
  Set translate mode
**/
void lwrx_settranslate(boolean rxtranslate) {
   rx_translate = rxtranslate;
}

/**
  Transfer a message to user buffer
**/
boolean lwrxapc_getmessage(byte *buf, byte len) {
   boolean ret = true;
   int16_t j=0,k=0;		//int

   //The incoming msg buffer is 11 bytes as follows:
   // Byte 0
   // Byte 1
   // Byte 2
   // Byte 3
   // Byte 4
   // Byte 5
   // Byte 6
   // Byte 7
   // Byte 8
   // Byte 9
   // Byte 10 - is added by receiver code and is the packet repeat ctr


   if((rx_msgcomplete != -1) && len <= RX_MSGLEN) {
      for(byte i=0; ret && i < RX_MSGLEN; i++) {
         if(rx_translate || (len != RX_MSGLEN)) {
            j = rx_findNibble(rx_msg[i]);
            if(j<0) ret = false;
         } else {
            j = rx_msg[i];
         }
         switch(len) {
            case 6:
               // pack the 6 byte output buffer
               // COMMAND  PARAM  ROOM  DEVICE CHANNELBITLIST TXLENGTHin PCTs
               // we need to stick the extra info at the end of the buffer
               // may as well do it on i == 0
               if(i==0)
               {
                  buf[4] = rx_msgcomplete; // the channel number bitlist
                  buf[5] = rx_msg[RX_MSGLEN]; // the no of tx packets counted
               }
            case 4:
               if(i==9) buf[2]=j;
               if(i==2) buf[3]=j;
            case 2:
               if(i==3) buf[0]=j;
               if(i==0) buf[1]=j<<4;
               if(i==1) buf[1]+=j;
               break;
            case 3:
               if(i==3) buf[0]=j;
               if(i==0) buf[1]=j<<4;
               if(i==1)
                {
                    buf[1]+=j;
                    buf[2] = rx_msgcomplete; // the channel number bitlist
                }
               break;
            case 10:
               buf[i]=j;
               break;
         }
      }
      rx_msgcomplete= -1;
   } else {
      ret = false;
   }
   return ret;
}

/**
  Return time in milliseconds since last packet received
**/
unsigned long lwrx_packetinterval() {
   return millis() - rx_prevpkttime;
}

/**
  Set up repeat filtering of received messages
**/
void lwrx_setfilter(byte repeats, byte timeout) {
   rx_repeats = repeats;
   rx_timeout = timeout;
}

/**
  Add a pair to filter received messages
  pairdata is device,dummy,5*addr,room
  pairdata is held in translated form to make comparisons quicker
**/
byte lwrx_addpair(byte* pairdata) {
#ifdef apczzz
   if(rx_paircount < RX_MAXPAIRS) {
      for(byte i=0; i< BYTES_PER_PAIR; i++) {
         rx_pairs[rx_paircount][i] = rx_nibble[pairdata[i]];
      }
      rx_paircommit();
   }
#endif
   return rx_paircount;
}

/**
  Make a pair from next message successfully received
**/
extern void lwrxapc_makepair(byte timeout, byte channel) {
   lastrxlightingchannel = channel & 0x03;
   rx_pairtimeout = timeout;
   rx_pairstarttime = millis();
   setIndicatorLED(LEDS_PAIRING_FLASH);
}



/**
  Get pair data (translated back to nibble form
**/
extern byte lwrx_getpair(byte* pairdata, byte pairnumber) {
   if(pairnumber < rx_paircount) {
      int16_t j;	//int
      for(byte i=0; i<BYTES_PER_PAIR; i++) {
        if ( i ==1 )
        {
            pairdata[i] = rx_pairs[pairnumber][i]; // the channel number
        }
        else
        {
         j = rx_findNibble(rx_pairs[pairnumber][i]);
         if(j>=0) pairdata[i] = j;
        }
      }
   }
   return rx_paircount;
}

extern void dumpPairs(void)
{
    byte pairctr;
    byte nibblectr;
    byte buffer[BYTES_PER_PAIR];
    Serial.printf("ALL PAIRS:\n\r\tDevice Channels ---- ID ------  Sub\n\r");

    for ( pairctr = 0; pairctr < rx_paircount ; pairctr++)
    {
        lwrx_getpair(buffer,pairctr);
        Serial.printf("\t%02x    (%02x)",buffer[0],buffer[1]);
        nibblectr = 0;
        do
        {
            Serial.printf("%d",((buffer[1] & 0x08) != 0)?1:0);
            buffer[1] <<= 0x01;
        }
        while ( ++nibblectr < 4 );
        Serial.printf("  %02x %02x %02x %02x %02x  %02x\n\r ",buffer[2],
                                buffer[3],
                                buffer[4],
                                buffer[5],
                                buffer[6],
                                buffer[7]
                                );
    }
}


/**
  Clear all pairing
**/
extern void lwrxapc_clearpairing() {
   rx_paircount = 0;
#if EEPROM_EN
   EEPROM.write(PAIRS_EEPROMaddr, 0);

   // also clear all moods
   byte address = MOOD_EEPROMaddr;
   byte ctr = MAX_MOODS * LIGHTING_CHANNELS;
   do
   {
        EEPROM.write(address++, EMPTYMOOD);
   }
   while ( --ctr != 0);
#endif
   Serial.printf(F("LwRx pairing cleared\r\n."));

}




/**
  Return stats on high and low pulses
**/
boolean lwrx_getstats(uint16_t *stats) {	//unsigned int
#ifdef nostats
   if(lwrx_stats_enable) {
      memcpy(stats, lwrx_stats, 2 * rx_stat_count);
      return true;
   } else {
      return false;
   }
#endif
    return false;
}

/**
  Set stats mode
**/
void lwrx_setstatsenable(boolean rx_stats_enable) {
#ifdef nostats
   lwrx_stats_enable = rx_stats_enable;
   if(!lwrx_stats_enable) {
      //clear down stats when disabling
      memcpy(lwrx_stats, lwrx_statsdflt, sizeof(lwrx_statsdflt));
   }
#endif
}
/**
  Set pairs behaviour
**/
void lwrx_setPairMode(boolean pairEnforce, boolean pairBaseOnly) {
   rx_pairEnforce = pairEnforce;
   rx_pairBaseOnly = pairBaseOnly;
}


/**
  Set things up to receive LightWaveRF 434Mhz messages
  pin must be 2 or 3 to trigger interrupts
  !!! For Spark, any pin will work
**/
void lwrx_setup(int pin) {
   restoreEEPROMPairing();
	rx_pin = pin;
   int int_no = getIntNo(rx_pin);
   pinMode(rx_pin,INPUT);
//   attachInterrupt(int_no, rx_process_bits, CHANGE);
    EICRA |= B00000100; // set any change on iNT 1 causes interrupt
    EIMSK |= B00000010; // enable INT1
    #ifdef nostats
    memcpy(lwrx_stats, lwrx_statsdflt, sizeof(lwrx_statsdflt));
    #endif
}

/**
  Check a message to see if it should be reported under pairing / mood / all off rules
  returns -1 if none found or lighting channel bitlist it matches (0x01 - 0x0F)
**/
signed char rx_reportMessage(void)
{
    if(rx_pairEnforce && rx_paircount == 0) {
        return false;
    }
    else
    {
        boolean allDevices;
        signed char pairNumber;
        byte channelBitlist;
        // True if mood to device 15 or Off cmd with Allof paramater
        allDevices = ((rx_msg[3] == rx_cmd_mood && rx_msg[2] == rx_dev_15) ||
                    (rx_msg[3] == rx_cmd_off && rx_msg[0] == rx_par0_alloff));

        pairNumber =  rx_checkPairs(&rx_msg[2], allDevices, &channelBitlist);
        if ( pairNumber >= 0 )
        {
            pairNumber = channelBitlist;
        }
        return pairNumber;
    }
}
/**
  Find nibble from byte
  returns -1 if none found
**/
int16_t rx_findNibble(byte data) {	//int
   int16_t i = 15;	//int
   do {
      if(rx_nibble[i] == data) break;
      i--;
   } while (i >= 0);
//Serial.printf("apcfindnibble %d %d\n\r",data, i);
   return i;
}



/**
  add pair from message buffer
**/
void lwrxapc_addpairfrommsg()
{
    // check if the pair already exists
    if(rx_paircount < RX_MAXPAIRS)
    {
        memcpy(rx_pairs[rx_paircount], &rx_msg[2], BYTES_PER_PAIR); // put a temp copy in the buffer
        int16_t existingpair = rx_checkPairs(rx_pairs[rx_paircount], false, NULL);
        if ( existingpair >= 0 )
        {
            // the pair exists though maybe not for this channel
            rx_pairs[existingpair][1] |= (0x01 << lastrxlightingchannel);
        }
        else
        {
            // its a new pairing
            existingpair = rx_paircount;
            rx_pairs[existingpair][1] = 0x01 << lastrxlightingchannel;
            rx_paircount++;
        }

      Serial.printf(F("APC Adding pair %d, channel %d\n\r"),rx_paircount,lastrxlightingchannel);
      rx1_paircommit(existingpair);
   }
}

/**
  Remove an existing pair matching the buffer.
  Because a pair may be included for 1 or more lighting channels, we only
  clear the lighting channel match. If the pairing then no longer matches any
  channels we delete it entirely.
**/
void rxapc_removePair(byte *buf)
{
    byte channelList;
    int16_t pair = rx_checkPairs(buf, false, &channelList);	//int
    boolean deletepair = false;
    boolean pairchanged = false;
    if(pair >= 0)
    {
        // we have found this pair in the table, first of all, lets knock the channel out of it
        if ( (rx_pairs[pair][1] & (0x01 << lastrxlightingchannel)) != 0)
        {
            // the channel we were looking for is in this pairing
            rx_pairs[pair][1] &= ~(0x01 << lastrxlightingchannel);
            pairchanged = true;

            if (rx_pairs[pair][1] == 0)
            {
                // this pairing no longer matches any channels, delete it
                deletepair = true;
            }
        }
        if ( deletepair )
        {
             Serial.printf(F("Deleting pair %d\n\r"),pair);
              while (pair < rx_paircount - 1)
              {
                 for(byte j=0; j<BYTES_PER_PAIR;j++)
                 {
                    rx_pairs[pair][j] = rx_pairs[pair+1][j];
                    #if EEPROM_EN
                       EEPROM.write(PAIRS_EEPROMaddr + 1 + BYTES_PER_PAIR * pair + j, rx_pairs[pair][j]);
                    #endif
                 }
                 pair++;
              }
              rx_paircount--;
              #if EEPROM_EN
                 EEPROM.write(PAIRS_EEPROMaddr, rx_paircount);
              #endif
        }
        else if (pairchanged)
        {
            Serial.printf(F("Changed pair %d\n\r"),pair);
            rx1_paircommit(pair);
        }
    }
}



/**
  check and commit pair
**/
void rx1_paircommit(byte pairno) {
#if EEPROM_EN
        Serial.printf("Commit pairno %d;=  ");
		for(byte i=0; i<BYTES_PER_PAIR; i++) {
			EEPROM.write(PAIRS_EEPROMaddr + 1 + BYTES_PER_PAIR * pairno + i, rx_pairs[pairno][i]);
            Serial.printf("%02x ",rx_pairs[pairno][i]);
		}
        Serial.printf("\n\r");
      EEPROM.write(PAIRS_EEPROMaddr, rx_paircount);
#endif
}
/**
  Check to see if message matches one of the pairs
    if mode is pairBase only then ignore device and room
    if allDevices is true then ignore the device number
  Returns matching pair number, -1 if not found, -2 if no pairs defined
  Fills in the matching lighting channels in channelList
**/
int16_t rx_checkPairs(byte *buf, boolean allDevices, byte *channelList ) {	//int

//Serial.printf("apc %d %d\n\r",allDevices, rx_pairBaseOnly);
   if(rx_paircount ==0) {
      return -2;
   } else {
      byte channelBitlist = 0;
      int16_t pair= rx_paircount;	//int
      int16_t firstmatch = -1;	//int
      int16_t j = -1;	//int
      int16_t jstart,jend;	//int
      if(rx_pairBaseOnly) {
         // skip room(8) and dev/cmd (0,1)
         jstart = 7;
         jend = 2;
      } else {
         //include room in comparison
         jstart = 8;
         //skip device comparison if allDevices true
         jend = (allDevices) ? 2 : 0;
      }
//      while (pair>0 && j<0) {
      while (pair>0)
      {
         pair--;
         j = jstart;
         while(j>jend)
         {
            j--;
            if(j != 1)
            {
               if(rx_pairs[pair][j] != buf[j]) {
                  j = -1;
               }
            }
         }
         if ( j >= 0  )
         {
            channelBitlist |= rx_pairs[pair][1];
            if (firstmatch == -1 )
             {
                firstmatch = pair;
             }
         }
      }
      //Serial.printf("pair %d 0x%02x\n\r",firstmatch, channelBitlist );
      if (channelList)
      {
          *channelList = channelBitlist;
      }
      return firstmatch;
   }
}


/**
   Retrieve and set up pairing data from EEPROM if used
**/
void restoreEEPROMPairing() {
#if EEPROM_EN
	rx_paircount = EEPROM.read(PAIRS_EEPROMaddr);
	if(rx_paircount > RX_MAXPAIRS) {
		rx_paircount = 0;
		EEPROM.write(PAIRS_EEPROMaddr, 0);
	} else {
		for( byte i=0; i < rx_paircount; i++) {
			for(byte j=0; j<BYTES_PER_PAIR; j++) {
				rx_pairs[i][j] = EEPROM.read(PAIRS_EEPROMaddr + 1 + BYTES_PER_PAIR * i + j);
			}
		}
	}
#endif
}
/**
   Get Int Number for a Pin
**/
int getIntNo(int pin) {
   return digitalPinToInterrupt(pin);
}


