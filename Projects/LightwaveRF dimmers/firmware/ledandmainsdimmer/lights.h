#ifndef __lights_h
#define  __lights_h

/*
    Taken from https://github.com/roberttidey/LightwaveRF started 24/12/2015

	Last change: AC 14/02/2016 19:55:45
*/



typedef enum
{
    LC_LIGHTS_NO_COMMAND = 0,
    LC_LIGHTS_OFF,
    LC_LIGHTS_ON,
    LC_LIGHTS_BRIGHTEN,
    LC_LIGHTS_DIM,
    LC_LIGHTS_SAVE_MOOD,
    LC_LIGHTS_USE_MOOD

}t_lampCommand;

extern void changeLightsSettings(byte channelmap, t_lampCommand command , byte level);
extern void manageLampOutputs(void);

typedef enum
{
    FM_RADIO_BRIGHTEN,
    FM_BUTTON_BRIGHTEN,
    FM_ON,
    FM_APPLY_MOOD,

    FM_FLAG = 0x20, // if this bit is set it is a dimming or OFF command
                    // if not it is a brightening or ON command
    FM_RADIO_DIM = FM_FLAG,
    FM_BUTTON_DIM,
    FM_OFF
}t_fade_mode;

extern void startFade(byte thisChannel, t_fade_mode thisFadeMode, int16_t thisCurrentLevel);
extern boolean lampStateOn(byte channel);
extern byte mapLampLevelToMood(byte lightingChannel);
extern void initLightLevels(void);

extern void initLights(void);

// The tick rate for the lamp update and everything to do with it such as button scan,
// information LED update. Delayed reacting to radio commands also use this as the tick
// rate. Most things calculate times and take into account changes to this value correctly.
//
// However it also directly affects the rate at which fades occur from level to level
// Empirical but suggest 10 -50mS
#define LAMP_UPDATEmS 15


#endif //  __lights_h
